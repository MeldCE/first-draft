# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.8.0] 2022-05-18

### Added

- Importing of PDFs. Each page of the PDF is rendered and then imported
  as a collection of images

## [0.7.0] 2022-02-11

### Added

- Presets label to presets

### Fixed

- Ignore z in url if is equal to 0
- Add tool preset information to the README
- Improved styling around options menu
- Handling of drawing when move mouse off canvas, unclick, then move back on
- Fixed issues with presets
- Errors caused by trying to change the styling of textboxes and lines
  being created

### Changed

- Migrated to [Vite](https://vitejs.dev/) as just so much easier
- Changed to using alpine as base image for docker images

## [0.6.2]

### Fixed

- Fixed initial zoom on draft with spaces, by removing initial URL rewrite

### Changed

- URLs are now not decoded in the address bar, makig them easier to share

## [0.6.1] 2021-01-22

### Fixed

- The scaling of font when the font size is changed during editing a textbox

## [0.6.0] 2021-01-20

### Added

- Added stroke ending to pen options
- Added text options and presets
- Added pen presets

## Changed

- Pressing escape if there are objects selected will deselect those selected
  objects. Pressing escape with no objects selected will change the tool to the
  Select tool

## [0.5.0] 2020-10-01

### Added

- Added UI testing framework using [WebdriverIO](https://webdriver.io)
- Added ability to share a view of a draft through a URL
- Added the current view to the url. If view was specified in the original
  url, you will be able to navigate back to it using the browsers back button.
- Added `viewUrl` to the config to enable/disable adding of the current view
  to the url
- Added `alwaysReplaceUrl` to disable/enable keeping the initial view in the
  browser history

### Fixed

- Optimizing of Javascript

## [0.4.3] - 2020-09-26

### Added

- Added deflate/gzip compression to server

## [0.4.2] - 2020-05-12

### Changed

- Changed to [alpine](https://www.alpinelinux.org/)-based
  [node](https://nodejs.org) [docker](https://docs.docker.com/engine/) image
  to decrease size and increase security of docker image
- Updated a few dependencies

## [0.4.1] - 2020-05-10

### Fixed

- Setting of client IP for debug logging

## [0.4.0] - 2020-04-12

### Added

- Thumbnailing of large images to increase performance of draft with large
  images and decrease the amount of data that needs to be downloaded (as
  it can be configured to download thumbnails and full-size images only when
  required [when the images are zoomed in on]). A 500ms debounce delay has been
  added to optimize performance of changing between images.
- Added the ability to zoom out further (up to 0.001x by default), and make it
  configurable using the `minZoomLevel` configuration parameter
- Implemented the ability to customise the tools enabled and the mappings
  per server (in the config file) and per draft (manually)
- Added a dual function selection box to the select tool. Click and drag the
  selection box to the right to select only those items completely enclosed
  by the box. Click and drag the selection box to the left to select all items
  that touch or are inside the box. You can add more items to an existing
  selection by holding down (by default) the Ctrl key. Also works with touch
  devices.
- When creating a new draft, the name of the draft is now set to the slug
  (the unique url id) of the draft by default unless the slug has been
  randomly generated.
- Add ability to export the current view as a PNG or SVG image and the
  entire drawing as an SVG image
- Sending of cursor, tool change and view change update messages, which will
  be used to share view and cursor with other users. This can be turned off
  for either the server, drawing or user by using the `share` config object.
- Added ability to debug requests and events on the server

### Fixed

- Fixed the remembering of passwords and user details
- non-urls being pasted are now ignored, unless in the text tool

### Other

- Updated dependencies, except PaperJs, which is currently broken
- Added temporary fix for Gitlab docker build issue (#149)

## [0.3.6] - 2019-09-10

### Fixed

- Enabling of saving of password when a password is entered

## [0.3.5] - 2019-09-09

### Added

- Support email address to README

## [0.3.4] - 2019-07-22

### Fixed

- Fixed image file select on Chrome for Android

## [0.3.3] - 2019-07-18

### Fixed

- Fixed pasting formatted text

## [0.3.2] - 2019-07-14

### Fixed

- Fixed README

## [0.3.1] - 2019-07-13

### Fixed

- pasting text when textbox tool active, but no textbox active

## [0.3.0] - 2019-07-12

### Added

- docker containers for containerised running and development
- automated development branch previewing for all
  [merge requests](https://gitlab.com/MeldCE/first-draft/merge_requests)
- preview of the next segment for the click-aciton pen tool
- being able to paste text into a draft when the textbox tool is selected
- pen configuration that can change line properties for future and currently
  selected lines
- insert images through either an image url or selecting them from your
  computer

### Fixed

- Using special characters in draft ids
- Using special characters in images

## [0.2.1] - 2019-05-24

### Added

- Pan/Zoom tool with global using middle click or shift click
- Cancelling of a path using the pen with a right click or escape
- Click select/move/delete tool
- Click pen tool
- Image insertion using drag and drop or pasting image(s) onto the draft
- Global edit password that can be set in a config file (see README)

### Fixed

- Sizing in mobile browsers
- Cancelled line not being removed from other connected clients

## [0.1.3] - 2019-03-13

### Added

- Added demo link to README

### Fixed

- Add bang to first-draft command so it runs

## [0.1.2] - 2019-03-10

Initial release.

Features:

- pen tool
- saving to JSON/folder

## [0.0.1] - 2019-03-02

Initial placeholder release

[unreleased]: https://gitlab.com/MeldCE/first-draft/compare/v0.8.0...dev
[0.8.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.7.0...v0.8.0
[0.7.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.6.2...v0.7.0
[0.6.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.3...v0.5.0
[0.4.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.6...v0.4.0
[0.3.6]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.5...v0.3.6
[0.3.5]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.4...v0.3.5
[0.3.4]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.3...v0.3.4
[0.3.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.1.3...v0.2.1
[0.1.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.0.1...v0.1.2
[0.0.1]: https://gitlab.com/MeldCE/first-draft/tree/v0.0.1
