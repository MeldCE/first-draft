import { JSONSchema7 } from 'json-schema';

export default {
  $id: 'schema/instanceOptions',
  title: 'Draft Instance Options',
  description: 'Options avaiable for each draft instance',
  type: 'object',
  properties: {
    name: {
      type: 'string',
      title: 'Draft Name'
    },
    editPassword: {
      type: 'string',
      format: 'password',
      title: 'Edit Password',
      description:
        'Require this password to be given before this draft can be edited'
    }
  }
} as JSONSchema7;
