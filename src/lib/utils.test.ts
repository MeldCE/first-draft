import test = require('japa');

import * as utils from './utils';

test('getDeep() gets a deep value', (assert) => {
  const path = ['test', 0];
  assert.equal(utils.getDeep({ test: ['hello'] }, path), 'hello');
  assert.deepEqual(path, ['test', 0]);
});

test('getDeep() returns undefined if value not set', (assert) => {
  const path = ['test', 'test', 0];
  assert.isUndefined(utils.getDeep({ test: {} }, path));
  assert.deepEqual(path, ['test', 'test', 0]);
});

test('setDeep() replaces a value that already is in the object', (assert) => {
  const object = { test: ['blah'] };
  assert.deepEqual(utils.setDeep(object, ['test', 0], 'hello'), {
    test: ['hello']
  });
  assert.deepEqual(object, { test: ['blah'] });

  assert.deepEqual(
    utils.setDeep([{ test: 'blah' }], [0, 'test'], 'hello'),
    [{ test: 'hello' }],
    'Error starting with an array'
  );
});

test('setDeep() creates objects if they dont already exist', (assert) => {
  const object = {};
  assert.deepEqual(utils.setDeep(object, ['test', 1], 'hello'), {
    test: [undefined, 'hello']
  });
  assert.deepEqual(object, {});

  assert.deepEqual(
    utils.setDeep(null, ['test', 1], 'hello'),
    { test: [undefined, 'hello'] },
    'Error creating object from scratch'
  );

  assert.deepEqual(
    utils.setDeep([], [1, 'test'], 'hello'),
    [undefined, { test: 'hello' }],
    'Error starting from an array'
  );

  assert.deepEqual(
    utils.setDeep(null, [1, 'test'], 'hello'),
    [undefined, { test: 'hello' }],
    'Error creating array from scratch'
  );
});

test('setDeep() throws if there is a type conflict', (assert) => {
  assert.throw(() => {
    utils.setDeep({ test: 'hello' }, ['test', 'test'], 'hello');
  }, 'Conflicting value exists in path at 1');

  assert.throw(() => {
    utils.setDeep({ test: ['hello'] }, ['test', 'test'], 'hello');
  }, 'Conflicting value exists in path at 1');
});

test('arrayContainsValues() works with a single value', (assert) => {
  assert.isTrue(utils.arrayContainsValues(['one', 2, null], 'one'));
  assert.isTrue(utils.arrayContainsValues(['one', 2, null], 2));
  assert.isTrue(utils.arrayContainsValues(['one', 2, null], null));
  assert.isFalse(utils.arrayContainsValues(['one', 2, null], 'two'));
});

test('arrayContainsValues() works with an arra of values', (assert) => {
  assert.isTrue(utils.arrayContainsValues(['one', 2, null], ['one']));
  assert.isTrue(utils.arrayContainsValues(['one', 2, null], ['one', 2]));
  assert.isTrue(utils.arrayContainsValues(['one', 2, null], [2, null]));
  assert.isFalse(utils.arrayContainsValues(['one', 2, null], ['one', 'two']));
});

test('removeValuesFromArray() creates a copy of removes values', (assert) => {
  const array = ['one', 2, null];

  assert.deepEqual(utils.removeValuesFromArray(array, 'one'), [2, null]);
  assert.deepEqual(array, ['one', 2, null], 'change original array');

  assert.deepEqual(
    utils.removeValuesFromArray(array, 2),
    ['one', null],
    'did not remove a number'
  );

  assert.deepEqual(
    utils.removeValuesFromArray(array, null),
    ['one', 2],
    'did not remove a null'
  );
  assert.deepEqual(array, ['one', 2, null], 'change original array');

  assert.deepEqual(
    utils.removeValuesFromArray(['one', 2, 2, null], 2),
    ['one', null],
    'did not remove multiple numbers'
  );
});

test('cleanFilename() cleans a filename', (assert) => {
  assert.equal(utils.cleanFilename('test/hello'), 'test_hello');
  assert.equal(utils.cleanFilename('test\\hello'), 'test_hello');
});
