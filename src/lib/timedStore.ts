export interface TimedStore<T> {
  create(id: string, data: T, lifetime?: number): boolean;
  get(id: string): T;
  have(id: string): boolean;
  delete(id: string): boolean;
  expiry(id: string): number | false;
}

export const timedStore = (storeLifetime?: number, expire?: boolean) => {
  const store = {};

  const getLifetime = (lifetime?: number) => {
    if (typeof lifetime === 'number') {
      return lifetime;
    } else if (typeof storeLifetime === 'number') {
      return storeLifetime;
    } else {
      return 0;
    }
  };

  return {
    create: (id: string, data?: any, lifetime?: number) => {
      if (!store[id]) {
        const itemLifetime = getLifetime(lifetime);

        if (lifetime) {
          store[id] = {
            data,
            expiry: new Date().getTime() + lifetime * 1000,
            timeout: setTimeout(() => {
              if (expire) {
                store[id].expired = true;
              } else {
                delete store[id];
              }
            }, lifetime * 1000)
          };
        } else {
          store[id] = {
            data
          };
        }

        return true;
      }

      return false;
    },
    get: (id: string) => {
      return store[id] && store[id].data;
    },
    have: (id: string) => {
      if (store[id]) {
        return true;
      }
      return false;
    },
    delete: (id: string) => {
      if (store[id]) {
        if (store[id].timeout) {
          clearTimeout(store[id].timeout);
        }
        delete store[id];
        return true;
      }
      return false;
    },
    expiry: (id: string) => {
      if (store[id]) {
        return store[id].expiry || 0;
      }

      return false;
    }
  };
};
export default timedStore;
