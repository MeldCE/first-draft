import { Draft, DraftUpdate } from '../typings/Draft';

const updateValue = (
  object: { [key: string]: any },
  key: string,
  value: any
) => {
  if (value === null && typeof object[key] !== 'undefined') {
    delete object[key];
  } else {
    object[key] = value;
  }
};

/**
 * Update the given draft by creating a SHALLOW copy of the draft (and config
 * if updating) and updating the values given.
 *
 * @param draft Draft to create an updated version of
 * @param update Update to apply to the draft
 *
 * @returns The updated draft
 */
export const mergeDraftUpdate = (draft: Draft, update: DraftUpdate) => {
  const updatedDraft: Draft = { ...draft };

  ['name', 'description', 'created', 'createdBy'].forEach((key) => {
    if (typeof update[key] !== 'undefined') {
      updateValue(updatedDraft, key, update[key]);
    }
  });

  if (update.config) {
    updatedDraft.config = updatedDraft.config ? { ...updatedDraft.config } : {};

    // TODO Remove 'name'
    ['permissions', 'tools', 'editPassword', 'name'].forEach((key) => {
      if (typeof update.config[key] !== 'undefined') {
        updateValue(updatedDraft.config, key, update.config[key]);
      }
    });
  }

  return updatedDraft;
};

/**
 * A DraftError object. Based off an Error, but can contain details about the
 * draft/layer/element that caused the error
 *
 * @param {string} message Error message
 * @param {object} [data] Error data
 * @param {string} [data.draftId] Draft ID
 * @param {string} [data.layerName] Draft layer name
 * @param {string} [data.elementId] Draft elementId
 */
export function DraftError(message, data) {
  Object.defineProperty(this, 'name', {
    writable: false,
    enumerable: false,
    value: 'DraftError: ' + message
  });
  Object.defineProperty(this, 'message', {
    writable: false,
    enumerable: false,
    value: message
  });
  if (data?.draftId) {
    this.draftId = data.draftId;
  }
  if (data?.layerName) {
    this.layerName = data.layerName;
  }
  if (data?.elementId) {
    this.elementId = data.elementId;
  }
}

DraftError.prototype = new Error();
