import test = require('japa');

import * as sha from './sha';

test('getSha() returns the hash of a value', (assert) => {
  assert.equal(
    sha.getSha('teststring'),
    '3c8727e019a42b444667a587b6001251becadabbb36bfed8087a92c18882d111'
  );
  assert.equal(
    sha.getSha('teststring'),
    '3c8727e019a42b444667a587b6001251becadabbb36bfed8087a92c18882d111'
  );
});

test('signChallenge() signs a challenge by concatination', (assert) => {
  // password > 5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8
  assert.equal(
    sha.signChallenge(
      'challenge',
      '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
    ),
    'bf990fb75ed421cc7ab3aa44454c74d5905657179a1e60b7f0185d38f1457780'
  );
  assert.equal(
    sha.signChallenge(
      'challenge',
      '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
    ),
    'bf990fb75ed421cc7ab3aa44454c74d5905657179a1e60b7f0185d38f1457780'
  );
  assert.equal(
    sha.signChallenge(
      'challenge',
      '3c8727e019a42b444667a587b6001251becadabbb36bfed8087a92c18882d111'
    ),
    'e1ecebad99b4eedb52bf6bb930dfa4adb25b6a1ac089aa45277f5e0a7aa31cde'
  );
});
