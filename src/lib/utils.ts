/**
 * Get a value from deep inside an object
 *
 * @param object Object to get value from
 * @param path Path to traverse to get value
 *
 * @returns The deep value or undefined if value or something in the path
 *   doesn't exist
 */
export const getDeep = (
  object: { [key: string]: any } | Array<any>,
  path: Array<string | number>
): any => {
  if (!path || !path.length) {
    return object;
  }

  path = path.concat();

  while (typeof object === 'object' && path.length) {
    object = object[path.shift()];
  }

  if (path.length) {
    return;
  }

  return object;
};

/**
 * Set a value deep inside an object, creating objects as required
 *
 * @param object Object to set value in
 * @param path Path to value to set
 * @param value Value to set value as
 *
 * @returns A copy of the given object with the value set
 */
export const setDeep = (
  object: { [key: string]: any } | Array<any> | null,
  path: Array<string | number>,
  value: any
): { [key: string]: any } | Array<any> => {
  if (!path || !path.length) {
    return object;
  }

  if (object === null) {
    if (typeof path[0] === 'number') {
      object = [];
    } else if (typeof path[0] === 'string') {
      object = {};
    } else {
      throw new Error('Path must only contain strings and numbers');
    }
  } else if (Array.isArray(object)) {
    if (typeof path[0] !== 'number') {
      throw new Error('Conflicting value exists in path at 0');
    }
    object = [...object];
  } else if (typeof object === 'object') {
    if (typeof path[0] !== 'string') {
      throw new Error('Conflicting value exists in path at 0');
    }
    object = { ...object };
  }

  let leaf = object;
  let name;
  let next;

  for (let i = 0; i < path.length - 1; i++) {
    name = path[i];
    next = path[i + 1];
    if (typeof leaf[name] === 'undefined' || leaf[name] === null) {
      if (typeof next === 'number') {
        leaf[name] = [];
      } else if (typeof next === 'string') {
        leaf[name] = {};
      } else {
        throw new Error('Path must only contain strings and numbers');
      }
    }
    if (Array.isArray(leaf[name])) {
      if (typeof next === 'number') {
        leaf[name] = [...leaf[name]];
      } else {
        throw new Error('Conflicting value exists in path at ' + (i + 1));
      }
    } else if (typeof next === 'string' && typeof leaf[name] === 'object') {
      leaf[name] = { ...leaf[name] };
    } else {
      throw new Error('Conflicting value exists in path at ' + (i + 1));
    }

    leaf = leaf[name];
  }

  leaf[path[path.length - 1]] = value;

  return object;
};

/**
 * Check if the given array contains all of the given values
 *
 * @param array Array to check for values
 * @param values Values to check for
 *
 * @returns Whether the array contains all the values
 */
export const arrayContainsValues = (array: any[], values: any | any[]) => {
  if (!Array.isArray(values)) {
    values = [values];
  }

  const length = values.length;

  for (let i = 0; i < length; i++) {
    if (array.indexOf(values[i]) === -1) {
      return false;
    }
  }

  return true;
};

/**
 * Duplicate the array, removing any of the specified value(s) from the new
 * array
 *
 * @param array Array to duplicate
 * @param values values to remove
 *
 * @return New array with values removed
 */
export const removeValuesFromArray = (array: any[], values: any | any[]) => {
  if (!Array.isArray(values)) {
    values = [values];
  }

  array = [...array];
  let i = 0;
  let index;

  while (i < array.length) {
    index = values.indexOf(array[i]);

    if (index !== -1) {
      array.splice(i, 1);
    } else {
      i++;
    }
  }

  return array;
};

/**
 * Clean filenames by removing/replacing bad characters
 *
 * @param id ID to clean
 */
export const cleanFilename = (id: string): string => {
  // Replace other characters
  id = id.replace(/[/\\]/g, '_');

  return id;
};
