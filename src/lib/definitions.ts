export const tokenCookie = 'token';
export const nonceParameter = 'nonce';

export const badChallenge = 'badChallenge';
export const unknownUser = 'unknownUser';
export const notEnabled = 'notEnabled';
export const unknownAuth = 'unknownAuth';
