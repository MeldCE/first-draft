/**
 * The Store module handles storing values in browser Storage
 * (LocalStorage and SessionStorage). Values are stored LocalStorage until
 * the page is unloaded. If the value should not be remembered, the value
 * is removed from LocalStorage, but stored in SessionStorage, so the value
 * persists between refreshes (the same session), but between sessions. If
 * the value should be remembered, it is kept in LocalStorage.
 *
 * ::SUBSCRIPTIONS:: details how many sessions are watching the value and
 *   if the value should be remembered
 * ::LOCK:: write locks values and the subscription details
 */

interface StoreValueDetails {
  subscribers: number;
  remember: boolean;
}

type Callback = (key: string, newValue: any, oldValue: any) => void;

const subscriptions: {
  [key: string]: { callbacks: Callback[]; value: any };
} = {};

const LOCK = '::LOCK::';
const SUBSCRIPTIONS = '::SUBSCRIPTIONS::';

/**
 *
 * @TODO Add tries and forceful last try
 */
const runWithLock = (key: string, callback: () => any) => {
  let count = 0;
  let lastLock;
  return new Promise((resolve, reject) => {
    const tryLock = () => {
      const lock = key + LOCK;
      const id = Math.random().toString();
      let currentLock = localStorage.getItem(lock);
      // Get a write lock
      if (!currentLock || (currentLock === lastLock && count >= 5)) {
        localStorage.setItem(lock, id);
        count = 0;

        currentLock = localStorage.getItem(lock);
        if (currentLock === id || (currentLock === lastLock && count >= 5)) {
          const value = callback();
          localStorage.removeItem(lock);
          resolve(value);
          return;
        }
      }

      if (lastLock !== currentLock) {
        lastLock = currentLock;
        count = 0;
      } else {
        count++;
      }

      console.debug(`Could not get lock for ${lock} on attempt ${count + 1}`);
      setTimeout(tryLock, Math.ceil(Math.random() * 10));
    };

    tryLock();
  });
};

const getDetails = (key: string): StoreValueDetails => {
  let value = localStorage.getItem(key + SUBSCRIPTIONS);
  if (value) {
    try {
      return JSON.parse(value);
    } catch {
      void 0;
    }
  }

  return {
    subscribers: 0,
    remember: true
  };
};

const removeStoreSubscription = (key: string) => {
  return runWithLock(key, () => {
    // Get current subscriber info
    let storeSubscribers = getDetails(key);

    if (storeSubscribers) {
      if (storeSubscribers.subscribers <= 1) {
        if (!storeSubscribers.remember) {
          let value = localStorage.getItem(key);
          if (value !== null) {
            sessionStorage.setItem(key, value);
            localStorage.removeItem(key);
          }
        }
        localStorage.removeItem(key + SUBSCRIPTIONS);
      } else {
        storeSubscribers.subscribers--;
        localStorage.setItem(
          key + SUBSCRIPTIONS,
          JSON.stringify(storeSubscribers)
        );
      }
    }
  });
};

export const subscribe = (
  key: string,
  callback: Callback,
  remember?: boolean
) => {
  const finishSubscription = () => {
    if (subscriptions[key].callbacks.indexOf(callback) === -1) {
      subscriptions[key].callbacks.push(callback);
    }
    return Object.create({
      /**
       * Unsubscribes from value changes
       */
      unsubscribe: (key: string, callback: Callback) => {
        if (subscriptions[key]) {
          const index = subscriptions[key].callbacks.indexOf(callback);

          if (index !== -1) {
            if (subscriptions[key].callbacks.length === 1) {
              delete subscriptions[key];

              removeStoreSubscription(key);
            } else {
              subscriptions[key].callbacks.splice(index, 1);
            }
          }
        }
      },
      /**
       * Gets the current value
       */
      get: () => {
        return subscriptions[key].value;
      },
      /**
       * Sets the value
       */
      set: (value: any) => {
        return runWithLock(key, () => {
          if (value === null || typeof value === 'undefined') {
            localStorage.removeItem(key);
            subscriptions[key].value = null;
          } else {
            localStorage.setItem(key, JSON.stringify(value));
            subscriptions[key].value = value;
          }
        });
      },
      remember: (value?: boolean) => {
        let details = getDetails(key);
        if (typeof value === 'boolean') {
          return runWithLock(key, () => {
            details.remember = value;
            localStorage.setItem(key + SUBSCRIPTIONS, JSON.stringify(details));

            return value;
          });
        } else {
          return details.remember;
        }
      },
      /**
       * Deletes the value, but does not unsubscribe from value changes
       */
      delete: () => {
        return runWithLock(key, () => {
          localStorage.removeItem(key);
        });
      }
    });
  };

  if (!subscriptions.hasOwnProperty(key)) {
    return runWithLock(key, () => {
      // Get the current value
      let value = localStorage.getItem(key);
      let storeSubscribers;
      // Try and get the current subscriptions
      storeSubscribers = localStorage.getItem(key + SUBSCRIPTIONS);
      if (storeSubscribers) {
        try {
          storeSubscribers = JSON.parse(storeSubscribers);
        } catch {
          storeSubscribers = null;
        }
      }
      if (value) {
        try {
          value = JSON.parse(value);
          remember = true;
        } catch {
          value = null;
        }
      }
      if (value === null) {
        value = sessionStorage.getItem(key);
        if (value) {
          try {
            value = JSON.parse(value);
            sessionStorage.removeItem(key);
            remember = false;
          } catch {
            value = null;
          }
        }
      }

      // Add to subscriber
      if (!storeSubscribers) {
        storeSubscribers = {
          subscribers: 0,
          remember: typeof remember === 'boolean' ? remember : true
        };
      }

      storeSubscribers.subscribers++;
      localStorage.setItem(
        key + SUBSCRIPTIONS,
        JSON.stringify(storeSubscribers)
      );
      subscriptions[key] = {
        callbacks: [],
        value
      };
    }).then(finishSubscription);
  } else {
    return Promise.resolve(finishSubscription());
  }
};

export const subscribeObject = (
  key: string,
  callback: Callback,
  remember?: boolean
) => {
  let cache;
  const callbacks: { [objectKey: string]: Callback[] } = {};
  const handler = (theKey, newValue, oldValue) => {
    cache = newValue;
    if (callback) {
      callback(theKey, newValue, oldValue);
    }
    const keys = Object.keys(callbacks);
    for (let i = 0; i < keys.length; i++) {
      if (newValue[keys[i]] !== oldValue[keys[i]]) {
        callbacks[keys[i]].forEach((callback) =>
          callback(keys[i], newValue[keys[i]], oldValue[keys[i]])
        );
      }
    }
  };

  return subscribe(key, handler, remember).then((subscription) => {
    cache = subscription.get();
    return Object.create({
      get: (objectKey?: string) => {
        if (objectKey) {
          return cache && cache[objectKey];
        } else {
          return cache;
        }
      },
      set: (objectKey: string | { [key: string]: any }, value: any) => {
        if (cache) {
          if (typeof objectKey === 'object') {
            let update = false;
            Object.entries(objectKey).forEach(([key, value]) => {
              if (value === null && cache.hasOwnProperty(key)) {
                delete cache[key];
                update = true;
              } else if (value !== null) {
                cache[key] = value;
                update = true;
              }
            });
            if (update) {
              subscription.set(cache);
            }
          } else if (value === null && cache.hasOwnProperty(objectKey)) {
            delete cache[objectKey];
            subscription.set(cache);
          } else if (value !== null) {
            cache[objectKey] = value;
            subscription.set(cache);
          }
        } else if (typeof objectKey === 'object') {
          cache = {
            ...objectKey
          };
          subscription.set(cache);
        } else if (value !== null) {
          cache = {
            [objectKey]: value
          };
          subscription.set(cache);
        }
      },
      subscribe: (objectKey: string, callback: Callback) => {
        if (!callbacks[objectKey]) {
          callbacks[objectKey] = [callback];
        } else {
          callbacks[objectKey].push(callback);
        }
      },
      unsubscribe: (objectKey: string, callback: Callback) => {
        if (callbacks[objectKey]) {
          const index = callbacks[objectKey].indexOf(callback);

          if (index !== -1) {
            if (callbacks[objectKey].length === 1) {
              delete callbacks[objectKey];
            } else {
              callbacks[objectKey].splice(index, 1);
            }
          }
        }
      },
      remember: subscription.remember
    });
  });
};

// Listen for change events
if (typeof window !== 'undefined') {
  window.addEventListener('storage', (event: StorageEvent) => {
    if (subscriptions[event.key]) {
      let newValue;
      let oldValue;
      try {
        newValue = (event.newValue && JSON.parse(event.newValue)) || null;
      } catch {
        newValue = null;
      }
      try {
        oldValue = (event.oldValue && JSON.parse(event.oldValue)) || null;
      } catch {
        oldValue = null;
      }
      subscriptions[event.key].value = newValue;
      const callbacks = subscriptions[event.key].callbacks;
      for (let i = 0; i < callbacks.length; i++) {
        callbacks[i](event.key, newValue, oldValue);
      }
    }
  });

  window.addEventListener('unload', (event) => {
    const keys = Object.keys(subscriptions);
    if (keys.length) {
      for (let i = 0; i < keys.length; i++) {
        removeStoreSubscription(keys[i]);
      }
    }
  });
}
