import { SocketContext } from '../../server/draft';
import { Socket } from 'socket.io';

import * as cookie from 'cookie';

export const addSocketData = (
  event: string,
  data: any,
  socket: Socket,
  context: SocketContext
) => {
  const cookies =
    context.config.cookie &&
    socket.request.headers['cookie'] &&
    cookie.parse(socket.request.headers['cookie']);

  const eventData = {
    event,
    data,
    time: data.time || Date.now(),
    socketId: socket.id,
    hostname: socket.request.headers.host.split(':')[0],
    clientIp:
      socket.request.headers['x-real-ip'] ||
      (socket.request.headers['x-forwarded-for'] &&
      Array.isArray(socket.request.headers['x-forwarded-for'])
        ? socket.request.headers['x-forwarded-for'][0]
        : (socket.request.headers['x-forwarded-for'] as string).replace(
            /,.*$/,
            ''
          )) ||
      socket.handshake.address ||
      null,
    userId: (context.user && context.user.id) || null,
    cookieId: (cookies && cookies[context.config.cookie.id]) || null
  };

  return eventData;
};

export const loggingSocket = (
  socket: Socket,
  onHandler: (event: string, data: any) => void,
  emitHandler?: (event: string, data: any) => void
) => {
  const onHandlers = {};

  const overrides: any = {
    on: {
      value: function (event: string, handler: (data: any) => void) {
        if (!onHandlers[event]) {
          onHandlers[event] = [];
        }
        const logHandler = (data) => {
          onHandler(event, data);
          handler(data);
        };
        onHandlers[event].push(logHandler);
        socket.on(event, logHandler);
      }
    }
  };

  if (emitHandler) {
    overrides.emit = {
      value: function (event: string, data: any) {
        socket.emit(event, data);
      }
    };
  }

  return Object.create(socket, overrides);
};

export const createSocketLogger = (
  socket: Socket,
  socketContext: SocketContext
) => {
  const log = socketContext.log.child
    ? socketContext.log.child('socket-event')
    : socketContext.log;
  const onHandler = (event: string, data: any) => {
    log.debug(addSocketData(event, data, socket, socketContext));
  };

  return loggingSocket(socket, onHandler);
};
