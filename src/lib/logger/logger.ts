interface Styling {
  [key: string]: any;
}

interface Options {
  color?: string;
  styling?: Styling;
}

export type DebugLevel = 'debug' | 'info' | 'error' | 'log';

export interface Logger {
  /**
   * Log a message
   *
   * @param message Data to log
   */
  (...message: Array<any>): void;
  /**
   * Set the id of the logging module
   *
   * @param id ID of the logging module.
   * @param noAppend If true, the ID will replace the existing instead of
   *   being appended to it
   */
  child?: (id: string | Array<string>, newOptions?: string | Options) => Logger;
  /**
   * Log a debug message
   *
   * @param message Data to log
   */
  debug: (...message: Array<any>) => void;
  /**
   * Log a info message
   *
   * @param message Data to log
   */
  info: (...message: Array<any>) => void;
  /**
   * Log an error message
   *
   * @param message Data to log
   */
  error: (...message: Array<any>) => void;
  /**
   * Log a log message
   *
   * @param message Data to log
   */
  log: (...message: Array<any>) => void;
}

const moveColor = (options: Options) => {
  if (options.color) {
    if (!options.styling) {
      options.styling = {
        color: options.color
      };
    } else {
      options.styling.color = options.color;
    }
  }
};

export const createLogger = (
  logger: Logger | Console,
  initialId: string[] = [],
  initialOptions: Options = {}
): Logger => {
  moveColor(initialOptions);
  const newLog = (id: string[], options: Options) => {
    const formatId = (additionalId?: string) => {
      const nowId = [...id];
      if (additionalId) {
        nowId.push(additionalId);
      }

      if (options.styling) {
        return [
          `%c${nowId.join('/')}`,
          Object.keys(options.styling)
            .map((property) => `${property}: ${options.styling[property]}`)
            .join('; ')
        ];
      } else {
        return [nowId.join('/')];
      }
    };

    const log = (...message) => {
      logger.log(formatId().concat(...message));
    };

    Object.defineProperties(log, {
      debug: {
        value: (...message) => {
          logger.debug(formatId('').concat(...message));
        }
      },
      info: {
        value: (...message) => {
          logger.info(formatId('').concat(...message));
        }
      },
      error: {
        value: (...message) => {
          logger.error(formatId('').concat(...message));
        }
      },
      log: {
        value: (...message) => {
          logger.log(formatId('').concat(...message));
        }
      },
      child: {
        value: (additionalId: string, newOptions?: string | Options) => {
          if (typeof newOptions === 'string') {
            newOptions = {
              ...options,
              color: newOptions
            };
            moveColor(newOptions);
            return newLog([...id, additionalId], newOptions);
          } else {
            return newLog([...id, additionalId], options);
          }
        }
      }
    });

    return log as Logger;
  };

  return newLog(initialId, initialOptions);
};
