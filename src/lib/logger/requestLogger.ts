export const createRequestData = (req, config) => ({
  time: Date.now(),
  method: req.method,
  path: req.path,
  hostname: req.hostname,
  clientIp:
    req.headers['x-real-ip'] ||
    (
      req.headers['x-forwarded-for'] &&
      req.headers['x-forwarded-for'].replace(/,.*$/, '')
    ) ||
    req.ip ||
    null,
  cookieId:
    (config.cookie && req.cookies && req.cookies[config.cookie.id]) || null,
  userAgent: req.headers['user-agent'] || null,
  referer: req.headers['referer']
});

export const createRequestLogger = (config) => (req, res, next) => {
  /* eslint-disable-next-line no-console */
  console.log(new Date().toISOString(), req.hostname, req.method, req.path);
  next();
};
