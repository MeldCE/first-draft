const keys = ['user', 'draft', 'server', 'token'];

export interface PrivilegesStore {
  user?: string[];
  draft?: string[];
  server?: string[];
  token?: string[];
  combined?: string[];
}

/**
 * Combine Privileges in a PrivilegesStore into a single privileges array
 *
 * @param privileges Priveleges store
 *
 * @returns Combined privileges
 */
export const combinePrivileges = (privileges: PrivilegesStore) => {
  let combined = [];
  let have = false;
  for (let i = 0; i < keys.length; i++) {
    const iPrivileges = privileges[keys[i]];
    if (iPrivileges) {
      have = true;
      for (let j = 0; j < iPrivileges.length; j++) {
        if (combined.indexOf(iPrivileges[j]) === -1) {
          combined.push(iPrivileges[j]);
        }
      }
    }
  }
  if (!have) {
    combined = null;
  }

  return combined;
};

/**
 * Create instance to nicely handle privileges from mulitple sources
 */
export const Privileges = () => {
  let combined = null;
  const privileges: PrivilegesStore = {};

  return {
    get: (type: 'user' | 'draft' | 'server' | 'token') => {
      return privileges[type] || null;
    },
    set: (
      type: 'user' | 'draft' | 'server' | 'token',
      newPrivileges: null | string[]
    ) => {
      privileges[type] = newPrivileges;

      combined = combinePrivileges(privileges);
    },
    delete: (type: 'user' | 'draft' | 'server' | 'token') => {
      privileges[type] = null;

      combined = combinePrivileges(privileges);
    },
    get privileges() {
      return combined;
    }
  };
};
