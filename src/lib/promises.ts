interface SettledPromises<T> extends Array<T> {
  fulfilled?: Array<boolean>;
}

/**
 * Function that will resolve with the results of the given promises once
 * all promises have either been fulfilled or been rejected
 *
 * @param promises Array of promises to wait for
 *
 * @returns A promise that will resolve to an array of the resolts of the
 *   given promises. The array will have an additional property (fulfilled)
 *   of an array of whether each promise resolved or rejected.
 */
export const promisesSettled = (
  promises: Array<Promise<any>>
): Promise<SettledPromises<any>> => {
  if (!promises.length) {
    return Promise.resolve([]);
  }
  return new Promise((resolve, reject) => {
    const results: SettledPromises<any> = Array(promises.length);
    results.fulfilled = Array(promises.length);
    let waiting = promises.length;

    for (let i = 0; i < promises.length; i++) {
      promises[i].then(
        (result) => {
          results[i] = result;
          results.fulfilled[i] = true;
          waiting--;
          if (!waiting) {
            resolve(results);
          }
        },
        (error) => {
          results[i] = error;
          results.fulfilled[i] = false;
          waiting--;
          if (!waiting) {
            resolve(results);
          }
        }
      );
    }
  });
};
