type Listener = (eventData: any) => void;

export interface Eventer {
  /**
   * Add listener to event
   *
   * @param event Event to listen for
   * @param listener Function to call on an event
   *
   * @returns If the listener wasn't already attached and was added
   */
  addEventListener: (event: string, listener: Listener) => boolean;
  /**
   * Remove listener from event
   *
   * @param event Event to remove listener from
   * @param listener Function to remove
   *
   * @returns If the listener was removed
   */
  removeEventListener: (event: string, listener: Listener) => boolean;
  /**
   * Emit an event
   *
   * @param event Event to event
   * @param eventData Event data to emit
   *
   * @returns Whether there were any listeners listening for the event
   */
  emit: (event: string, eventData: any) => boolean;
}

/**
 * Create a eventer
 *
 * @param events Events that the eventer will be producing and can be
 *   listened for
 */
export const createEventer = (events?: Array<string>) => {
  const listeners: { [event: string]: Array<Function> } = {};

  return {
    addEventListener: (event: string, listener: Listener) => {
      if (events && events.indexOf(event) === -1) {
        throw new Error('Unknown event: ' + event);
      }

      if (!listeners[event]) {
        listeners[event] = [];
      }

      if (listeners[event].indexOf(listener) === -1) {
        listeners[event].push(listener);
        return true;
      }

      return false;
    },
    removeEventListener: (event: string, listener: Listener) => {
      if (events && events.indexOf(event) === -1) {
        throw new Error('Unknown event: ' + event);
      }

      if (!listeners[event]) {
        return false;
      }

      const index = listeners[event].indexOf(listener);

      if (index === -1) {
        return false;
      }

      if (listeners[event].length === 1) {
        delete listeners[event];
      } else {
        listeners[event].splice(index, 1);
      }

      return true;
    },
    emit: (event: string, eventData: any) => {
      if (events && events.indexOf(event) === -1) {
        throw new Error('Unknown event: ' + event);
      }

      if (!listeners[event]) {
        return false;
      }

      for (let i = 0; i < listeners[event].length; i++) {
        listeners[event][i](eventData);
      }

      return true;
    }
  } as Eventer;
};
export default createEventer;
