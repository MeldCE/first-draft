const s4 = (): string => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(32)
    .substring(1);
};

/**
 * Make a random id from the characters used in base 32 with the first
 * character being an alphabetic character
 *
 * @param length Length of random string to make
 */
export const makeId = (length: number = 9): string => {
  const iterations = Math.ceil((length - 1) / 3);

  let idString = '';

  let i = Math.round(Math.random() * 48);
  if (i > 24) {
    idString += String.fromCharCode(i + 73); // 97 (a) - 24
  } else {
    idString += String.fromCharCode(i + 65); // 65 (A)
  }
  i = 0;
  while (i++ < iterations) {
    idString += s4();
  }

  if (length % 3) {
    return idString.slice(0, length + 1);
  } else {
    return idString;
  }
};
export default makeId;

/**
 * Clean draft ids by removing/replacing bad characters
 *
 * @param id ID to clean
 */
export const cleanId = (id: string): string => {
  id = decodeURIComponent(id);
  // Remove bad characters
  id = id.replace(/['"]/g, '');
  // Replace other characters
  id = id.replace(/[!-,/:-@[-^`{-~]/g, '_');
  // Single _
  id = id.replace(/_+/g, '_');

  return id;
};
