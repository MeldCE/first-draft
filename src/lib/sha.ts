import * as jsSHA from 'jssha/src/sha256';

export const getSha = (value: string): string => {
  const sha = new (jsSHA.default || jsSHA)('SHA-256', 'TEXT');
  sha.update(value);
  return sha.getHash('HEX');
};

export const signChallenge = (challenge: string, token: string): string => {
  return getSha(challenge + token);
};
