#!/usr/bin/env node
import * as express from 'express';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import { Server as socketIO } from 'socket.io';
import * as http from 'http';
import { resolve, join } from 'path';
import { constants as fsConstants, accessSync } from 'fs';
import getConfig from './modules/config';
import * as urlJoin from 'url-join';
import makeId from '../lib/id';
import { createRequestLogger } from '../lib/logger';

import startDraft from './draft';

const app = express();
app.use(cookieParser());
app.use(compression());
const server = new http.Server(app);
const io = new socketIO(server);

export const start = () =>
  getConfig()
    .then(({ config, appConfig }) => {
      // TODO Make better
      const dist = resolve(__dirname, '../dist');

      // Check if the drafts folder exists
      try {
        accessSync(resolve('drafts'), fsConstants.R_OK | fsConstants.W_OK);
      } catch (error) {
        console.error('Could not access drafts folder');
        process.exit(1);
      }

      if (createRequestLogger) {
        app.use(createRequestLogger(config));
      }

      console.log('Starting first-draft');
      startDraft(app, config, io);

      app.get('*/_config', (req, res, next) => {
        // if (req.header('Referer')
        res.json(appConfig);
      });

      app.get(urlJoin(config.baseUri, config.draftUri, '*'), (req, res) => {
        if (!req.path.endsWith('/')) {
          res.redirect(301, req.path + '/');
        }
        if (
          !req.get('dnt') &&
          config.cookie &&
          (!req.cookies || !req.cookies['c'])
        ) {
          res.cookie(config.cookie.id, makeId(40), {
            expires: new Date(Date.now() + (config.cookie.lifetime || 0) * 1000)
          });
        }
        res.sendFile(join(dist, 'index.html'));
      });

      app.use((err, req, res, next) => {
        console.error(
          `Request to ${req.originalUrl} produced an error:`,
          err.message
        );
        res.status(err.status).end();
      });

      app.use(express.static(dist));

      const port = Number(process.env.PORT) || 7067;
      const host = process.env.HOST || 'localhost';

      server.listen(port, host, () => {
        console.log(`First Draft started on ${host}:${port}`);
      });

      return server;
    })
    .catch((error) => {
      console.error('Error starting first-draft: ' + error.message);
    });

if (require.main === module) {
  process.on('SIGTERM', () => process.exit());
  start();
}
