import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import { Server as socketIO } from 'socket.io';
import * as http from 'http';
import { resolve } from 'path';
import getConfig from './modules/config';
import * as urlJoin from 'url-join';
import makeId from '../lib/id';
import { createRequestLogger } from '../lib/logger';
import { createServer } from 'vite';

import startDraft from './draft';

const app = express();
app.use(cookieParser());
const server = new http.Server(app);
const io = new socketIO(server);

export const start = () =>
  getConfig()
    .then(async ({ config, appConfig }) => {
      const port = Number(process.env.PORT) || 7067;
      const host = process.env.HOST || 'localhost';

      const root = resolve(__dirname, '../app');

      // Create Vite server in middleware mode.
      const vite = await createServer({
        root,
        mode: process.env.NODE_ENV || 'development',
        server: {
          middlewareMode: 'html',
          https: false,
          port,
          host,
          hmr: {
            port: port + 1
          }
        }
      });

      if (createRequestLogger) {
        app.use(createRequestLogger(config));
      }

      console.log('starting first-draft');
      startDraft(app, config, io);

      app.get('*/_config', (req, res, next) => {
        // if (req.header('Referer')
        res.json(appConfig);
      });

      app.get(
        urlJoin(config.baseUri, config.draftUri, '*'),
        (req, res, next) => {
          if (!req.path.endsWith('/')) {
            res.redirect(301, req.path + '/');
          } else {
            if ((config.cookie && !req.cookies) || !req.cookies['c']) {
              res.cookie(config.cookie.id, makeId(40), {
                expires: new Date(
                  Date.now() + (config.cookie.lifetime || 0) * 1000
                )
              });
            }
            next();
          }
        }
      );

      app.use((err, req, res, next) => {
        console.error(
          `Request to ${req.originalUrl} produced an error:`,
          err.message
        );
        res.status(err.status).end();
      });

      console.log('Adding vite middleware');
      app.use(vite.middlewares);

      server.listen(port, host, () => {
        console.log(`First Draft started on ${host}:${port}`);
      });
    })
    .catch((error) => {
      console.error('Error starting first-draft: ' + error.message);
    });

if (require.main === module) {
  process.on('SIGTERM', () => process.exit());
  start();
}
