import { Config } from '../typings/Config';
import { Draft } from '../typings/Draft';
import { Instance, Storage } from '../typings/Storage';
import { Socket } from 'socket.io';

import { createDraftHandler } from './modules/draft';
import { createFilesHandler } from './modules/files';
import { createUserHandler } from './modules/user';
import { createStorage } from './storage/file';
import { Eventer, createEventer } from '../lib/eventer';
import { Logger, createLogger, createSocketLogger } from '../lib/logger';

export interface Context {
  config: Config;
  storage: Storage;
  log: Logger;
  events: Eventer;
}

export interface SocketContext extends Context {
  socket: Socket;
  draftId: string | null;
  draftStore: Instance | null;
  draft: Draft | null;
  user: any;
}

export const startDraft = (app, config: Config, io) => {
  const context: Context = {
    config,
    storage: createStorage(config),
    log: createLogger(console, ['first-draft'], { color: 'brown' }),
    events: createEventer()
  };
  const filesHandler = createFilesHandler(app, context);
  const userHandler = createUserHandler(app, context);
  const draftHandler = createDraftHandler(app, context);

  io.on('connection', (socket) => {
    const socketContext: SocketContext = {
      ...context,
      events: createEventer(),
      socket: null,
      draftStore: null,
      draftId: null,
      draft: null,
      user: null
    };

    if (createSocketLogger) {
      socketContext.socket = createSocketLogger(socket, socketContext);
    } else {
      socketContext.socket = socket;
    }

    draftHandler.addDraftEvents(socketContext);
    userHandler.connection(socketContext);
    filesHandler.addEventHandlers(socketContext);
  });
};
export default startDraft;
