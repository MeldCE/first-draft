import { Config } from '../../typings/Config';
import { DraftMessage, DraftUpdateMessage } from '../../typings/Message';
import { Storage } from '../../typings/Storage';

import * as fs from 'fs';
import * as path from 'path';
import Queue from 'rowdy-fs';
import { promisify } from 'util';
import { mergeDraftUpdate, DraftError } from '../../lib/draft';

const access = promisify(fs.access);
const mkdir = promisify(fs.mkdir);
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const unlink = promisify(fs.unlink);
const open = promisify(fs.open);
const close = promisify(fs.close);

export const createStorage = (config: Config): Storage => {
  // TODO Change drafts folder to a config entry on server only (draftsDir)
  const draftsDir = path.resolve('drafts');
  const cachedDrafts: {
    [id: string]: { queue: Queue; subscribers: number };
  } = {};

  const haveDraft = (id: string) => {
    return access(path.join(draftsDir, id), fs.constants.R_OK).then(
      () => {
        return Promise.resolve(true);
      },
      (error) => {
        if (error.code === 'ENOENT') {
          return Promise.resolve(false);
        }

        return Promise.reject(error);
      }
    );
  };

  /**
   * Check a filename is valid. If, it isn't return a string with why
   *
   * @param filename Filename to check
   *
   * @returns null if value, invalid reason otherwise
   */
  const checkFilename = (filename: string): null | string => {
    if (filename.indexOf('.' + path.sep) !== -1) {
      return 'no director navigation paths allowed';
    }

    return null;
  };

  const draftHasFile = (id: string, filename: string) => {
    const invalid = checkFilename(filename);
    if (invalid) {
      return Promise.reject(new Error(invalid));
    }

    if (filename === 'draft.json') {
      return Promise.reject(new Error('filename is protected'));
    }

    return access(path.join(draftsDir, id, filename), fs.constants.R_OK).then(
      () => true,
      (error) => {
        if (error.code === 'ENOENT') {
          return Promise.resolve(false);
        }
        return Promise.reject(error);
      }
    );
  };

  const draftFile = (id: string) => path.join(draftsDir, id, 'draft.json');
  const draftFolder = (id: string) => path.join(draftsDir, id);

  const storageInstance = {
    haveDraft,
    loadDraft: (id: string) => {
      return haveDraft(id)
        .then((have) => {
          if (!have) {
            return mkdir(draftFolder(id));
          }
        })
        .then(() => {
          let cachedDraft;
          if (!cachedDrafts[id]) {
            cachedDraft = new Queue(draftFile(id), {
              pauseOnWriteError: true
            });
            cachedDrafts[id] = {
              queue: cachedDraft,
              subscribers: 1
            };
          } else {
            cachedDraft = cachedDrafts[id].queue;
            cachedDrafts[id].subscribers++;
          }

          const instance = {
            close() {
              if (cachedDraft) {
                cachedDrafts[id].subscribers--;
                if (!cachedDrafts[id].subscribers) {
                  delete cachedDrafts[id];
                }
              }
            },
            read() {
              return cachedDraft.read().then((content) => {
                try {
                  const draft = JSON.parse(content);
                  // TODO Validate draft with schema
                  return draft;
                } catch (error) {
                  return Promise.reject(
                    `Error reading draft: ${error.message}`
                  );
                }
              });
            },
            save(data) {
              return cachedDraft
                .write(JSON.stringify(data.draft))
                .then(() => data.draft);
            },
            update(data: DraftUpdateMessage) {
              let draft;
              return cachedDraft
                .modify((contents) => {
                  if (!contents) {
                    return Promise.reject('No draft to update');
                  }

                  // Convert to Object
                  try {
                    draft = JSON.parse(contents);
                  } catch (error) {
                    return Promise.reject('Error parsing existing drawing');
                  }

                  draft = mergeDraftUpdate(draft, data.data);

                  return JSON.stringify(draft);
                })
                .then(() => draft)
                .catch((error) => {
                  cachedDraft.clearError();
                  return Promise.reject(error);
                });
            },
            modify(data: DraftMessage) {
              let draft;
              return cachedDraft
                .modify((contents) => {
                  if (!contents) {
                    return Promise.reject('No draft to update');
                  }

                  // Convert to Object
                  try {
                    draft = JSON.parse(contents);
                  } catch (error) {
                    return Promise.reject('Error parsing existing drawing');
                  }

                  if (!draft || !draft.drawings || !draft.drawings[0]) {
                    return Promise.reject('No drawing');
                  }

                  const drawing = draft.drawings[0];

                  const checkElementForDelete = (element) => {
                    // Check if the element is a raster
                    if (element.type === 'Raster') {
                      // TODO config for files?
                      if (element.source.startsWith('files/')) {
                        // Check if there are any other instances using
                        // the same file
                        const another = drawing.layers.find((layer) => {
                          return layer.children.find((child) => {
                            if (child !== element) {
                              if (
                                child.type === 'Raster' &&
                                child.source === element.source
                              ) {
                                return true;
                              }
                            }
                          });
                        });

                        if (!another) {
                          // Delete the image
                          const filename = decodeURI(
                            element.source.slice('files/'.length)
                          );

                          // Sanity check file has no /
                          if (filename.indexOf('/') === -1) {
                            storageInstance.removeFile(id, filename);

                            // Check for thumbnails to delete
                            if (config.thumbnails) {
                              config.thumbnails.forEach((thumbnail) => {
                                const thumbnailFilename = path.join(
                                  'thumbs',
                                  thumbnail.thumbnailBigDimension +
                                    '_' +
                                    filename
                                );
                                storageInstance
                                  .draftHasFile(id, thumbnailFilename)
                                  .then((has) => {
                                    if (has) {
                                      return storageInstance.removeFile(
                                        id,
                                        thumbnailFilename
                                      );
                                    }
                                  });
                              });
                            }
                          }
                        }
                      }
                    }
                  };

                  let index;
                  let layer;

                  switch (data.action) {
                    case 'create':
                      if (data.element.commit === false) {
                        break;
                      }

                      layer = drawing.layers.find(
                        (layer) => layer.name === data.layerName
                      );

                      if (!layer) {
                        return Promise.reject(
                          new DraftError('Could not find layer', {
                            draftId: id,
                            layerName: data.layerName
                          })
                        );
                      }

                      index = layer.children.findIndex(
                        (element) => element.name === data.element.name
                      );

                      if (index !== -1) {
                        return Promise.reject(
                          new DraftError('Duplicate element ID', {
                            draftId: id,
                            layerName: data.layerName,
                            elementId: data.element.name
                          })
                        );
                      }

                      layer.children.push(data.element);
                      break;
                    case 'modify':
                      if (data.element.commit === false) {
                        break;
                      }

                      layer = drawing.layers.find(
                        (layer) => layer.name === data.layerName
                      );

                      if (!layer) {
                        return Promise.reject(
                          new DraftError('Could not find layer', {
                            draftId: id,
                            layerName: data.layerName
                          })
                        );
                      }

                      index = layer.children.findIndex(
                        (element) => element.name === data.element.name
                      );

                      if (index === -1) {
                        return Promise.reject(
                          new DraftError('Could not find element', {
                            draftId: id,
                            layerName: data.layerName,
                            elementId: data.element.name
                          })
                        );
                      }

                      layer.children[index] = data.element;
                      break;
                    case 'modifyMultiple': {
                      for (let i = 0; i < data.elements.length; i++) {
                        const element = data.elements[i];
                        if (element.element.commit === false) {
                          continue;
                        }

                        layer = drawing.layers.find(
                          (layer) => layer.name === element.layerName
                        );

                        if (!layer) {
                          return Promise.reject(
                            new DraftError('Could not find layer', {
                              draftId: id,
                              layerName: element.layerName
                            })
                          );
                        }

                        index = layer.children.findIndex(
                          (child) => child.name === element.element.name
                        );

                        if (index === -1) {
                          return Promise.reject(
                            new DraftError('Could not find element', {
                              draftId: id,
                              layerName: element.layerName,
                              elementId: element.element.name
                            })
                          );
                        }

                        layer.children[index] = element.element;
                      }
                      break;
                    }
                    case 'position': {
                      for (let i = 0; i < data.elements.length; i++) {
                        const element = data.elements[i];

                        const layer = drawing.layers.find(
                          (layer) => layer.name === element.layerName
                        );
                        if (!layer) {
                          return Promise.reject(
                            new DraftError('Could not find layer', {
                              draftId: id,
                              layerName: element.layerName
                            })
                          );
                        }

                        const item = layer.children.find(
                          (item) => item.name === element.elementName
                        );

                        if (!item) {
                          return Promise.reject(
                            new DraftError('Could not find element', {
                              draftId: id,
                              layerName: element.layerName,
                              elementId: element.elementName
                            })
                          );
                        }

                        item.position = element.position;
                      }
                      break;
                    }
                    case 'deleteMultiple': {
                      for (let i = 0; i < data.elements.length; i++) {
                        const element = data.elements[i];

                        const layer = drawing.layers.find(
                          (layer) => layer.name === element.layerName
                        );
                        if (!layer) {
                          return Promise.reject(
                            new DraftError('Could not find layer', {
                              draftId: id,
                              layerName: element.layerName
                            })
                          );
                        }

                        const index = layer.children.findIndex(
                          (item) => item.name === element.elementName
                        );

                        if (index === -1) {
                          // Element doesn't exist already so ignore
                          continue;
                        }

                        checkElementForDelete(layer.children[index]);

                        layer.children.splice(index, 1);
                      }
                      break;
                    }
                    case 'delete':
                      if (data.element.commit === false) {
                        break;
                      }

                      layer = drawing.layers.find(
                        (layer) => layer.name === data.layerName
                      );
                      index = layer.children.findIndex(
                        (element) => element.name === data.elementName
                      );

                      if (index !== -1) {
                        checkElementForDelete(layer.children[index]);

                        layer.children.splice(index, 1);
                      }
                      break;
                    case 'createLayer':
                      index = drawing.layers.findIndex(
                        (layer) => layer.name === data.layer.name
                      );

                      if (index !== -1) {
                        return Promise.reject(
                          new DraftError('Layer already exists', {
                            draftId: id,
                            layerName: data.layerName
                          })
                        );
                      }

                      drawing.layers.push(data.layer);
                      break;
                    case 'modifyLayer':
                      index = drawing.layers.findIndex(
                        (layer) => layer.name === data.layer.name
                      );

                      if (index === -1) {
                        return Promise.reject(
                          new DraftError('Could not find layer', {
                            draftId: id,
                            layerName: data.layerName
                          })
                        );
                      }

                      drawing.layers[index] = {
                        ...drawing.layers[index],
                        ...data.layer
                      };
                      break;
                    case 'deleteLayer':
                      index = drawing.layers.findIndex(
                        (layer) => layer.name === data.layerName
                      );

                      if (index === -1) {
                        return Promise.reject(
                          new DraftError('Could not find layer', {
                            draftId: id,
                            layerName: data.layerName
                          })
                        );
                      }

                      drawing.layers.splice(index, 1);
                      break;
                  }

                  if (data.user) {
                    // TODO Add last modified?
                  }

                  return JSON.stringify(draft);
                })
                .then(() => draft)
                .catch((error) => {
                  cachedDraft.clearError();
                  return Promise.reject(error);
                });
            }
          };

          return instance;
        });
    },
    draftHasFile,
    storeFile: (
      id: string,
      filename: string,
      contents: any,
      overwrite?: boolean
    ) => {
      const invalid = checkFilename(filename);
      if (invalid) {
        return Promise.reject(new Error(invalid));
      }

      let promise;
      // Check folder exists
      if (filename.indexOf(path.sep) !== -1) {
        const dir = path.join(draftsDir, id, path.dirname(filename));
        promise = access(dir, fs.constants.W_OK).catch((error) => {
          if (error.code === 'ENOENT') {
            return mkdir(dir, { recursive: true });
          } else {
            return Promise.reject(error);
          }
        });
      } else {
        promise = Promise.resolve();
      }

      return promise
        .then(() =>
          open(path.join(draftsDir, id, filename), 'w' + (overwrite ? '' : 'x'))
        )
        .then(
          (fd) => {
            writeFile(fd, contents);
            return close(fd);
          },
          (error) => {
            if (error.code === 'EEXIST') {
              return Promise.reject(new Error('File already exists'));
            } else {
              return Promise.reject(error);
            }
          }
        );
    },
    getFile: (id: string, filename: string) => {
      return draftHasFile(id, filename).then((haveFile) => {
        if (!haveFile) {
          return Promise.resolve(undefined);
        }

        return readFile(path.join(draftsDir, id, filename));
      });
    },
    removeFile: (id: string, filename: string) => {
      return draftHasFile(id, filename).then((haveFile) => {
        if (!haveFile) {
          return Promise.resolve(undefined);
        }

        return unlink(path.join(draftsDir, id, filename));
      });
    }
  };
  return storageInstance;
};
