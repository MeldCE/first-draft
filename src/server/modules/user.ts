import { Draft } from '../../typings/Draft';

import {
  badChallenge,
  notEnabled,
  tokenCookie,
  unknownAuth,
  unknownUser
} from '../../lib/definitions';
import makeId from '../../lib/id';
import {
  combinePrivileges,
  Privileges,
  PrivilegesStore
} from '../../lib/privileges';
import * as sha from '../../lib/sha';
import { timedStore, TimedStore } from '../../lib/timedStore';
import { SocketContext, Context } from '../draft';
import { UserConfig } from '../../typings/User';
import { arrayContainsValues, removeValuesFromArray } from '../../lib/utils';
import { AuthResult } from '../../typings/Message';

interface Challenge {
  nonce?: string;
  data?: any;
}

interface Operation {
  permissions?: string[];
  promise: {
    resolve: (value: any) => void;
    reject: (error: any) => void;
  };
  challenge: string;
}

import { resolve } from 'path';
let users: UserConfig;
const tokenLifetime = 600;
const challengesLifetime = 60;

try {
  users = require(resolve('users.json'));
  if (typeof users === 'object') {
    Object.entries(users).forEach(([username, user]) => {
      user.username = username;
    });
  } else {
    users = {};
  }
} catch (error) {
  users = {};
}

export const createUserHandler = (expressApp, context: Context) => {
  return {
    connection: (socketContext: SocketContext) => {
      let user = null;
      // Stores the calculated privileges from the current draft (privileges
      // have to be reset after every draft change). Used, along with user to
      // determine what authentication has been given
      const privileges = Privileges();
      const challenges: TimedStore<Challenge> = timedStore(challengesLifetime);
      const operations = [];
      let operationChallenge: null | {
        requirePassword: boolean;
        challenge: string;
      } = null;

      /**
       * Create a new authentication challenge for the socket
       *
       * @param data Data to store with the authentication challenge
       */
      const createChallenge = (data?: { [key: string]: any }) => {
        // Make an id
        let id = null;
        for (let i = 0; i < 5; i++) {
          id = makeId();
          if (!challenges.have(id)) {
            break;
          }
          id = null;
        }

        if (id === null) {
          throw new Error('Could not create a new challenge');
        }

        const challenge = {
          challenge: id,
          nonce: makeId()
        };

        challenges.create(id, {
          nonce: challenge.nonce,
          data
        });

        return challenge;
      };

      /**
       * Send a authentication challenge nonce to the client
       *
       * @param id ID to use in the message emitted
       * @param data Data to be sent with the challenge
       *
       * @returns Challenge ID
       */
      const sendChallenge = (
        id?: string | null,
        data?: { [key: string]: any }
      ) => {
        // TODO Check haven't reached challenge limit
        // Create a new challenge
        const challenge = {
          ...data,
          id: null,
          ...createChallenge()
        };

        if (!id) {
          id = makeId();
        }

        challenge.id = id;

        socketContext.socket.emit(
          context.config.baseEvent + 'auth:challenge',
          challenge
        );

        return challenge.challenge;
      };

      /**
       * Set and send and new operation challenge
       *
       * @param requirePassword Whether or not a password is required. If not
       *   given, the requirePassword from the previous operationChallenge will
       *   be used or false
       */
      const newOperationChallenge = (requirePassword?: boolean) => {
        requirePassword = typeof requirePassword
          ? requirePassword
          : operationChallenge
          ? operationChallenge.requirePassword
          : false;

        operationChallenge = {
          requirePassword,
          challenge: sendChallenge(null, { requirePassword })
        };
      };

      /**
       * Check any waiting operations for if the required privileges are now
       * met
       */
      const checkWaitingOperations = (challenge?: string) => {
        if (
          challenge &&
          operationChallenge &&
          operationChallenge.challenge === challenge
        ) {
          operationChallenge = null;
        }

        if (operations.length) {
          let needNewChallenge = null;
          let i = 0;
          while (i < operations.length) {
            if (
              !operations[i].permissions ||
              hasPermissions(operations[i].permissions)
            ) {
              operations[i].promise.resolve(true);
              operations.splice(i, 1);
              continue;
            }

            needNewChallenge =
              needNewChallenge || Boolean(operations[i].permissions);
            i++;
          }

          if (needNewChallenge !== null) {
            if (
              !operationChallenge ||
              (needNewChallenge && !operationChallenge.requirePassword)
            ) {
              newOperationChallenge(needNewChallenge);
            }
          }
        }
      };

      /**
       * Handle changes in draft
       */
      const changeDraft = ({ oldDraftId, newDraftId }) => {
        if (newDraftId !== oldDraftId) {
          if (oldDraftId) {
            context.events.removeEventListener(
              `draft:${oldDraftId}:update`,
              checkPrivilegesFromUpdate
            );
          }
          if (newDraftId) {
            context.events.addEventListener(
              `draft:${newDraftId}:update`,
              checkPrivilegesFromUpdate
            );
          }
        }
      };

      socketContext.events.addEventListener('draft:change', changeDraft);

      /**
       * Check if privileges have been changed in the draft update
       *
       * @param changes Draft changes
       */
      const checkPrivilegesFromUpdate = ({ changes }) => {
        if (changes && changes.config) {
          let updatePrivileges = false;
          if (context.config.draftEditPasswords) {
            if (changes.config.hasOwnProperty('editPassword')) {
              if (!changes.config.editPassword) {
                // Password remove
                if (context.config.editPassword && privileges.get('server')) {
                  // Recalculate server editPassword privileges
                  privileges.set(
                    'server',
                    context.config.editPrivileges || ['edit']
                  );
                }
                privileges.delete('draft');
                updatePrivileges = true;
              } else {
                // New password
                if (privileges.get('draft')) {
                  // Request reauthorization
                  sendChallenge(null, {
                    draftId: socketContext.draftId,
                    message:
                      'Draft edit password has changed. Please enter the new password',
                    type: 'draftEdit'
                  });
                }
                privileges.delete('draft');
                updatePrivileges = true;
              }
            } else if (changes.config.hasOwnProperty('editPrivileges')) {
              if (privileges.get('draft')) {
                privileges.set(
                  'draft',
                  socketContext.config.editPrivileges || ['edit']
                );
              }
              if (privileges.get('server')) {
                privileges.set(
                  'server',
                  removeValuesFromArray(
                    context.config.editPrivileges || ['edit'],
                    socketContext.config.editPrivileges || ['edit']
                  )
                );
              }
              updatePrivileges = true;
            }
          }

          if (updatePrivileges) {
            socketContext.socket.emit(
              context.config.baseEvent + 'auth:update',
              {
                id: makeId(),
                privileges: privileges.privileges
              }
            );
          }
        }
      };

      /**
       * Calculate privileges for a given draft
       *
       * @param draft
       *
       * @returns The calculated privileges
       */
      const recalculatePrivileges = (draft: Draft) => {
        const calculatedPrivileges: PrivilegesStore = {};

        if (user && user.privileges) {
          calculatedPrivileges.user = user.privileges;
        }

        if (
          context.config.draftEditPasswords &&
          draft.config &&
          draft.config.editPassword
        ) {
          if (context.config.editPassword) {
            calculatedPrivileges.server = removeValuesFromArray(
              context.config.editPrivileges || ['edit'],
              draft.config.editPrivileges || ['edit']
            );
          }
          calculatedPrivileges.draft = draft.config.editPrivileges || ['edit'];
        } else if (context.config.editPassword) {
          calculatedPrivileges.server = context.config.editPrivileges || [
            'edit'
          ];
        } else if (
          !context.config.editPassword &&
          (!context.config.draftEditPasswords ||
            !(draft.config && draft.config.editPassword))
        ) {
          calculatedPrivileges.server = ['edit'];
        }

        calculatedPrivileges.combined = combinePrivileges(calculatedPrivileges);

        return calculatedPrivileges;
      };

      // Emit a new challenge if requested by the client
      socketContext.socket.on(
        context.config.baseEvent + 'auth:challenge',
        (data) => {
          // Ignore if don't have an id
          if (!data.id) {
            return;
          }

          sendChallenge(data.id);
        }
      );

      // Check the authentication is correct
      socketContext.socket.on(
        context.config.baseEvent + 'auth:check',
        (data) => {
          // Ignore if don't have an id
          if (!data.id) {
            return;
          }

          // Send a challenge if don't have a challenge
          if (!data.challenge) {
            sendChallenge(data.id);
            return;
          }

          // Send a not enabled if draft edit passwords are not enabled
          if (
            data.type === 'draftEdit' &&
            !(
              context.config.draftEditPasswords &&
              socketContext.draft &&
              socketContext.draft.config &&
              socketContext.draft.config.editPassword
            )
          ) {
            socketContext.socket.emit(context.config.baseEvent + 'auth:check', {
              id: data.id,
              result: null,
              code: notEnabled
            });
            return;
          }

          // Check the challenge
          const challenge = challenges.get(data.challenge);

          if (!challenge) {
            socketContext.socket.emit(context.config.baseEvent + 'auth:check', {
              id: data.id,
              code: badChallenge
            });
            return;
          }

          let hash;
          switch (data.type) {
            case 'user':
              const sentUser = users[data.username];

              if (!sentUser) {
                socketContext.socket.emit(
                  context.config.baseEvent + 'auth:check',
                  {
                    id: data.id,
                    result: false,
                    code: unknownUser
                  }
                );
                return;
              }

              hash = sentUser.passwordHash;
              break;
            case 'edit':
              hash =
                context.config.editPassword &&
                sha.getSha(context.config.editPassword);
              break;
            case 'draftEdit':
              hash =
                socketContext.draft &&
                socketContext.draft.config &&
                socketContext.draft.config.editPassword;
              break;
            default:
              socketContext.socket.emit(
                context.config.baseEvent + 'auth:check',
                {
                  id: data.id,
                  result: false,
                  code: unknownAuth
                }
              );
              return;
          }

          if (!hash) {
            socketContext.socket.emit(context.config.baseEvent + 'auth:check', {
              id: data.id,
              result: false
            });
            return;
          }

          const answer = sha.signChallenge(challenge.nonce, hash);

          socketContext.socket.emit(context.config.baseEvent + 'auth:check', {
            id: data.id,
            result: data.answer === answer
          });
        }
      );

      socketContext.socket.on(
        context.config.baseEvent + 'auth:authenticate',
        (data) => {
          // Ignore if don't have an id
          if (!data.id) {
            return;
          }

          // Check the user exists
          const sentUser = users[data.username];

          if (
            !data.username &&
            !context.config.editPassword &&
            !(
              context.config.draftEditPasswords &&
              socketContext.draft &&
              socketContext.draft.config &&
              socketContext.draft.config.editPassword &&
              !(operationChallenge && operationChallenge.requirePassword)
            )
          ) {
            socketContext.socket.emit(
              context.config.baseEvent + 'auth:result',
              {
                id: data.id,
                success: true,
                code: notEnabled
              }
            );
            return;
          }

          // Allow setting of name without authentication if user doesn't exist
          if (!sentUser && !data.challenge) {
            socketContext.socket.emit(
              context.config.baseEvent + 'auth:result',
              {
                id: data.id,
                success: true,
                noExist: true,
                user: {
                  name: data.username
                }
              }
            );
            return;
          }

          if (!data.challenge) {
            sendChallenge(data.id);
            return;
          }

          // Check the challenge
          const challenge = data.challenge && challenges.get(data.challenge);

          if (!challenge) {
            socketContext.socket.emit(
              context.config.baseEvent + 'auth:result',
              {
                id: data.id,
                code: badChallenge
              }
            );
            return;
          }

          // Delete the challenge
          challenges.delete(data.challenge);

          let editPassword = false;
          let draftEditPassword = false;
          let hash =
            sentUser &&
            (sentUser.passwordHash || sha.getSha(sentUser.password));
          let success;
          let auth;
          if (hash) {
            auth = 'user';
            success = data.answer === sha.signChallenge(challenge.nonce, hash);
          } else {
            hash =
              socketContext.draft &&
              socketContext.draft.config &&
              socketContext.draft.config.editPassword;

            if (hash) {
              editPassword = true;
              draftEditPassword = true;
              auth = 'draft';
              success =
                data.answer === sha.signChallenge(challenge.nonce, hash);
            }

            if (!hash || !success) {
              hash =
                context.config.editPassword &&
                sha.getSha(context.config.editPassword);

              if (hash) {
                editPassword = true;
                auth = 'server';
                success =
                  data.answer === sha.signChallenge(challenge.nonce, hash);
              }
            }
          }

          if (success) {
            if (editPassword) {
              if (draftEditPassword) {
                if (auth === 'server') {
                  privileges.set(
                    'server',
                    removeValuesFromArray(
                      context.config.editPrivileges || ['edit'],
                      socketContext.draft.config.editPrivileges || ['edit']
                    )
                  );
                } else {
                  privileges.set(
                    'draft',
                    socketContext.draft.config.editPrivileges || ['edit']
                  );
                }
              } else if (auth === 'server') {
                privileges.set(
                  'server',
                  context.config.editPrivileges || ['edit']
                );
              }
              user = {
                name: data.username
              };
            } else if (auth === 'user') {
              user = {
                id: sentUser.username,
                name: sentUser.name,
                username: sentUser.username
              };
              privileges.set(
                'user', // TODO Use (draft) config permissions
                sentUser.privileges || ['edit']
              );
            }

            const response: AuthResult = {
              id: data.id,
              success: true,
              user,
              privileges: privileges.privileges,
              draftId: null
            };

            if (user.username) {
              response.user = user;
            } else if (user.name) {
              response.noUser = true;
            }

            if (auth === 'draft') {
              response.draftId = socketContext.draftId;
            }

            socketContext.socket.emit(
              context.config.baseEvent + 'auth:result',
              response
            );

            // Clear any operations requiring new privileges
            checkWaitingOperations();
          } else {
            const response: AuthResult = {
              id: data.id,
              success: false
            };
            if (auth === 'user') {
              response.username = data.username;
            }
            socketContext.socket.emit(
              context.config.baseEvent + 'auth:result',
              response
            );

            if (operationChallenge) {
              newOperationChallenge();
            }
          }
        }
      );

      // TODO Send user details
      socketContext.socket.on(
        context.config.baseEvent + 'auth:userDetails',
        (data) => {}
      );

      socketContext.socket.on('disconnect', () => {
        socketContext.events.removeEventListener('draft:change', changeDraft);
        if (socketContext.draftId) {
          context.events.removeEventListener(
            `draft:${socketContext.draftId}:update`,
            checkPrivilegesFromUpdate
          );
        }
      });

      /**
       * Check if the current user has given permissions for the
       * current draft
       *
       * @param permissions Permissions to check the user has
       *
       * @returns A boolean of if the user has permissions or null if
       *   there is no draft open
       */
      const hasPermissions = (
        permissions: string | string[]
      ): null | boolean => {
        if (!socketContext.draft) {
          return null;
        }

        if (!Array.isArray(permissions)) {
          permissions = [permissions];
        }

        if (
          permissions.length === 1 &&
          permissions[0] === 'edit' &&
          !(
            context.config.editPassword ||
            (context.config.draftEditPasswords &&
              socketContext.draft &&
              socketContext.draft.config &&
              socketContext.draft.config.editPassword)
          )
        ) {
          return true;
        }

        if (!user) {
          return false;
        }

        return (
          (privileges.privileges &&
            arrayContainsValues(privileges.privileges, permissions)) ||
          false
        );
      };

      /**
       * Check if the current user has the given permissions for the
       * given draft
       *
       * @param permissions Permissions to check the user has
       * @param draftId Draft ID to check the user has permissions for. If
       *   not given, the current draft will be checked
       *
       * @returns A boolean of if the user has permissions or a Promise that
       *   resolves to the same once it has been determined
       */
      const hasPermissionsOnDraft = (
        permissions: string | string[],
        draftId: string
      ): Promise<boolean> => {
        if (draftId === socketContext.draftId) {
          return Promise.resolve(hasPermissions(permissions));
        }

        return context.storage.loadDraft(draftId).then((instance) => {
          return instance.read().then((draft) => {
            instance.close();

            const calculatedPrivileges = recalculatePrivileges(draft);

            return (
              arrayContainsValues(calculatedPrivileges.combined, permissions) ||
              false
            );
          });
        });
      };

      // Create socket context user object
      socketContext.user = {};

      Object.defineProperties(socketContext.user, {
        hasPermissions: {
          value: hasPermissions
        },
        hasPermissionsOnDraft: {
          value: hasPermissionsOnDraft
        },
        requestAuthentication: {
          value: (permissions?: string | string[]) => {
            if (permissions && !Array.isArray(permissions)) {
              permissions = [permissions];
            }

            // Check user doesn't already have permissions
            if (hasPermissions(permissions)) {
              return Promise.resolve(true);
            }

            const challengeData = { promise: null, permissions };

            const promise = new Promise((resolve, reject) => {
              challengeData.promise = { resolve, reject };
            });

            const requirePassword = Boolean(permissions);

            // Check if have current operation challenge and it is
            // requiring a password if permissions are needed, if not create
            // a new one
            if (operationChallenge) {
              if (requirePassword && !operationChallenge.requirePassword) {
                newOperationChallenge(requirePassword);
              }
            } else {
              newOperationChallenge(requirePassword);
            }

            operations.push(challengeData);

            return promise;
          }
        },
        user: {
          get() {
            return user;
          }
        }
      });
    }
  };
};
