import { nonceParameter } from '../../lib/definitions';
import { makeId, cleanId } from '../../lib/id';
import { timedStore } from '../../lib/timedStore';
import { cleanFilename } from '../../lib/utils';
import { Context, SocketContext } from '../draft';
import { getType } from 'mime';
import { join } from 'path';
import sharp = require('sharp');
import bodyParser = require('body-parser');

const uploadNonceLifetime = 120;

export const createFilesHandler = (app, context: Context) => {
  const uploadNonces = timedStore(uploadNonceLifetime);
  /// Move limit into global config (for use in client)
  const rawParser = bodyParser.raw({ limit: '10mb', type: 'image/*' });

  app.put('/d/:draftId/files/:filename', rawParser, (req, res) => {
    const draftId = cleanId(req.params.draftId);
    const filename = cleanFilename(req.params.filename);
    // Check for an upload nonce
    const nonce =
      req.get(nonceParameter) || (req.query && req.query[nonceParameter]);
    if (nonce) {
      // Check the nonce is valid
      if (!uploadNonces.have(nonce)) {
        res.status(403).json({ error: 'Please try again.' }).end();
        return;
      }
      uploadNonces.delete(nonce);

      // Check the draft exists
      context.storage.haveDraft(draftId).then(
        (draftExists) => {
          if (draftExists) {
            // Ensure that the file does not already exist
            context.storage.draftHasFile(draftId, filename).then((exists) => {
              if (exists) {
                res.status(409).json({ message: 'File already exists' }).end();
              } else {
                context.storage.storeFile(draftId, filename, req.body).then(
                  () => {
                    res.status(201).end();
                  },
                  (error) => {
                    res.status(500).json({ error: error.message });
                  }
                );
              }
            });
          } else {
            res.status(404).json({ error: 'Draft does not exist' }).end();
          }
        },
        (error) => {
          res.status(500).json({ error: error.message }).end();
        }
      );
    } else {
      res.status(403).json({ error: 'Please try again' }).end();
    }
  });

  app.patch('/d/:draftId/files/:filename', rawParser, (req, res) => {
    const draftId = cleanId(req.params.draftId);
    const filename = cleanFilename(req.params.filename);
    // Check for an upload nonce
    const nonce =
      req.get(nonceParameter) || (req.query && req.query[nonceParameter]);
    if (nonce) {
      if (!uploadNonces.have(nonce)) {
        res.status(403).end();
        return;
      }
      uploadNonces.delete(nonce);

      // Check the draft exists
      context.storage.haveDraft(draftId).then(
        (draftExists) => {
          if (draftExists) {
            // Ensure that the file does not already exist
            context.storage.storeFile(draftId, filename, req.body).then(
              () => {
                res.status(202).end();
              },
              (error) => {
                res.status(500).json({ error: error.message });
              }
            );
          } else {
            res.status(403).end();
          }
        },
        (error) => {
          res.status(500).json({ error: error.message }).end();
        }
      );
    } else {
      res.status(403).end();
    }
  });

  app.get('/d/:draftId/files/:filename', (req, res, next) => {
    const draftId = cleanId(req.params.draftId);
    const filename = cleanFilename(req.params.filename);

    // Check for an upload token
    //const token = req.cookies && req.cookies[tokenCookie];
    //

    context.storage.getFile(draftId, filename).then((file) => {
      if (!file) {
        res.status(404).end();
        return;
      }

      res.set('Content-Type', getType(filename));
      res.send(file).end();
    });
  });

  app.get('/d/:draftId/thumbs/files/:filename', (req, res, next) => {
    const size = Math.min(4000, parseInt(req.query.size)) || 1000;
    const draftId = cleanId(req.params.draftId);
    const filename = cleanFilename(req.params.filename);
    const thumb = join('thumbs', size + '_' + filename);

    // Check for an upload token
    //const token = req.cookies && req.cookies[tokenCookie];

    // Check for the thumbnail first
    context.storage
      .draftHasFile(draftId, thumb)
      .then((has) => {
        if (!has) {
          return context.storage
            .draftHasFile(draftId, filename)
            .then(
              (has) => {
                if (!has) {
                  return false;
                }

                // Make a thumbnail from the original
                return context.storage
                  .getFile(draftId, filename)
                  .then((file) => {
                    const image = sharp(file);
                    return image.metadata().then((metadata) => {
                      if (metadata.width < size || metadata.heigt < size) {
                        return image.toBuffer();
                      }

                      let width;
                      let height;
                      let ratio;

                      if ([6, 8, 5, 7].indexOf(metadata.orientation) !== -1) {
                        width = metadata.height;
                        height = metadata.width;
                      } else {
                        width = metadata.width;
                        height = metadata.height;
                      }
                      if (width / size > height / size) {
                        ratio = size / width;
                        width = size;
                        height = Math.floor(height * ratio);
                      } else {
                        ratio = size / height;
                        height = size;
                        width = Math.floor(width * ratio);
                      }

                      return image
                        .rotate()
                        .resize({
                          width,
                          height,
                          fit: 'contain'
                        })
                        .toBuffer();
                    });
                  })
                  .then((thumbnail) => {
                    return context.storage
                      .storeFile(draftId, thumb, thumbnail)
                      .then(() => {
                        return thumbnail;
                      });
                  });
              },
              (error) => {
                res.status(400).end();
                return Promise.resolve();
              }
            )
            .catch((error) => {
              console.error('caught error', error);
              res.status(500).end();
            });
        } else {
          return context.storage.getFile(draftId, thumb);
        }
      })
      .then((file) => {
        if (!file) {
          res.status(404).end();
          return;
        }

        res.set('Content-Type', getType(thumb));
        res.send(file).end();
      });
  });

  return {
    addEventHandlers: (socketContext: SocketContext) => {
      socketContext.socket.on('draft:file:upload:request', (data) => {
        if (!data.draftId || !data.filename) {
          socketContext.socket.emit('draft:file:upload:response', {
            id: data.id,
            error: 'Require a filename'
          });
          return;
        }

        // Clean filename
        data.filename = cleanFilename(data.filename);

        // Check if the file already exists
        context.storage
          .haveDraft(data.draftId)
          .then((haveDraft) => {
            if (!haveDraft) {
              socketContext.socket.emit('draft:file:upload:response', {
                id: data.id,
                error: 'Draft does not exist'
              });
              return;
            }

            return context.storage.draftHasFile(data.draftId, data.filename);
          })
          .then((haveFile) => {
            const id = makeId();
            if (!haveFile) {
              uploadNonces.create(id, {
                socketId: socketContext.socket.id,
                filename: data.filename,
                ipAddress: socketContext.socket.handshake.address
              });

              socketContext.socket.emit('draft:file:upload:response', {
                id: data.id,
                result: {
                  draftId: data.draftId,
                  filename: data.filename,
                  nonce: id,
                  lifetime: uploadNonceLifetime
                }
              });
            } else {
              let i = 1;
              let newFilename;
              const parts = data.filename.match(/^(.+)(-(\d+))?(\..+)$/);
              if (parts[3]) {
                i = Number(parts[3]) + 1;
              }
              newFilename = `${parts[1]}-${i}${parts[4]}`;

              const check = (haveNewFile?: boolean) => {
                if (haveNewFile === false) {
                  uploadNonces.create(id, {
                    socketId: socketContext.socket.id,
                    filename: newFilename,
                    ipAddress: socketContext.socket.handshake.address
                  });

                  socketContext.socket.emit('draft:file:upload:response', {
                    id: data.id,
                    result: {
                      draftId: data.draftId,
                      filename: data.filename,
                      exists: true,
                      newFilename,
                      nonce: id,
                      lifetime: uploadNonceLifetime
                    }
                  });
                  return;
                }

                if (haveNewFile === true) {
                  newFilename = `${parts[1]}-${++i}${parts[4]}`;
                }

                context.storage
                  .draftHasFile(data.draftId, newFilename)
                  .then(check);
              };

              check();
            }
          });
      });
    }
  };
};
export default createFilesHandler;
