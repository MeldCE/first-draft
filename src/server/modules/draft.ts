import { Context, SocketContext } from '../draft';
import { BaseMessage, ErrorMessage, Message } from '../../typings/Message';

import { cleanId } from '../../lib/id';

export const createDraftHandler = (app, context: Context) => {
  const log = context.log.child('draft', 'yellow');
  return {
    addDraftEvents: (socketContext: SocketContext) => {
      /**
       * Update the cached draft if receive an update event
       */
      const updateDraft = ({ draft }) => {
        socketContext.draft = draft;
      };

      const closeOpen = () => {
        if (socketContext.draftId && socketContext.draftStore) {
          socketContext.socket.leave(
            context.config.baseEvent + socketContext.draftId
          );
          context.events.removeEventListener(
            `draft:${socketContext.draftId}:update`,
            updateDraft
          );
          context.events.removeEventListener(
            `draft:${socketContext.draftId}:modify`,
            updateDraft
          );
          socketContext.draftStore.close();
          socketContext.draftStore = null;
          socketContext.draftId = null;
        }
      };

      socketContext.socket.on(context.config.baseEvent + 'connect', (data) => {
        closeOpen();

        const response: BaseMessage = {
          id: data.id,
          draft: null
        };

        if (data.name) {
          const draftId = cleanId(data.name);
          const oldDraftId = socketContext.draftId;

          socketContext.storage
            .haveDraft(draftId)
            .then((haveDraft) => {
              if (haveDraft) {
                return context.storage.loadDraft(draftId).then((store) => {
                  socketContext.draftStore = store;
                  socketContext.draftId = draftId;
                  socketContext.socket.join(context.config.baseEvent + draftId);
                  context.events.addEventListener(
                    `draft:${draftId}:update`,
                    updateDraft
                  );
                  context.events.addEventListener(
                    `draft:${draftId}:modify`,
                    updateDraft
                  );

                  return socketContext.draftStore.read();
                });
              } else {
                return null;
              }
            })
            .then((draft) => {
              socketContext.draft = draft;
              socketContext.events.emit('draft:change', {
                oldDraftId,
                newDraftId: draftId
              });
              if (data.getDraft || !draft) {
                response.draft = draft;

                if (draft && draft.config && draft.config.editPassword) {
                  response.draft = {
                    ...draft,
                    config: {
                      ...draft.config,
                      editPassword: true
                    }
                  };
                }

                socketContext.socket.emit(
                  context.config.baseEvent + 'current',
                  response as Message
                );
              }
            })
            .catch((error) => {
              log.error('Got error while loading draft' + draftId + ':', error);
              response.error = {
                code: error.code,
                message: `Error loading draft: ${error.message}`
              };

              socketContext.socket.emit(
                context.config.baseEvent + 'error',
                response as ErrorMessage
              );
            });
        } else {
          response.error = {
            message: 'No draft id given'
          };

          socketContext.socket.emit(
            context.config.baseEvent + 'error',
            response as ErrorMessage
          );
        }
      });

      socketContext.socket.on(context.config.baseEvent + 'create', (data) => {
        closeOpen();

        const response: BaseMessage = {
          id: data.id
        };

        if (data.name) {
          const draftId = cleanId(data.name);

          socketContext.storage.haveDraft(draftId).then(
            (have) => {
              if (have) {
                response.error = {
                  code: 'exists',
                  message: 'Already have a draft with that id'
                };
                socketContext.socket.emit(
                  context.config.baseEvent + 'error',
                  response as ErrorMessage
                );
                return;
              } else {
                return socketContext.storage
                  .loadDraft(draftId)
                  .then((store) => {
                    socketContext.draftStore = store;
                    socketContext.draftId = draftId;
                    socketContext.socket.join(
                      context.config.baseEvent + draftId
                    );

                    return socketContext.draftStore.save(data);
                  })
                  .then(() => {
                    // TODO Send confirmation to client
                    socketContext.draft = data.draft;
                  })
                  .catch((error) => {
                    socketContext.socket.emit(
                      context.config.baseEvent + 'error',
                      {
                        id: data.id,
                        message: `Error creating draft: ${error.message}`
                      }
                    );
                  });
              }
            },
            (error) => {
              response.error = {
                code: error.code,
                message: `Error checking for existing drawing: ${error.message}`
              };

              socketContext.socket.emit(
                context.config.baseEvent + 'error',
                response as ErrorMessage
              );
            }
          );
        } else {
          response.error = {
            message: 'No draft id given'
          };

          socketContext.socket.emit(
            context.config.baseEvent + 'error',
            response as ErrorMessage
          );
        }
      });

      /**
       * Handle incoming draw socket messages
       *
       * @param data Draw event data
       */
      const handleDraw = (data) => {
        if (!socketContext.draftId || !socketContext.draftStore) {
          socketContext.socket.emit(context.config.baseEvent + 'error', {
            id: data.id,
            type: 'nodraft'
          });
          return;
        }

        // Check for authentication if required
        if (!socketContext.user.hasPermissions(['edit'])) {
          const promise = socketContext.user.requestAuthentication(['edit']);

          if (!data.active && promise instanceof Promise) {
            // Cache event waiting for authentication
            promise.then((authenticated) => {
              if (
                authenticated &&
                socketContext.user.hasPermissions(['edit'])
              ) {
                handleDraw(data);
              }
            });
          } else {
            socketContext.socket.emit(context.config.baseEvent + 'reject', {
              id: data.id,
              reason: 'permission',
              layerName: data.layerName,
              elementName: data.element && data.element.name
            });
          }
          return;
        }

        // Don't store if being actively drawn to save writes
        // TODO Change `active` to `complete`?
        if (!data.active) {
          socketContext.draftStore
            .modify(data)
            .then((draft) => {
              socketContext.draft = draft;
              socketContext.socket
                .to(context.config.baseEvent + socketContext.draftId)
                .emit(context.config.baseEvent + 'draw', data);
              context.events.emit(`draft:${socketContext.draftId}:modify`, {
                change: data,
                draft
              });
            })
            .catch((error) => {
              log.info('Error handling draw event', error);
              socketContext.socket.emit(context.config.baseEvent + 'error', {
                ...error,
                id: data.id,
                message: `Error modifying draft: ${error.message || error}`
              });
            });
        } else {
          socketContext.socket
            .to(context.config.baseEvent + socketContext.draftId)
            .emit(context.config.baseEvent + 'draw', data);
        }
      };

      socketContext.socket.on(context.config.baseEvent + 'draw', handleDraw);

      const handleUpdate = (data) => {
        if (!socketContext.draftId || !socketContext.draftStore) {
          socketContext.socket.emit(context.config.baseEvent + 'error', {
            id: data.id,
            type: 'nodraft'
          });
          return;
        }

        // Check correct draft
        if (socketContext.draftId !== data.draftId) {
          socketContext.socket.emit(context.config.baseEvent + 'error', {
            id: data.id,
            type: 'draftIdMismatch'
          });
          return;
        }

        // Check for authentication if required
        if (!socketContext.user.hasPermissions(['edit'])) {
          const promise = socketContext.user.requestAuthentication(['edit']);

          if (!data.active && promise instanceof Promise) {
            // Cache event waiting for authentication
            promise.then((authenticated) => {
              if (
                authenticated &&
                socketContext.user.hasPermissions(['edit'])
              ) {
                handleUpdate(data);
              }
            });
          } else {
            socketContext.socket.emit(context.config.baseEvent + 'reject', {
              id: data.id,
              reason: 'permission'
            });
          }
          return;
        }

        // TODO Validate update data

        socketContext.draftStore
          .update(data)
          .then((draft) => {
            socketContext.draft = draft;
            const event = {
              changes: JSON.parse(JSON.stringify(data.data)),
              draft
            };

            // TODO Clean update data
            if (data.data.config && data.data.config.editPassword) {
              data.data.config.editPassword = true;
            }

            socketContext.socket
              .to(context.config.baseEvent + socketContext.draftId)
              .emit(socketContext.config.baseEvent + 'update', data);
            context.events.emit(`draft:${socketContext.draftId}:update`, event);
          })
          .catch((error) => {
            socketContext.socket.emit(context.config.baseEvent + 'error', {
              id: data.error,
              message: `Error updating draft: ${error.message}`
            });
          });
      };

      socketContext.socket.on(
        context.config.baseEvent + 'update',
        handleUpdate
      );

      /*TODO socketContext.socket.on(
        context.config.baseEvent + 'rename',
        (data) => {}
      );*/

      socketContext.socket.on(context.config.baseEvent + 'view', (data) => {
        if (!socketContext.draftId) {
          return;
        }

        const passData = {
          bounds: data.bounds,
          cursor: data.cursor,
          user: null
        };

        if (socketContext.user.user) {
          passData.user = socketContext.user.user.username;
        }

        socketContext.socket
          .to(context.config.baseEvent + socketContext.draftId)
          .emit(context.config.baseEvent + 'view', passData);
      });

      //TODO socketContext.socket.on('disconnect', () => {});
    }
  };
};
