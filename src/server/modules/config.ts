import { AppConfig, Config } from '../../typings/Config';

import * as fs from 'fs';
import { resolve } from 'path';
import { cwd } from 'process';
import { promisify } from 'util';

const access = promisify(fs.access);

const defaultConfig: Config = {
  baseUri: '/',
  draftUri: 'd',
  baseEvent: 'draft:',
  create: true,
  color: 'red',
  pdfDpi: 150,
  maxImageRow: 4,
  imageSpacing: 20,
  minZoomLevel: 0.001,
  draftEditPasswords: true,
  thumbnails: [
    {
      scale: 0.1,
      minimumImageDimension: 1500,
      thumbnailBigDimension: 500
    },
    {
      scale: 0.3,
      minimumImageDimension: 2000,
      thumbnailBigDimension: 1000
    }
  ],
  preloadThumbnails: true,
  preloadOriginal: true,
  banner: process.env.DRAFT_BANNER || null,
  viewUrl: true,
  alwaysReplaceUrl: false,
  share: {
    cursor: true,
    tool: true,
    view: true
  },
  tools: {
    settings: {
      pen: {
        presets: [
          {
            color: '#ff0000',
            thickness: 2,
            strokeCap: 'none',
            smoothing: 10,
            opacity: 100
          },
          {
            color: '#000000',
            thickness: 2,
            strokeCap: 'none',
            smoothing: 10,
            opacity: 100
          },
          {
            color: '#ffff00',
            thickness: 30,
            strokeCap: 'none',
            smoothing: 10,
            opacity: 40
          },
          {
            color: '#00ff00',
            thickness: 30,
            strokeCap: 'none',
            smoothing: 10,
            opacity: 40
          }
        ]
      },
      textbox: {
        presets: [
          {
            color: '#ff0000',
            fontSize: 12,
            background: true,
            backgroundColor: '#ffffff',
            padding: 5,
            backgroundOpacity: 80
          },
          {
            color: '#000000',
            fontSize: 12,
            background: true,
            backgroundColor: '#ffffff',
            padding: 5,
            backgroundOpacity: 80
          }
        ]
      }
    }
  }
};

const defaultServerConfig = {
  cookie: {
    id: 'c',
    lifetime: 16000000
  }
};

export const getConfig = (): Promise<{
  config: Config;
  appConfig: AppConfig;
}> => {
  const configFile = resolve(cwd(), 'config.js');
  return access(configFile, fs.constants.R_OK)
    .then(() => {
      try {
        // Can't use JSON.parse as importing as JS
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        let readConfig = require(configFile);
        if (readConfig.default) {
          readConfig = readConfig.default;
        }
        return Promise.resolve({
          ...defaultConfig,
          ...readConfig
        });
      } catch (error) {
        return Promise.reject(error);
      }
    })
    .catch((error) => {
      if (error.code === 'ENOENT' || error.code === 'MODULE_NOT_FOUND') {
        return Promise.resolve({
          ...defaultConfig
        });
      } else {
        Promise.reject(error);
      }
    })
    .then((config) => {
      const appConfig: any = {
        ...config
      };
      config = {
        ...defaultServerConfig,
        ...config
      } as Config;

      if (appConfig.editPassword) {
        appConfig.editPassword = true;
      }

      return {
        config,
        appConfig
      };
    });
};
export default getConfig;
