import urlJoin from 'url-join';
import io from 'socket.io-client';

import { cleanId } from '../lib/id';

import { startDraft } from './modules/draft';
import { createStatus } from './modules/status';

import makeId from '../lib/id';

fetch('_config', {
  headers: {
    'Content-Type': 'application/json'
  }
})
  .then((response) => {
    return response.json();
  })
  .then((config) => {
    if (config.thumbnails && config.thumbnails.length) {
      config.thumbnails.sort((a, b) => {
        if (a.scale > b.scale) {
          return 1;
        } else if (a.scale < b.scale) {
          return -1;
        } else {
          return 0;
        }
      });

      config.thumbnails.forEach((level) => {
        if (
          typeof level.minimumImageDimension === 'number' &&
          typeof level.minimumImageSize !== 'number'
        ) {
          level.minimumImageSize =
            level.minimumImageDimension * level.minimumImageDimension;
        }
      });
    }

    const pathRegex = new RegExp(
      `^${urlJoin(config.baseUri, config.draftUri, '(.+)/')}$`
    );

    let id;
    let randomId = false;
    const match = pathRegex.exec(location.pathname);
    if (!match) {
      if (config.create) {
        id = makeId();
        randomId = true;
        history.replaceState(
          { id },
          '',
          urlJoin(config.baseUri, config.draftUri, id + '/')
        );
      }
    } else {
      id = cleanId(match[1]);
    }

    let socket;
    if (
      import.meta.env.MODE === 'test' ||
      import.meta.env.MODE === 'test_dev'
    ) {
      socket = {
        sent: [],
        queue: [],
        mockers: {},
        addMocker: (event, handler) => {
          if (socket.mockers[event]) {
            if (socket.mockers[event].indexOf(handler) === -1) {
              socket.mockers[event].push(handler);
            }
          } else {
            socket.mockers[event] = [handler];
            let i = 0;
            while (i < socket.queue.length) {
              if (event === '*') {
                handler(socket.queue[i].event, socket.queue[i].data);
              } else if (socket.queue[i].event === event) {
                handler(socket.queue[i].data);
                socket.queue.splice(i, 1);
              } else {
                i++;
              }
            }
          }
        },
        removeMocker: (event, handler) => {
          if (socket.mockers[event]) {
            const index = socket.mockers[event].indexOf(handler);
            if (index !== -1) {
              socket.mockers[event].splice(index, 1);
            }
          }
        },
        waitForEvent: (event) => {
          const first = socket.sent.find((e) => e.event === event);
          if (first) {
            return Promise.resolve(first.data);
          }
          return new Promise((resolve) => {
            const handler = (data) => {
              socket.removeMocker(event, handler);
              resolve(data);
            };
            socket.addMocker(event, handler);
          });
        },
        handlers: {},
        on: (event, handler) => {
          if (socket.handlers[event]) {
            if (socket.handlers[event].indexOf(handler) === -1) {
              socket.handlers[event].push(handler);
            }
          } else {
            socket.handlers[event] = [handler];
          }
        },
        off: (event, handler) => {
          if (socket.handlers[event]) {
            const index = socket.handlers[event].indexOf(handler);
            if (index !== -1) {
              socket.handlers[event].splice(index, 1);
            }
          }
        },
        emit: (event, data) => {
          /* eslint-disable-next-line no-console */
          console.debug('%c<<< Emitted event', 'color: #860', event, data);
          socket.sent.push({ event, data });
          if (socket.mockers[event]?.length) {
            socket.mockers[event].map((handler) => handler(data));
          } else {
            socket.queue.push({ event, data });
          }
          (socket.mockers['*'] || []).map((handler) => handler(event, data));
        },
        emitMock: (event, data) => {
          /* eslint-disable-next-line no-console */
          console.debug('%c>>> Received event', 'color: #090', event, data);
          (socket.handlers[event] || []).map((handler) => handler(data));
        },
        clear: () => {
          socket.sent = [];
        }
      };
      window['socketEvents'] = socket;

      /* eslint-disable-next-line no-console */
      console.debug('socketEvents', socket);

      // Mock connection event
      setTimeout(() => {
        socket.emitMock('connect');
      }, 0);
    } else {
      const ioSocket = io();
      socket = {
        emit: (event, data) => {
          if (import.meta.env.DEV) {
            /* eslint-disable-next-line no-console */
            console.debug('%c<<< Emitted event', 'color: #860', event, data);
          }
          ioSocket.emit(
            event,
            data
              ? {
                  ...data,
                  senderId: ioSocket.id
                }
              : null
          );
        },
        on: (event, handler) => {
          ioSocket.on(event, (data) => {
            if (import.meta.env.DEV) {
              /* eslint-disable-next-line no-console */
              console.debug('%c>>> Received event', 'color: #090', event, data);
            }
            if (!data || data.senderId !== ioSocket.id) {
              handler(data);
            }
          });
        }
      };
    }

    const toolbar = document.getElementById('toolbar');
    const status = createStatus(document.getElementById('footer'), config);
    // TODO Split out draft creation so can have multiple drafts per app
    const instance = startDraft(
      document.getElementById('canvas'),
      config,
      socket,
      {
        id,
        noSetName: randomId,
        toolbar,
        status
      }
    );

    if (
      import.meta.env.MODE === 'test' ||
      import.meta.env.MODE === 'test_dev'
    ) {
      window['draftInstance'] = instance;

      /* eslint-disable-next-line no-console */
      console.debug('draftInstance', instance);
    }
  });
