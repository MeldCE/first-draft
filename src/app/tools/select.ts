interface MoveContext {
  startPosition?: Point;
  initialPositions?: Array<{
    item: any;
    position: Point;
  }>;
}

const moveHandler = (instance, event, context, complete?: boolean) => {
  if (context.startPosition) {
    let offset;
    if (event.middleCoordinates) {
      offset = event.middleCoordinates.subtract(context.startPosition);
      context.lastOffset = offset;
    } else {
      offset = context.lastOffset;
    }

    if (offset.length || context.moved) {
      context.moved = true;
      const newPositions = context.initialPositions.map((item) => ({
        item: item.item,
        position: item.position.add(offset)
      }));

      instance.drawing.moveItems(newPositions, !complete);
    }

    return true;
  }
};

const startSelectBox = (instance, event, context) => {
  context.startPosition = event.middleCoordinates;
  context.initiallySelected = instance.drawing.selected;
  if (event.parameters && event.parameters.add) {
    context.additionSelect = true;
  }
  context.box = instance.drawing.createRectangle({}, false);
  // TODO set cursor to crosshair instance.drawing.set
  // TODO Store currently selected if additional key held down
  context.bounce = null;
  return true;
};

const updateSelectBox = (instance, event, context) => {
  if (context.box) {
    context.box.remove();
  }
  const options = {
    from: context.startPosition,
    to: event.middleCoordinates,
    strokeWidth: 1,
    selected: false,
    deselectAll: false
  };
  // If crossing window (rtl), draw box as dashed
  if (event.middleCoordinates.x < context.startPosition.x) {
    options.dashArray = [6, 4];
  }
  context.box = instance.drawing.createRectangle(options, false);
  if (context.bounce === null) {
    if (event.middleCoordinates.x > context.startPosition.x) {
      // Selection window
      instance.drawing.selectWithin(
        context.startPosition,
        event.middleCoordinates
      );
    } else {
      // Crossing window
      instance.drawing.selectCrossing(
        context.startPosition,
        event.middleCoordinates
      );
    }
    context.bounce = setTimeout(() => {
      context.bounce = null;
    }, 100);
  }
  if (context.additionSelect) {
    instance.drawing.select(context.initiallySelected, true);
  }
  context.box.unselect();
  return true;
};

const finishSelectBox = (instance, event, context) => {
  context.box.remove();
  if (context.bounce !== null) {
    clearTimeout(context.bounce);
  }
  return false;
};

export default {
  name: 'Select',
  description: 'Select/move tool',
  icon: 'cursor',
  requiredPermissions: ['edit'],
  buttons: [
    {
      label: 'Delete selected',
      icon: 'trash',
      action: 'delete'
    }
  ],
  mappings: [
    {
      type: 'keyup',
      filter: {
        key: 'Delete'
      },
      action: 'delete'
    },
    {
      active: true,
      type: 'mousedown',
      filter: {
        button: 0
      },
      action: 'startMove',
      chainActions: [
        {
          action: 'updateMove',
          type: 'mousemove'
        },
        {
          action: 'finishMove',
          type: 'mouseup'
        },
        {
          type: 'keyup',
          filter: {
            key: 'Escape'
          },
          action: 'cancelMove'
        }
      ]
    },
    {
      active: true,
      type: 'touchstart',
      filter: {
        touches: 1
      },
      action: 'startMove',
      chainActions: [
        {
          action: 'updateMove',
          type: 'touchmove'
        },
        {
          action: 'finishMove',
          type: 'touchend'
        },
        {
          type: 'keyup',
          filter: {
            key: 'Escape'
          },
          action: 'cancelMove'
        }
      ]
    },
    {
      active: true,
      type: 'mousedown',
      filter: {
        button: 0,
        ctrlKey: true
      },
      parameters: {
        add: 'ctrlKey'
      },
      action: 'startSelectBox',
      chainActions: [
        {
          action: 'updateSelectBox',
          type: 'mousemove'
        },
        {
          action: 'finishSelectBox',
          type: 'mouseup'
        },
        {
          type: 'keyup',
          filter: {
            key: 'Escape'
          },
          action: 'cancelSelectBox'
        }
      ]
    },
    {
      active: true,
      type: 'click',
      filter: {
        button: 0,
        ctrlKey: true
      },
      parameters: {
        add: 'ctrlKey'
      },
      action: 'select'
    },
    {
      active: true,
      type: 'click',
      filter: {
        button: 0
      },
      action: 'select'
    },
    {
      type: 'keyup',
      filter: {
        key: 'Escape'
      },
      action: 'clearAndSelectTool'
    }
  ],
  actions: {
    clearSelection: {
      handler: (instance, event) => {
        instance.drawing.deselectAll();
      }
    },
    clearAndSelectTool: {
      handler: (instance, event) => {
        if (instance.drawing.selected.length) {
          instance.drawing.deselectAll();
        } else {
          instance.changeTool('select');
        }
      }
    },
    delete: {
      requiredPermissions: ['edit'],
      handler: (instance, event) => {
        instance.drawing.deleteSelected();
      }
    },
    startSelectBox: {
      handler: startSelectBox
    },
    updateSelectBox: {
      handler: updateSelectBox
    },
    finishSelectBox: {
      handler: finishSelectBox
    },
    cancelSelectBox: {
      handler: (instance, event, context) => {
        instance.drawing.select(context.initiallySelected);
        context.box.remove();
      }
    },
    select: {
      handler: (instance, event, context) => {
        if (!event.parameters || !event.parameters.add) {
          instance.drawing.deselectAll();
        }

        if (event.item) {
          event.item.selected = true;
        }
      }
    },
    selectAll: {
      handler: (instance, event, context) => {
        instance.drawing.selectAll();
      }
    },
    startMove: {
      handler: (instance, event, context) => {
        if (event.item) {
          if (!event.item.selected) {
            if (!event.parameters || !event.parameters.add) {
              instance.drawing.deselectAll();
            }
            event.item.selected = true;
          }

          const selected = instance.drawing.selected;

          context.startPosition = event.middleCoordinates;
          context.initialPositions = selected.map((item) => ({
            item,
            position: item.position
          }));

          return true;
        } else {
          context.type = 'selectBox';
          return startSelectBox(instance, event, context);
        }
      }
    },
    updateMove: {
      handler: (instance, event, context) => {
        if (context.type === 'selectBox') {
          return updateSelectBox(instance, event, context);
        }

        return moveHandler(instance, event, context);
      }
    },
    finishMove: {
      handler: (instance, event, context) => {
        if (context.type === 'selectBox') {
          return finishSelectBox(instance, event, context);
        }

        moveHandler(instance, event, context, true);
        context.startPosition = null;
        context.initialPositions = null;
        return false;
      }
    },
    cancelMove: {
      handler: (instance, event, context) => {
        if (context.type === 'selectBox') {
          return cancelSelectBox(instance, event, context);
        }

        if (context.initialPositions) {
          instance.drawing.moveItems(context.initialPositions, true);
        }
      }
    }
  }
} as Tool<MoveContext>;
