import { Tool } from '../../typings/Tool';
import { RowData } from '../../typings/Draft';
import { promisesSettled } from '../../lib/promises';
import pdfjsWorkerUrl from 'pdfjs-dist/build/pdf.worker.js?url';

const nameFromUrl = (url) => {
  const parsed = new URL(url);
  return decodeURI(
    parsed.pathname.substring(parsed.pathname.lastIndexOf('/') + 1)
  );
};

const checkUrlIsFile = (url: string) => {
  return new Promise((resolve, reject) => {
    //let name;
    try {
      new URL(url);
      //resolve(url);
      //name = nameFromUrl(url);

      // Try loading the url into an Image to check that it is an image
      const image = new Image();

      const loadHandler = (event) => {
        resolve(url);
        image.removeEventListener('load', loadHandler);
        image.removeEventListener('error', errorHandler);
      };
      const errorHandler = (event) => {
        reject(
          new Error(
            `Error inserting URL ${url}: Was not an image or could not download the image`
          )
        );
        image.removeEventListener('load', loadHandler);
        image.removeEventListener('error', errorHandler);
      };
      image.addEventListener('load', loadHandler);
      image.addEventListener('error', errorHandler);
      image.src = url;
    } catch (error) {
      reject(new Error(`Error inserting URL ${url}: Could not process URL`));
    }
  });
};

let pdfjs = null;

const insertPdf = (
  instance,
  pdfFile: File,
  position?: Point | Promise<RowData>
) => {
  const pdfPromise = [pdfFile.arrayBuffer()];
  if (!pdfjs) {
    pdfjs = import('pdfjs-dist').then((module) => {
      pdfjs = module;
      pdfjs.GlobalWorkerOptions.workerSrc = pdfjsWorkerUrl;
    });
    pdfPromise.push(pdfjs);
  } else if (pdfjs instanceof Promise) {
    pdfPromise.push(pdfjs);
  }

  // Split pdf file to be used for pages
  const filename = pdfFile.name.endsWith('.pdf')
    ? pdfFile.name.slice(0, -4)
    : pdfFile.name;

  const status = instance.app.status
    ? instance.app.status.createStatus(`Loading PDF ${pdfFile.name}...`, {
        timeout: false
      })
    : null;
  return Promise.all(pdfPromise).then(([pdfBuffer]) => {
    const task = pdfjs.getDocument(pdfBuffer);
    const images = [];

    return task.promise
      .then((pdf) => {
        if (status) {
          status.update(
            `Loading PDF ${pdfFile.name} (${pdf.numPages} pages)...`
          );
        }
        let promise = Promise.resolve();
        let insertPromise;
        if (position instanceof Promise) {
          insertPromise = position;
        } else {
          insertPromise = Promise.resolve({ position });
        }
        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d');
        for (let i = 1; i <= pdf.numPages; i++) {
          promise = promise
            .then(() => pdf.getPage(i))
            .then((page) => {
              if (status) {
                status.update(
                  `Rendering page ${page.pageNumber} of ${pdfFile.name} (${pdf.numPages} pages)...`
                );
              }
              // TODO Need to scale to use custom DPI (default 75dpi)
              const scale = (instance.app.config.pdfDpi || 75) / 75;
              const viewport = page.getViewport({
                scale
              });
              canvas.width = viewport.width;
              canvas.height = viewport.height;
              canvas.style.width = Math.floor(viewport.width);
              canvas.style.height = Math.floor(viewport.height);
              document.body.appendChild(canvas);

              return page
                .render({
                  canvasContext: context,
                  viewport
                })
                .promise.then(() => {
                  return new Promise((resolve) =>
                    canvas.toBlob(resolve, 'image/png')
                  ).then((blob) => {
                    const file = new File(
                      [blob],
                      filename + '_page_' + page.pageNumber + '.png',
                      {
                        type: 'image/png'
                      }
                    );
                    images.push(file);
                    return file;
                  });
                });
            });

          insertPromise = Promise.all([promise, insertPromise]).then(
            ([file, rowData]) => {
              return instance.drawing.createImages([file], null, rowData);
            }
          );
        }

        return insertPromise.then((rowData) => {
          if (status) {
            status.remove();
          }

          return rowData;
        });
      })
      .catch((error) => {
        status.remove();
        instance.app.status.createStatus(
          `Error loading PDF ${pdfFile.name}: ${error.message}`,
          {
            type: 'error',
            timeout: true
          }
        );
      });
  });
};

const insertImagesFromItems = (
  instance,
  items: DataTransferList,
  position?: Point,
  ignoreBadUrls?: boolean
) => {
  const images = [];
  const pdfs = [];
  if (
    // Old Firefox file
    items.length === 2 &&
    items[0].kind === 'string' &&
    items[1].kind === 'file'
  ) {
    images.push(
      new Promise((resolve, reject) => {
        const file = items[1].getAsFile();
        items[0].getAsString((value) => {
          let name;
          const match = value.match(/<img .*src="([^"]*)".*>/);
          if (match) {
            name = nameFromUrl(match[1]);
          } else {
            try {
              new URL(value);
              name = nameFromUrl(value);
            } catch (error) {
              void 1;
            }
          }

          if (name) {
            resolve({
              name,
              file
            });
          } else {
            resolve(file);
          }
        });
      })
    );
  } else if (
    // Edge file
    items.length === 3 &&
    items[0].kind === 'file' &&
    items[1].kind === 'string' &&
    items[1].type === 'text/html' &&
    items[2].kind === 'string' &&
    items[2].type === 'text/plain'
  ) {
    images.push(
      new Promise((resolve, reject) => {
        const file = items[0].getAsFile();
        items[2].getAsString((value) => {
          let name = null;
          try {
            name = nameFromUrl(value);
          } catch (error) {
            void 1;
          }

          if (name) {
            resolve({
              name,
              file
            });
          } else {
            resolve(file);
          }
        });
      })
    );
  } else {
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (item.kind === 'string') {
        images.push(
          new Promise((resolve, reject) => {
            item.getAsString((value) => {
              if (ignoreBadUrls) {
                checkUrlIsFile(value).then(resolve, () => resolve(false));
              } else {
                checkUrlIsFile(value).then(resolve, reject);
              }
            });
          })
        );
      } else if (item.kind === 'file') {
        if (item.type === 'application/pdf') {
          pdfs.push(Promise.resolve(item.getAsFile()));
        } else if (item.type.match(/^image\//)) {
          images.push(Promise.resolve(item.getAsFile()));
        }
      }
    }
  }

  let positionPromise;
  if (images?.length) {
    positionPromise = promisesSettled(images).then((fileObjects) => {
      // Go through and check promises resolved
      for (let i = fileObjects.length - 1; i >= 0; i--) {
        if (!fileObjects.fulfilled[i]) {
          instance.app.status.createStatus(
            fileObjects[i].message || fileObjects[i],
            { type: 'error', timeout: true }
          );
          fileObjects.splice(i, 1);
        } else if (fileObjects[i] === false) {
          // Ignore bad urls
          fileObjects.splice(i, 1);
        }
      }
      if (fileObjects.length) {
        return instance.drawing.createImages(fileObjects, position);
      }
    });
  } else {
    positionPromise = position;
  }

  promisesSettled(pdfs).then((pdfFiles) => {
    for (let i = 0; i < pdfFiles.length; i++) {
      positionPromise = insertPdf(instance, pdfFiles[i], positionPromise);
    }
  });
};

export default {
  name: 'Image',
  description: 'Insert an image into a draft',
  icon: '',
  requiredPermissions: ['edit'],
  buttons: [
    {
      label: 'Insert image',
      icon: 'image',
      form: {
        title: 'Insert Image/PDF',
        description:
          'Select the image(s), PDF(s) and/or enter a url to an image to insert',
        elements: {
          url: {
            title: 'Image URL',
            type: 'url'
          },
          files: {
            title: 'Image/PDF File(s)',
            type: 'file',
            attributes: {
              multiple: true,
              accept: 'image/*,application/pdf'
            }
          }
        },
        action: 'upload',
        submitLabel: 'Insert'
      }
    }
  ],
  mappings: [
    {
      type: 'paste',
      action: 'upload'
    },
    {
      type: 'drop',
      filter: {
        type: 'image'
      },
      description: 'Drop images to insert in draft',
      action: 'upload'
    }
  ],
  actions: {
    upload: {
      handler: (instance, event) => {
        // TODO Add processing past status here
        if (event.type === 'paste') {
          const items = event.clipboardData && event.clipboardData.items;
          if (items) {
            insertImagesFromItems(instance, items, null, true);
          }
        } else if (event.type === 'drop') {
          const position = instance.drawing.toDrawingCoordinates(
            event.clientX,
            event.clientY
          );
          insertImagesFromItems(instance, event.dataTransfer.items, position);
        } else if (event.type === 'submit' && event.data) {
          const files = event.data.files && Array.from(event.data.files);
          const images = [];
          const pdfs = [];
          let rowDataPromise = null;
          // Check if any of the files are pdfs
          if (files && files.length) {
            for (let i = 0; i < files.length; i++) {
              if (files[i].type === 'application/pdf') {
                pdfs.push(files[i]);
              } else {
                images.push(files[i]);
              }
            }

            if (images.length) {
              rowDataPromise = instance.drawing.createImages(images);
            }

            if (pdfs.length) {
              for (let i = 0; i < pdfs.length; i++) {
                rowDataPromise = insertPdf(instance, pdfs[i], rowDataPromise);
              }
            }
          }
          if (event.data.url) {
            checkUrlIsFile(event.data.url)
              .then(
                () => {
                  return [event.data.url];
                },
                (error) => {
                  instance.app.status.createStatus(error.message, {
                    type: 'error',
                    timeout: true
                  });
                  return Promise.resolve([]);
                }
              )
              .then((array) => {
                if (files && files.length) {
                  array = array.concat(Array.from(files));
                }
                if (array.length) {
                  instance.drawing.createImages(array);
                }
              });
          }
        }
      }
    }
  },
  activate: () => {
    return Promise.resolve(false);
  }
} as Tool<void>;
