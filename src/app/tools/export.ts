import { Tool } from '../../typings/Tool';

import saveAs from 'file-saver';

const saveSvg = (instance, svg) => {
  const file = new Blob([svg], { type: 'image/svg+xml' });
  saveAs(file, instance.app.draftId + '_' + new Date().toISOString() + '.svg');
};

export default {
  name: 'Import/Export',
  description: 'Import/Export drawing',
  icon: '',
  buttons: [
    {
      label: 'Import/Export drawing',
      icon: 'download',
      form: {
        title: 'Import/Export',
        elements: [
          {
            name: 'downloadViewPNG',
            type: 'button',
            title: 'Download view as PNG',
            handler: 'downloadViewPNG'
          },
          {
            name: 'downloadSVG',
            type: 'button',
            title: 'Download drawing as SVG',
            handler: 'downloadSVG'
          },
          {
            name: 'downloadPNG',
            type: 'button',
            title: 'Download drawing as PNG',
            handler: 'downloadPNG'
          }
        ]
      }
    }
  ],
  actions: {
    downloadSVG: {
      handler: (instance, event) => {
        instance.drawing.showCover();
        const promise = instance.drawing.exportSVG({ bounds: 'content' });
        instance.app.status.createStatus('Exporting drawing as PNG...', {
          promise
        });
        promise.then((svg) => {
          saveSvg(instance, svg);
          instance.drawing.hideCover();
        });
      }
    },
    downloadViewPNG: {
      handler: (instance, event) => {
        instance.drawing.showCover();
        const promise = instance.drawing.exportPNG();
        instance.app.status.createStatus('Exporting view as PNG...', {
          promise
        });
        promise.then((png) => {
          saveAs(
            png,
            instance.app.draftId + '_' + new Date().toISOString() + '.png'
          );
          instance.drawing.hideCover();
        });
      }
    },
    downloadPNG: {
      handler: (instance, event) => {
        // instance.drawing.showCover();
        const promise = instance.drawing.exportPNG({ bounds: 'content' });
        if (promise === null) {
          return;
        }
        instance.app.status.createStatus('Exporting view as PNG...', {
          promise
        });
        promise.then((png) => {
          if (png === null) {
            instance.app.status.createStatus(
              'Browser unable to export image. Draft may be too large.',
              { type: 'error' }
            );
          } else {
            saveAs(
              png,
              instance.app.draftId + '_' + new Date().toISOString() + '.png'
            );
          }
          instance.drawing.hideCover();
        });
      }
    }
  }
} as Tool;
