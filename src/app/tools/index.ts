import image from './image';
import pen from './pen';
import panZoom from './pan-zoom';
import select from './select';
import textbox from './textbox';
import exportTool from './export';

export default {
  image,
  pen,
  panZoom,
  select,
  textbox,
  export: exportTool
};
