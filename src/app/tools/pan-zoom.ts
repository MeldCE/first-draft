import { Tool } from '../../typings/Tool';

interface PanZoomContext {
  cursor?: string;
}

export default <Tool<PanZoomContext>>{
  name: 'Pan/Zoom',
  description: 'Pan/zoom tool',
  icon: 'pan',
  cursor: 'grab',
  draggable: true,
  buttons: [
    {
      label: 'Zoom to extent',
      icon: 'arrows-out',
      action: 'zoomToExtent'
    },
    {
      label: 'Reset pan/zoom',
      icon: 'reset',
      action: 'resetPanAndZoom'
    }
  ],
  mappings: [
    {
      active: true,
      type: 'mousedown',
      filter: {
        button: 0
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'mousemove'
        },
        {
          action: 'stop',
          type: 'mouseup'
        }
      ]
    },
    {
      type: 'wheel',
      action: 'zoom'
    },
    {
      type: 'mousemove',
      filter: {
        buttons: 4
      },
      action: 'update',
      chainActions: [
        {
          action: 'stop',
          type: 'mouseup',
          filter: {
            button: 1
          }
        }
      ]
    },
    {
      type: 'mousemove',
      filter: {
        $and: {
          buttons: 1,
          shiftKey: true
        }
      },
      action: 'update',
      chainActions: [
        {
          action: 'stop',
          type: 'mouseup'
        },
        {
          action: 'stop',
          type: 'keyup',
          filter: {
            key: 'Shift'
          }
        }
      ]
    },
    {
      type: 'mousedown',
      filter: {
        $or: {
          button: 1,
          $and: {
            button: 0,
            shiftKey: true
          }
        }
      },
      action: 'start',
      chainActions: [
        {
          action: 'stop',
          type: 'mouseup',
          filter: {
            button: 1
          }
        },
        {
          action: 'stop',
          type: 'keyup',
          filter: {
            key: 'Shift'
          }
        }
      ]
    },
    {
      active: true,
      type: 'touchstart',
      filter: {
        touches: 1
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'touchmove',
          filter: {
            touches: 1
          }
        },
        {
          action: 'stop',
          type: 'touchend',
          filter: {
            touches: 0
          }
        }
      ]
    },
    {
      type: 'touchstart',
      filter: {
        touches: 2
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'touchmove',
          filter: {
            touches: 2
          }
        },
        {
          action: 'stop',
          type: 'touchend'
        }
      ]
    }
  ],
  actions: {
    start: {
      handler: (instance, event, context?: PanContext) => {
        instance.setCursor('grab');
        return true;
      }
    },
    update: {
      handler: (instance, event, context: PanContext) => {
        instance.setCursor('grabbing');
        if (event.type === 'mousemove' && !event.originalEvent.buttons) {
          instance.setCursor();
          context.cursor = null;
          return;
        }
        if (event.coordinateMovement) {
          instance.drawing.scrollBy(
            new instance.drawing.Point(
              -event.coordinateMovement.x,
              -event.coordinateMovement.y
            )
          );
        }
        return true;
      }
    },
    stop: {
      handler: (instance, event, context: PanContext) => {
        instance.setCursor();
        context.cursor = null;
      }
    },
    zoom: {
      handler: (instance, event) => {
        if (event.originalEvent.deltaY) {
          if (event.originalEvent.deltaY < 0) {
            instance.drawing.zoomIn(event.middleCoordinates);
          } else {
            instance.drawing.zoomOut(event.middleCoordinates);
          }
          event.preventDefault();
        }
        return true;
      }
    },
    zoomin: {
      handler: (instance, event) => {
        // For pinch use distance between touches relative to initial distance
        // betwee ntouches
        instance.drawing.zoomIn();
        return true;
      }
    },
    zoomout: {
      handler: (instance, event) => {
        instance.drawing.zoomOut();
        return true;
      }
    },
    resetZoom: {
      handler: (instance, event) => {
        instance.drawing.zoomTo(1);
        return true;
      }
    },
    resetPan: {
      handler: (instance, event) => {
        instance.drawing.setOffset(0, 0);
        return true;
      }
    },
    resetPanAndZoom: {
      handler: (instance, event) => {
        instance.drawing.zoomTo(1);
        instance.drawing.setOffset(0, 0);
      }
    },
    zoomToExtent: {
      handler: (instance, event) => {
        instance.drawing.zoomToExtent();
        return true;
      }
    }
  }
};
