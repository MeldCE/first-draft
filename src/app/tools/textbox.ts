import { Tool } from '../../typings/Tool';
import colorally from 'colorally';

interface Options {
  /// Background color and opacity in a 8 hex string
  background: string;
  /// Text color
  color: string;
  fontSize: number;
  /// Padding on background to text
  padding: number;
  /// If true, no background will be rendered
  noBackground?: boolean;
}

interface TextContext {
  edit?: any;
  /// If the textbox is being created from the last action
  create?: boolean;
  pasted?: boolean;
  options?: any; // Options
}

const makeBackgroundColor = (
  instance: any,
  color?: string,
  opacity?: number
) => {
  if (typeof opacity !== 'number') {
    opacity = (instance.options && instance.options.backgroundOpacity) || 100;
  }

  return (
    (color ||
      (instance.options && instance.options.backgroundColor) ||
      '#000000') +
    ((Math.round(opacity * 2.55) || 0) < 16 ? '0' : '') +
    (Math.round(opacity * 2.55) || 0).toString(16)
  );
};

const textOptions = (instance) => {
  const options: Options = {
    background: makeBackgroundColor(instance, null, null), // TODO Remove nulls added because of tests
    color: instance.options.color || instance.color,
    fontSize: instance.options.fontSize,
    padding: instance.options.padding,
    noBackground: undefined // TODO added because of tests
  };

  if (!instance.options.background) {
    options.noBackground = true;
  }

  return options;
};

const createPresetButton = (settings) => {
  const button = document.createElement('button');
  button.className = 'preset';
  const div = document.createElement('div');
  div.className = 'text';
  const inner = document.createElement('div');
  div.appendChild(inner);
  inner.innerText = 'A';
  if (settings.background) {
    inner.style.padding = settings.padding + 'px';
    inner.style.background = makeBackgroundColor(
      null,
      settings.backgroundColor,
      settings.backgroundOpacity
    );
  }
  inner.style.color = settings.color;
  inner.style.fontSize = settings.fontSize + 'px';
  button.appendChild(div);

  const parts = [`has ${settings.fontSize} pixel font`];
  if (settings.background) {
    parts.push(
      `a ${colorally(
        settings.backgroundColor
      ).name.toLowerCase()} background color`
    );
    if (settings.backgroundOpacity !== 100) {
      parts.push(`a background opacity of ${settings.backgroundOpacity}%`);
    }
    parts.push(`${settings.padding} pixel padding`);
  }
  button.title =
    `Use ${colorally(settings.color).name.toLowerCase()} textbox that ` +
    parts.join(', ') +
    '\nClick to use and double click to remove the preset';

  return button;
};

const stopEditing = (instance, event, context) => {
  if (context.edit) {
    if (!context.edit.removed) {
      const textbox = context.edit.close();
      if (context.create && !textbox.data.content) {
        instance.drawing.delete(textbox);
      }
    }
    context.create = null;
    context.edit = null;
  }
};

export default {
  name: 'Textbox',
  description: 'Textbox drawing and editing tool',
  icon: 'font',
  cursor: 'text',
  requiredPermissions: ['edit'],
  mappings: [
    {
      active: true,
      type: 'paste',
      action: 'paste'
    },
    {
      active: true,
      type: 'click',
      filter: {
        $or: {
          button: 0,
          touches: 1
        }
      },
      action: 'text',
      chainActions: [
        {
          type: 'keyup',
          filter: {
            key: 'Escape'
          },
          action: 'cancel'
        }
      ]
    }
  ],
  actions: {
    paste: {
      handler: (instance, event: any, context) => {
        if (
          event.clipboardData &&
          ((event.clipboardData.items.length === 1 &&
            event.clipboardData.items[0].kind === 'string') ||
            (event.clipboardData.items.length === 2 &&
              event.clipboardData.items[0].kind === 'string' &&
              event.clipboardData.items[1].kind === 'string'))
        ) {
          if (!context.edit) {
            // Create a new textbox at the event point
            event.clipboardData.items[0].getAsString((text) => {
              context.edit = instance.drawing.createTextbox(
                instance.drawing.lastPointerCoordinates,
                text,
                textOptions(instance)
              ).edit;
              context.create = true;
            });
          }
          return true;
        }
        // TODO Can't fall through at the moment
      },
      allowDefault: (instance, event: any, context) => {
        if (
          event.clipboardData &&
          ((event.clipboardData.items.length === 1 &&
            event.clipboardData.items[0].kind === 'string') ||
            (event.clipboardData.items.length === 2 &&
              event.clipboardData.items[0].kind === 'string' &&
              event.clipboardData.items[1].kind === 'string')) &&
          context.edit &&
          !context.pasted
        ) {
          return true;
        }

        return false;
      }
    },
    text: {
      handler: (instance, event: any, context) => {
        if (event.item && event.item.type === 'Textbox') {
          if (context.edit) {
            if (context.edit.removed) {
              context.edit = null;
            } else {
              const textbox = context.edit.close();
              if (context.create && !textbox.data.content) {
                instance.drawing.delete(textbox);
              }
            }
            context.create = null;
          }
          // Edit the textbox
          context.edit = instance.drawing.editTextbox(event.item);
        } else {
          if (context.edit) {
            if (!context.edit.removed) {
              const textbox = context.edit.close();
              if (context.create && !textbox.data.content) {
                instance.drawing.delete(textbox);
              }
            }
            context.create = null;
            context.edit = null;
            return false;
          }

          // Create a new textbox at the event point
          context.edit = instance.drawing.createTextbox(
            event.middleCoordinates,
            null,
            textOptions(instance)
          ).edit;
          context.create = true;
        }

        return true;
      }
    },
    stop: {
      handler: stopEditing
    },
    cancel: {
      handler: (instance, event, context) => {
        if (context.edit) {
          if (!context.edit.removed) {
            const textbox = context.edit.cancel();
            if (context.create && !textbox.data.content) {
              instance.drawing.delete(textbox);
            }
          }
          context.edit = null;
          context.create = null;
        }
      }
    },
    optionChange: {
      handler: (instance, event: any, context) => {
        let selected = instance.drawing.selected;

        if (selected.length) {
          selected = selected.filter((element) => element.type === 'Textbox');

          if (selected.length) {
            const value = event.value;
            let option = event.option;
            switch (event.option) {
              case 'background':
                option = 'addBackground';
                break;
              case 'backgroundColor':
              case 'backgroundOpacity':
              case 'fontSize':
              case 'padding':
              case 'color':
                break;
              default:
                return;
            }
            instance.drawing.setProperty(selected, option, value);
          }
        }
      }
    },
    optionsOpen: {
      handler: (instance, event: any, context) => {
        let selected = instance.drawing.selected;

        if (selected.length) {
          selected = selected.filter((element) => element.type === 'Textbox');

          if (selected.length) {
            context.options = { ...event.form.value };
            // Remove options that should not be restored
            delete context.options['presets'];
            event.form.setValue('selected', true);
          }
        }
      }
    },
    optionsClose: {
      handler: (instance, event: any, context) => {
        if (context.options) {
          instance.setOptions(context.options);
          context.options = null;
          event.form.setValue('selected', null);
        }
      }
    },
    unselect: {
      handler: (instance, event: any, context) => {
        event.form.setValue('selected', null);
        context.options = null;
        stopEditing(instance, event, context);
        instance.drawing.deselectAll();
      }
    },
    savePreset: {
      handler: (instance, event: any, context) => {
        const value = event.form.value;
        const preset = {
          color: value.color || instance.color,
          fontSize: value.fontSize,
          background: value.background,
          backgroundColor: value.backgroundColor,
          padding: value.padding,
          backgroundOpacity: value.backgroundOpacity
        };
        const presets = [...(value.presets || []), preset];
        event.form.setValue('presets', presets, true);

        // Save the presets to the user
        instance.app.user.saveSetting('tools.textbox.presets', presets);
      }
    }
  },
  options: {
    unselect: {
      type: 'button',
      title:
        'As text is selected, changes made will only affect the ' +
        'selected text. Click here to unselect the text',
      attributes: {
        hidden: (values) => {
          return !values.selected;
        },
        class: 'linkButton'
      },
      handler: 'unselect'
    },
    presets: {
      title: 'Presets',
      type: 'custom',
      create: (div, formObject) => {
        const createButtons = (preset, index) => {
          const button = createPresetButton(preset);
          button.addEventListener('click', (event) => {
            if (event.defaultPrevented) {
              return;
            }
            event.preventDefault();
            formObject.setValue('color', preset.color, true);
            formObject.setValue('fontSize', preset.fontSize, true);
            formObject.setValue('background', preset.background, true);
            formObject.setValue(
              'backgroundColor',
              preset.backgroundColor,
              true
            );
            formObject.setValue('padding', preset.padding, true);
            formObject.setValue(
              'backgroundOpacity',
              preset.backgroundOpacity,
              true
            );
          });
          button.addEventListener('dblclick', (event) => {
            if (event.defaultPrevented) {
              return;
            }
            event.preventDefault();
            const presets = formObject.value.presets;
            presets.splice(index, 1);
            formObject.setValue('presets', presets, true);
            formObject.context?.app?.user.saveSetting(
              'tools.textbox.presets',
              presets
            );
          });
          div.appendChild(button);
        };

        if (formObject.value.presets) {
          div.innerHTML = '<label>Presets</label>';
          formObject.value.presets.forEach(createButtons);
        }

        formObject.addValueWatch('presets', (presets) => {
          // Clear div
          div.innerHTML = '<label>Presets</label>';

          presets.forEach(createButtons);
        });
      }
    },
    saveAsPreset: {
      title: 'Save settings as preset',
      type: 'button',
      handler: 'savePreset'
    },

    color: {
      title: 'Color',
      type: 'string',
      format: 'color',
      allowConfigured: 'default color'
    },
    fontSize: {
      title: 'Text size',
      description: 'The size of the text',
      type: 'number',
      default: 12,
      range: true,
      attributes: {
        min: 6,
        max: 200
      }
    },
    background: {
      title: 'Add background',
      type: 'boolean',
      default: true
    },
    backgroundColor: {
      title: 'Background Color',
      type: 'string',
      format: 'color',
      default: '#ffffff'
    },
    padding: {
      title: 'Background Padding',
      description: 'The background',
      type: 'number',
      default: 5,
      range: true,
      attributes: {
        min: 0,
        max: 50
      }
    },
    backgroundOpacity: {
      title: 'Background Opacity',
      type: 'number',
      default: 80,
      range: true,
      attributes: {
        min: 0,
        max: 100
      }
    }
  }
} as Tool<TextContext>;
