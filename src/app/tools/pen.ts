import { Tool } from '../../typings/Tool';
import colorally from 'colorally';

interface PenContext {
  initialCoordinates?: Coordinates;
  attachmentType?: AttachmentType;
  path?: Path;
  fullPath?: Path;
  previewPath?: Path;
  options?: {
    [key: string]: any;
  };
}

const pathOptions = (instance) => ({
  opacity: instance.options.opacity && instance.options.opacity / 100,
  strokeWidth: instance.options.thickness,
  strokeColor: instance.options.color || instance.color,
  strokeCap: instance.options.strokeCap
});

const previewPathOptions = (instance) => {
  const options = pathOptions(instance);
  options.opacity *= 0.2;
  options.selected = false;
  return options;
};

/**
 * Finsih a line that is currently being drawn
 */
const finishLine = (
  instance,
  event,
  context: PenContext,
  includePosition: boolean = true
) => {
  if (context.path && !context.path.removed) {
    if (context.previewPath) {
      context.previewPath.remove();
    }
    if (
      includePosition &&
      event.middleCoordinates &&
      context.attachmentType !== 'click'
    ) {
      context.path.addPoint(event.middleCoordinates, true);
    }
    context.path.finish(
      context.attachmentType === 'click' ? 0 : instance.options.smoothing
    );
  }
  context.path = null;
  context.previewPath = null;
};

const createPresetButton = (settings) => {
  const button = document.createElement('button');
  let div = document.createElement('div');
  button.className = 'preset';
  div.style.height = '100%';
  div.style.maxWidth = '100%';
  div.style.width = settings.thickness + 'px';
  div.style.background = settings.color;
  div.style.opacity = settings.opacity + '%';
  button.appendChild(div);
  if (settings.thickness >= 20) {
    div = document.createElement('div');
    div.className = 'label';
    div.innerText = settings.thickness;
    button.appendChild(div);
  }
  const parts = [`is ${settings.thickness} pixels thick`];
  if (settings.opacity !== 100) {
    parts.push(`has an opacity of ${settings.opacity}%`);
  }
  parts.push(`has ${settings.strokeCap} line endings`);
  button.title =
    `Use ${colorally(settings.color).name.toLowerCase()} pen that ` +
    parts.join(', ') +
    '\nClick to use and double click to remove the preset';

  return button;
};

export default {
  name: 'Pen',
  description: 'Pen tool',
  icon: 'pencil',
  cursor: 'crosshair',
  requiredPermissions: ['edit'],
  draggable: true,
  mappings: [
    {
      active: true,
      type: 'mousedown',
      filter: {
        button: 0,
        movement: true
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'mousemove'
        },
        {
          action: 'stop',
          type: 'mouseup',
          filter: {
            button: 0
          }
        },
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Delete'
          }
        },
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Escape'
          }
        },
        {
          type: 'contextmenu'
        },
        {
          action: 'cancel',
          type: 'mouseup',
          filter: {
            button: 2
          }
        }
      ]
    },
    {
      active: true,
      type: 'click',
      filter: {
        button: 0
      },
      action: 'click',
      chainActions: [
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Delete'
          }
        },
        {
          action: 'cancel',
          type: 'keyup',
          filter: {
            key: 'Escape'
          }
        },
        {
          type: 'contextmenu',
          action: 'stop'
        },
        {
          action: 'stop',
          type: 'mouseup',
          filter: {
            button: 2
          }
        },
        {
          type: 'mousemove',
          action: 'drawPreview'
        },
        {
          type: 'mousedown',
          filter: {
            button: 0,
            movement: true
          },
          action: 'start',
          chainActions: [
            {
              action: 'update',
              type: 'mousemove'
            },
            {
              type: 'mouseup',
              endChain: true,
              action: 'resumeClick'
            }
          ]
        }
      ]
    },
    {
      active: true,
      type: 'touchstart',
      filter: {
        touches: 1
      },
      action: 'start',
      chainActions: [
        {
          action: 'update',
          type: 'touchmove',
          filter: {
            touches: 1
          }
        },
        {
          action: 'stop',
          type: 'touchend',
          filter: {
            touches: 0
          }
        }
      ]
    }
  ],
  actions: {
    start: {
      handler: (instance, event, context: PenContext) => {
        if (context.previewPath) {
          context.previewPath.remove();
          context.previewPath = null;
        }
        if (!context.path) {
          instance.drawing.deselectAll();
          context.initialCoordinates = event.middleCoordinates;
          context.attachmentType = 'drag';
          context.path = instance.drawing.createPath(pathOptions(instance));
        } else {
          context.path.addPoint(event.middleCoordinates, true);
          if (context.attachmentType === 'click') {
            context.fullPath = context.path;
            context.path = instance.drawing.createPath(pathOptions(instance));
          }
        }
        context.path.addPoint(event.middleCoordinates, true);
        return true;
      }
    },
    update: {
      handler: (instance, event, context: PenContext) => {
        if (event.type === 'mousemove' && !event.originalEvent.buttons) {
          return finishLine(instance, event, context, false);
        }
        if (context.path.removed) {
          return;
        }
        context.path.addPoint(event.middleCoordinates, true);
        return true;
      }
    },
    click: {
      handler: (instance, event, context: PenContext) => {
        if (!context.path) {
          instance.drawing.deselectAll();
          context.initialCoordinates = event.middleCoordinates;
          context.attachmentType = 'click';
          context.path = instance.drawing.createPath(pathOptions(instance));
        } else if (context.path.removed) {
          return;
        }
        if (context.previewPath) {
          context.previewPath.remove();
          context.previewPath = null;
        }
        context.previewPath = instance.drawing.createPath(
          previewPathOptions(instance),
          false
        );
        context.path.select();
        context.previewPath.addPoint(event.middleCoordinates, true);
        context.path.addPoint(event.middleCoordinates, true);
        return true;
      }
    },
    resumeClick: {
      handler: (instance, event, context: PenContext) => {
        if (context.fullPath) {
          if (context.fullPath.removed) {
            return;
          }
          // Smooth the line
          context.path.smooth();
          // Join the paths together
          context.fullPath.join(context.path);
          // Clean up
          context.path.remove();
          context.path = context.fullPath;
          context.path.select();
        }
        context.previewPath = instance.drawing.createPath(
          previewPathOptions(instance),
          false
        );
        context.previewPath.addPoint(event.middleCoordinates, true);
        context.path.select();

        return false;
      }
    },
    drawPreview: {
      handler: (instance, event, context: PenContext) => {
        if (context.previewPath) {
          context.previewPath.removePoint(1);
          context.previewPath.addPoint(event.middleCoordinates, true);
        }
        return true;
      }
    },
    stop: {
      handler: (instance, event, context: PenContext) => {
        return finishLine(instance, event, context);
      }
    },
    cancel: {
      handler: (instance, event, context: PenContext) => {
        if (context.path) {
          context.path.remove();
          if (context.previewPath) {
            context.previewPath.remove();
          }
        }
        context.path = null;
        context.previewPath = null;
      }
    },
    optionChange: {
      handler: (instance, event, context: PenContext) => {
        let selected = instance.drawing.selected;

        if (selected.length) {
          selected = selected.filter((element) => element.type === 'Path');

          if (selected.length) {
            let option;
            let value;
            switch (event.option) {
              case 'color':
                option = 'strokeColor';
                value = event.value;
                break;
              case 'thickness':
                option = 'strokeWidth';
                value = event.value;
                break;
              case 'strokeCap':
                option = 'strokeCap';
                value = event.value;
                break;
              case 'opacity':
                option = 'opacity';
                value = event.value / 100;
                break;
              case 'smoothing':
              default:
                return;
            }
            instance.drawing.setProperty(selected, option, value);
          }
        }
      }
    },
    optionsOpen: {
      handler: (instance, event, context: PenContext) => {
        let selected = instance.drawing.selected;

        if (selected.length) {
          selected = selected.filter((element) => element.type === 'Path');

          if (selected.length) {
            context.options = { ...event.form.value };
            // Remove options that should not be restored
            delete context.options['presets'];
            event.form.setValue('selected', true);
          }
        }
      }
    },
    optionsClose: {
      handler: (instance, event, context: PenContext) => {
        // Reset pen options to as they were before options were open
        // when a line is currently selected
        if (context.options) {
          instance.setOptions(context.options);
          context.options = null;
          event.form.setValue('selected', null);
        }
      }
    },
    unselect: {
      handler: (instance, event, context: PenContext) => {
        event.form.setValue('selected', null);
        context.options = null;
        instance.drawing.deselectAll();
      }
    },
    savePreset: {
      handler: (instance, event, context: PenContext) => {
        const value = event.form.value;
        const preset = {
          color: value.color || instance.color,
          opacity: value.opacity,
          thickness: value.thickness,
          smoothing: value.smoothing,
          strokeCap: value.strokeCap
        };
        const presets = [...(value.presets || []), preset];
        event.form.setValue('presets', presets, true);

        // Save the presets to the user
        instance.app.user.saveSetting('tools.pen.presets', presets);
      }
    }
  },
  options: {
    unselect: {
      type: 'button',
      title:
        'As lines are selected, changes made will only affect the ' +
        'selected lines. Click here to unselect the lines',
      attributes: {
        hidden: (values) => {
          return !values.selected;
        },
        class: 'linkButton'
      },
      handler: 'unselect'
    },
    presets: {
      title: 'Presets',
      type: 'custom',
      create: (div, formObject) => {
        const createButtons = (preset, index) => {
          const button = createPresetButton(preset);
          button.addEventListener('click', (event) => {
            if (event.defaultPrevented) {
              return;
            }
            event.preventDefault();
            formObject.setValue('color', preset.color, true);
            formObject.setValue('opacity', preset.opacity, true);
            formObject.setValue('thickness', preset.thickness, true);
            formObject.setValue('smoothing', preset.smoothing, true);
            formObject.setValue('strokeCap', preset.strokeCap, true);
          });
          button.addEventListener('dblclick', (event) => {
            if (event.defaultPrevented) {
              return;
            }

            event.preventDefault();
            const presets = formObject.value.presets;
            presets.splice(index, 1);
            formObject.setValue('presets', presets, true);
            formObject.context?.app?.user.saveSetting(
              'tools.pen.presets',
              presets
            );
          });
          div.appendChild(button);
        };

        if (formObject.value.presets) {
          div.innerHTML = '<label>Presets</label>';
          formObject.value.presets.forEach(createButtons);
        }

        formObject.addValueWatch('presets', (presets) => {
          // Clear div
          div.innerHTML = '<label>Presets</label>';

          presets.forEach(createButtons);
        });
      }
    },
    saveAsPreset: {
      title: 'Save settings as preset',
      type: 'button',
      handler: 'savePreset'
    },
    color: {
      title: 'Color',
      type: 'string',
      format: 'color',
      allowConfigured: 'default color'
    },
    thickness: {
      title: 'Thickness',
      description: 'The thickness of the line in pixels',
      type: 'number',
      default: 2,
      range: true,
      attributes: {
        min: 1,
        max: 200
      }
    },
    opacity: {
      title: 'Opacity',
      description: 'The opacity of the line as a percentage',
      type: 'number',
      default: 100,
      range: true,
      attributes: {
        min: 0,
        max: 100
      }
    },
    smoothing: {
      title: 'Smoothing',
      description: 'The smoothing of the line when drawing freehand',
      type: 'number',
      default: 10,
      range: true,
      attributes: {
        min: 0,
        max: 200,
        hidden: (values) => values.selected
      }
    },
    strokeCap: {
      title: 'Line Ending',
      description: 'The shape of the end of the lines',
      type: 'options',
      default: 'round',
      options: [
        {
          label: 'rounded',
          value: 'round'
        },
        {
          label: 'squared',
          value: 'square'
        },
        {
          label: 'none',
          value: 'none'
        }
      ]
    }
  }
} as Tool<PenContext>;
