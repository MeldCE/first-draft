export const createOverlay = (container: HTMLElement) => {
  const overlay = document.createElement('div');
  overlay.className = 'cover';
  const overlayContainer = document.createElement('div');
  overlayContainer.className = 'overlay';
  overlay.appendChild(overlayContainer);
  container.appendChild(overlay);

  return {
    close: () => {
      overlay.remove();
    },
    get element() {
      return overlayContainer;
    }
  };
};
