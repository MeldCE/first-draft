import { ClientUser } from '../../typings/User';
import { UserModule } from '../../typings/Modules';

import createEventer from '../../lib/eventer';
import makeId from '../../lib/id';
import * as sha from '../../lib/sha';
import { arrayContainsValues, setDeep } from '../../lib/utils';
import { createForm } from './form';
import { createOverlay } from './overlay';
import { subscribe, subscribeObject } from '../../lib/store';

const storagePrefix = 'fd:user:';

interface LoginOptions {
  requirePassword?: boolean;
  requireUsername?: boolean;
  requirePermissions?: string[];
  /// ID of message requesting authentication from server
  id?: string;
  /// ID of challenge sent by server
  challenge?: string;
  /// Nonce to use for authentication
  nonce?: string;
  /// Message that should be displayed to the user
  message?: string;
}

interface AuthenticationRequests extends LoginOptions {
  promise: {
    resolve: () => void;
    reject: (error: string) => void;
  };
}

interface StorePasswordOptions {
  check?: boolean;
  remember?: boolean;
}

/**
 * Login has two checkboxes
 * - Remember me - will remember the username using localStorage, otherwise
 *   it will be put in localStorage
 * - Keep me logged in - if allowed in config and password given, will
 *   remember the password hash in localStorage, otherwise will remember the
 *   password in sessionStorage
 */

export const createUser = (app) => {
  let editHash = null,
    userHash = null,
    draftHash = null;
  let userPrivileges = null;
  let user: ClientUser = null;
  let login = null;
  const containerElement = document.getElementById('canvasContainer');
  /// Array of AuthenticationRequests that are still awaiting authentication
  let awaitingAuthentication: AuthenticationRequests[] = [];
  const eventer = createEventer(['authenticate', 'update']);
  /// Authentication requests that have been sent to the server
  const authenticationAttempt = {};
  /// Store privileges from each authentication
  let privileges = null;
  let rememberMe = true;
  let rememberPassword = false;

  /**
   * Create a function to run every time the id for the given type changes
   *
   * @param idFor What the id is for - draftId for a draft, true for the user
   *   false for the edit password and null for the current draft
   */
  const idCheck = (idFor: string | boolean | null) => {
    return (key, newValue, oldValue) => {
      if (newValue && oldValue) {
        // Return as will be forced to reauth by server
        return;
      }
      if (newValue !== oldValue) {
        reauthenticate(idFor === null ? app.draftId : idFor);
      }
    };
  };

  /**
   * Check the given id is unique, otherwise return a new auth id
   *
   * @returns The given id if given and unique or a new id
   */
  const getAuthId = (id?: string) => {
    id = id || makeId();
    for (let i = 0; i < 5; i++) {
      if (!authenticationAttempt[id]) {
        return id;
      }

      id = makeId();
    }

    throw new Error('Could not get unique id');
  };

  /**
   * Store the given hash in to correct storage
   *
   * @param hash Hash to store
   * @param hashFor The thing the hash is for - the draft ID for a draft edit
   *   password, true for the current user or falsey for the edit password
   */
  const storeHash = (
    hash: string,
    hashFor?: string | true,
    remember?: boolean = null
  ) => {
    if (hashFor === true) {
      if (userHash) {
        userHash.set(hash);
        if (remember !== null) {
          userHash.remember(remember);
        }
        return;
      }
    } else if (hashFor) {
      if (hashFor === app.draftId && draftHash) {
        if (remember !== null) {
          draftHash.remember(remember);
        }
        draftHash.set(hash);
        return;
      }
    } else if (editHash) {
      if (remember !== null) {
        editHash.remember(remember);
      }
      editHash.set(hash);
      return;
    }

    if (hash) {
      if (remember) {
        localStorage.setItem(getHashKey(hashFor), JSON.stringify(hash));
        sessionStorage.removeItem(getHashKey(hashFor));
      } else {
        sessionStorage.setItem(getHashKey(hashFor), JSON.stringify(hash));
        localStorage.removeItem(getHashKey(hashFor));
      }
    } else {
      localStorage.removeItem(getHashKey(hashFor));
    }
  };

  /**
   * Get the Storage key for the hash or privileges for the given item
   *
   * @param type 'id' for hash, 'privileges' for privileges
   * @param hashFor The thing the key is for - the draft ID for a draft edit
   *   password, true for the current user or falsey for the edit password
   */
  const getHashKey = (keyFor?: string | true) => {
    if (keyFor === true) {
      return storagePrefix + 'user:id';
    } else if (keyFor) {
      return storagePrefix + 'ids:' + keyFor;
    } else {
      return storagePrefix + 'id';
    }
  };

  const updateUser = () => {
    const promises = [];
    if (user.get('id')) {
      if (!userHash) {
        promises.push(
          subscribe(storagePrefix + 'user:id', idCheck(true), false).then(
            (hashSubscription) => {
              userHash = hashSubscription;
            }
          )
        );
      }
    } else {
      if (userHash) {
        userHash.unsubscribe();
        userHash = null;
      }
      if (userPrivileges) {
        userPrivileges.unsubscribe();
        userPrivileges = null;
      }
    }

    return Promise.all(promises);
  };

  /**
   * Clear and close login screen
   */
  const clearLogin = () => {
    if (login) {
      login.login.remove();
      login.overlay.close();
      login = null;
    }
  };

  /**
   * Show the login screen if not already showing
   *
   * @param requirePassword Require the user to enter a password
   * @param haveUser The user is a valid user
   *
   * @returns A Promise that will resolve once the user has authenticated or
   *   cancelled authenticating
   */
  const showLogin = (options?: boolean | LoginOptions) => {
    if (typeof options === 'boolean') {
      options = {
        requirePassword: options
      };
    }
    if (login) {
      if (!login.id && options && options.id) {
        login.id = options.id;
      }
      if (!login.challenge && options && options.challenge && options.nonce) {
        login.challenge = options.challenge;
        login.nonce = options.nonce;
      }

      if (options.message) {
        login.login.setMessage(options.message);
      }

      const request = {
        ...(options || {}),
        promise: {}
      };
      const promise = new Promise((resolve, reject) => {
        request.promise.resolve = resolve;
        request.promise.reject = reject;
      });
      awaitingAuthentication.push(request);
      return promise;
    }

    const loginForm = {
      title: 'Your Details',
      elements: {
        username: null,
        rememberMe: null,
        password: {
          type: 'string',
          format: 'password',
          title: 'Password',
          attributes: {
            autocomplete: 'current-password',
            required: (options && options.requirePassword) || null
          }
        },
        rememberPassword: {
          type: 'boolean',
          title: 'Remember Password',
          attributes: {
            disabled: (data) => {
              return !data.password;
            }
          }
        }
      },
      buttons: [
        {
          type: 'submit',
          label: 'Submit',
          handler: (values, validity) => {
            rememberMe = values.rememberMe;
            rememberPassword = values.rememberPassword;
            const username = values.username || null;
            const id = getAuthId(login.id);
            if (values.password) {
              const thisHash = sha.getSha(values.password);
              authenticationAttempt[id] = {
                details: {
                  username
                },
                hash: thisHash,
                state: 'challenge',
                rememberMe,
                rememberPassword
              };
              if (login.challenge) {
                authenticationAttempt[id].state = 'result';
                app.socket.emit(app.config.baseEvent + 'auth:authenticate', {
                  id,
                  username,
                  challenge: login.challenge,
                  answer: sha.signChallenge(login.nonce, thisHash)
                });
                login.challenge = null;
                login.nonce = null;
              } else {
                app.socket.emit(app.config.baseEvent + 'auth:challenge', {
                  id,
                  username
                });
              }
            } else if (username) {
              authenticationAttempt[id] = {
                details: {
                  username
                },
                state: 'result'
              };
              app.socket.emit(app.config.baseEvent + 'auth:authenticate', {
                id,
                username
              });
            } else if (
              !(options && (options.requirePassword || options.requireUsername))
            ) {
              clearLogin();
            } else if (options) {
              const requireItems = [];
              if (options.requireUsername) {
                requireItems.push('username');
              }
              if (options.requirePassword) {
                requireItems.push('password');
              }

              login.login.setMessage(
                'Require a ' + requireItems.join('and'),
                'error'
              );
            }
          }
        },
        {
          label: 'Cancel',
          handler: () => {
            if (awaitingAuthentication.length) {
              awaitingAuthentication.forEach((request) => {
                if (request.id) {
                  app.socket.emit(app.config.baseEvent + 'auth:cancel', {
                    id: request.id,
                    challenge: request.challenge
                  });
                }
                request.promise.reject('cancelled by user');
              });
              awaitingAuthentication = [];
            }
            clearLogin();
          }
        }
      ],
      disabled: (data) =>
        !(
          (app.config.editPassword ||
            app.config.hideUsername ||
            data.username) &&
          (!(options && options.requirePassword) || data.password)
        )
    };

    if ((options && options.requireUsername) || !app.config.hideUsername) {
      loginForm.elements.username = {
        name: 'username',
        title: 'Username',
        type: 'string'
      };
      loginForm.elements.rememberMe = {
        name: 'rememberMe',
        type: 'boolean',
        title: 'Remember me',
        attributes: {
          disabled: (data) => {
            return !data.username;
          }
        }
      };
    }

    const overlay = createOverlay(containerElement);

    login = {
      overlay,
      id: options && options.id,
      challenge: options && options.nonce && options.challenge,
      nonce: options && options.challenge && options.nonce,
      login: createForm(loginForm, overlay.element, {
        username: (user && (user.username || user.name)) || '',
        rememberMe: user.get('username') ? user.remember() : true,
        rememberPassword
      })
    };

    if (options.message) {
      login.login.setMessage(options.message);
    }

    let resolve, reject;
    const promise = new Promise((promiseResolve, promiseReject) => {
      resolve = promiseResolve;
      reject = promiseReject;
    });

    awaitingAuthentication.push({
      ...(options || {}),
      promise: {
        resolve,
        reject
      }
    });

    return promise;
  };

  /**
   * Check the password is correct for the given type
   *
   * @param password Password to check
   * @param passwordFor Draft ID if the password is for a draft edit password,
   *   true for if the password is for the current user, or falsey for if the
   *   password is for the server edit password
   *
   * @returns A Promise that resolves to whether the password is correct or
   *   rejects is a code was returned from the check
   */
  const checkPassword = (password: string, passwordFor: string | boolean) => {
    return new Promise((resolve, reject) => {
      const details = {};
      if (passwordFor === true) {
        username = user.get('username');

        if (!username) {
          reject(new Error('No current user'));
          return;
        }
        details.username = username;
        details.type = 'user';
      } else if (passwordFor) {
        if (!app.config.draftEditPasswords) {
          resolve(null);
          return;
        }

        details.draftId = passwordFor;
        details.type = 'draftEdit';
      } else {
        details.type = 'edit';
      }
      const id = getAuthId();

      authenticationAttempt[id] = {
        details,
        type: 'check',
        hash: sha.getSha(password),
        state: 'challenge',
        result: (data) => {
          if (data.code) {
            reject(data.code);
          } else {
            resolve(data.result);
          }
        }
      };

      app.socket.emit(app.config.baseEvent + 'auth:challenge', {
        id
      });
    });
  };

  /**
   * Check authentication for the user, draft, or server
   *
   * @param draftId Either true to check the authentication of the user,
   *   a draft ID to check the authentication for a draft edit password, or
   *   falsey to check the server edit password
   *
   * @returns A Promise that resolves to the result of the authentication
   *   check
   */
  const reauthenticate = (draftId?: string | true | null): Promise<boolean> => {
    let hash;
    const details = {};
    if (draftId === null) {
      const promises = [];

      if (userHash && userHash.get()) {
        promises.push(reauthenticate(true));
      }
      if (app.draftId && draftHash && draftHash.get()) {
        promises.push(reauthenticate(app.draftId));
      }
      if (editHash && editHash.get()) {
        promises.push(reauthenticate());
      }

      return Promise.all(promises);
    } else if (draftId === true) {
      if (!(user && user.get('id'))) {
        return Promise.resolve(false);
      }
      if (!(userHash && userHash.get())) {
        // TODO Force login
        return Promise.resolve(false);
      }

      hash = userHash.get();
      details.userId = user.get('id');
    } else if (draftId) {
      if (draftId === app.draftId) {
        if (!(draftHash && draftHash.get())) {
          return Promise.resolve(false);
        }

        details.draftId = draftId;
        hash = draftHash.get();
      } else {
        return Promise.resolve(false);
      }
    } else {
      if (!(editHash && editHash.get())) {
        return Promise.resolve(false);
      }
      hash = editHash.get();
    }

    return new Promise((resolve, reject) => {
      const id = getAuthId();

      authenticationAttempt[id] = {
        details,
        hash,
        hashFor: draftId || false,
        state: 'challenge',
        result: (data) => {
          resolve(data.success || false);
        }
      };

      app.socket.emit(app.config.baseEvent + 'auth:challenge', {
        id
      });
    });
  };

  const hasPermissions = (permissions: string | string[]) => {
    if (!Array.isArray(permissions)) {
      permissions = [permissions];
    }

    if (
      permissions.length === 1 &&
      permissions[0] === 'edit' &&
      !(
        app.config.editPassword ||
        (app.config.draftEditPasswords &&
          app.draft &&
          app.draft.config &&
          app.draft.config.editPassword)
      )
    ) {
      return true;
    }

    return (
      (privileges && arrayContainsValues(privileges, permissions)) || false
    );
  };

  subscribeObject(storagePrefix + 'user', updateUser).then((subscription) => {
    user = subscription;
    if (subscription.get('id')) {
      updateUser();
    }
    rememberMe = user.remember();
  });

  if (app.config.draftEditPasswords) {
    subscribe(storagePrefix + 'ids:' + app.draftId, idCheck(null), false).then(
      (subscription) => {
        draftHash = subscription;
      }
    );
  }

  if (app.config.editPassword) {
    subscribe(storagePrefix + 'id', idCheck(false), false).then(
      (subscription) => {
        editHash = subscription;
      }
    );
  }

  // Listen for authentication events
  // The challenge event is received when a challenge has been requested from
  // the server
  app.socket.on(app.config.baseEvent + 'auth:challenge', (data) => {
    const attempt = data.id && authenticationAttempt[data.id];

    if (attempt) {
      if (attempt.state === 'challenge') {
        attempt.state = 'result';

        if (data.challenge && data.nonce) {
          const answer = sha.signChallenge(data.nonce, attempt.hash);

          app.socket.emit(
            app.config.baseEvent +
              'auth:' +
              (attempt.type === 'check' ? 'check' : 'authenticate'),
            {
              id: data.id,
              ...attempt.details,
              challenge: data.challenge,
              answer
            }
          );
        }
        // TODO Request another challenge?
      }
    } else {
      let draftId;
      let username;
      if (data.type && !data.newPassword) {
        let hash;
        switch (data.type) {
          case 'user':
            hash = userHash && userHash.get();
            break;
          case 'draftEdit':
            draftId = data.draftId || app.draftId;
            if (draftId === app.draftId) {
              hash = draftHash && draftHash.get();
            } else {
              hash = localStorage.getItem(getHashKey(draftId));
              if (hash) {
                try {
                  hash = JSON.parse(hash);
                } catch {
                  hash = null;
                }
              }
            }
            break;
          case 'edit':
            hash = editHash && editHash.get();
            break;
        }

        if (hash) {
          // Attempt authentication
          const id = getAuthId(data.id);
          const answer = sha.signChallenge(data.nonce, hash);
          authenticationAttempt[id] = {
            state: 'result',
            details: {
              draftId,
              username
            },
            result: (success) => {
              if (!success) {
                showLogin({
                  id: data.id,
                  type: data.type,
                  draftId: data.draftId,
                  requirePassword: data.newPassword,
                  message: data.message
                });
              }
            }
          };

          app.socket.emit(app.config.baseEvent + 'auth:authenticate', {
            ...authenticationAttempt[id].details,
            id,
            challenge: data.challenge,
            answer
          });
          return;
        }
      }

      showLogin({
        ...data,
        requirePassword: data.newPassword
      });
    }
  });

  app.socket.on(app.config.baseEvent + 'auth:result', (data) => {
    const attempt = data.id && authenticationAttempt[data.id];
    if (attempt && attempt.state === 'result') {
      if (data.privileges) {
        privileges = data.privileges;
      }
      if (data.success) {
        let hashFor;
        if (data.user) {
          user.set(data.user);
          if (typeof attempt.rememberMe === 'boolean') {
            user.remember(attempt.rememberMe);
          }
        }
        if (data.user && data.user.id) {
          hashFor = true;
        } else if (data.draftId) {
          hashFor = data.draftId;
        }
        if (attempt.hash) {
          storeHash(attempt.hash, hashFor, attempt.rememberPassword);
        }

        let authenticationOk = true;
        let requireMorePermissions = false;
        let requireUsername = false;
        if (awaitingAuthentication.length) {
          let i = 0;
          while (i < awaitingAuthentication.length) {
            const request = awaitingAuthentication[i];
            if (request.requireUsername && !user.get('username')) {
              authenticationOk = false;
              requireUsername = true;
              i++;
              continue;
            }
            if (
              request.requirePermissions &&
              !hasPermissions(request.requirePermissions)
            ) {
              authenticationOk = false;
              requireMorePermissions = true;
              i++;
              continue;
            }
            request.promise.resolve(true);
            awaitingAuthentication.splice(i, 1);
          }
        }

        if (authenticationOk) {
          clearLogin();
        } else {
          const message = requireMorePermissions
            ? "You don't have enough permissions. Please try again"
            : requireUsername
            ? 'A username is required, please enter one'
            : null;
          if (login) {
            login.login.setMessage(message);
          } else {
            showLogin({
              requirePassword: requireMorePermissions,
              requireUsername
            });
          }
        }
      } else {
        if (data.username && user.get('username') === data.username) {
          user.delete(true);
        }
        if (attempt.hashFor === true) {
          if (userHash) {
            userHash.delete();
          }
        } else if (attempt.hashFor && attempt.hashFor === app.draftId) {
          if (draftHash) {
            draftHash.delete();
          }
        } else if (attempt.hashFor === false) {
          if (editHash) {
            editHash.delete();
          }
        }

        if (login) {
          login.login.setMessage('Please try again', 'error');
        }
        if (!data.user) {
          // TODO Fixup so doesn't show is auth fails for draft/server editPassword if automatic
          showLogin(true);
        }
      }
      if (attempt.result) {
        attempt.result(data.success);
      }
      eventer.emit('authenticate', data.success);
      delete authenticationAttempt[data.id];
    }
  });

  app.socket.on(app.config.baseEvent + 'auth:check', (data) => {
    const attempt = data.id && authenticationAttempt[data.id];
    if (attempt && attempt.type === 'check' && attempt.state === 'result') {
      if (attempt.result) {
        attempt.result({
          result: data.result,
          code: data.code
        });
      }

      delete authenticationAttempt[data.id];
    }
  });

  app.socket.on(app.config.baseEvent + 'auth:update', (data) => {
    if (Object.prototype.hasOwnProperty.call(data, 'privileges')) {
      privileges = data.privileges;

      eventer.emit('update', null);
    }
  });

  return {
    addEventListener: eventer.addEventListener,
    removeEventListener: eventer.removeEventListener,
    /**
     * Requests that the user authenticates to gain the given permissions or
     * a username
     *
     * @param permissions An arrary of permissions required
     * @param username If true, request the user gives a username
     *
     * @returns A Promise that resolves once the user has authenticated or
     *   rejects if the authentication is cancelled
     */
    requestAuthentication: (permissions?: string[], username?: boolean) => {
      // TODO Check if already have username and have permissions
      return showLogin({
        requirePermissions: permissions,
        requirePassword: Boolean(permissions), // TODO Better calculation
        requireUsername: username
      });
    },
    /**
     * Check that the given password matches the current password
     *
     * @param password Password to validate
     * @param passwordFor Either the ID of the draft the password is for, true
     *   when the password is for the user, or falsey when the password is
     *   for the edit password
     */
    checkPassword,
    /**
     * Verify and store a password if the password is validated
     *
     * @param password Password to store
     * @param passwordFor Either the ID of the draft the password is for, true
     *   when the password is for the user, or falsey when the password is
     *   for the edit password
     *
     * @returns A Promise that resolves to whether the password was stored
     */
    storePassword: (
      password: string,
      passwordFor: string | boolean,
      options: StorePasswordOptions
    ) => {
      if (!options.check) {
        storeHash(
          (password && sha.getSha(password)) || null,
          passwordFor,
          options.remember
        );
        return Promise.resolve();
      }

      if (typeof passwordFor === 'string' && passwordFor == app.draftId) {
        storeHash(sha.getSha(password), passwordFor, options.remember);
        return reauthenticate(passwordFor);
      }

      return checkPassword(password, passwordFor).then((success) => {
        if (success) {
          storeHash(sha.getSha(password), passwordFor, options.remember);
        }

        return success;
      });
    },
    /**
     * Check the stored hash if there is one is still value by requesting
     * and signing a authentication challenge
     */
    reauthenticate,
    hasPermissions,
    get username() {
      return user.get('username');
    },
    get user() {
      return user.get() || {};
    },
    get settings() {
      return user.get('settings') || {};
    },
    get token() {
      if (!nonce) {
        return null;
      }

      if (hash) {
        return sha.getSha(hash.value + nonce);
      }

      return sha.getSha(nonce);
    },
    saveSetting(path: string, value: any) {
      user.set(
        'settings',
        setDeep(user.get('settings') || null, path.split('.'), value)
      );
    }
  } as UserModule;
};
