import { Form, FormData } from '../../typings/Form';

const formatFieldTypes = ['color', 'password', 'url', 'email'];

export const createForm = (
  form: Form,
  container: HTMLElement,
  originalValues?: FormData,
  context?: { [key: string]: any }
) => {
  let fields: {
    [element: string]: {
      input: HTMLInputElement;
      [key: string]: HTMLElement;
    };
  } = {};
  let submit = null;
  const lastNeeded = {};
  let submitLastNeeded = null;
  const listeners = {
    __: null
  };
  let errorDiv = null;
  let formElement = document.createElement('form');
  formElement.className = 'form';
  let values;

  if (form.schema) {
    if (!form.elements) {
      form.elements = {};
    }

    const convert = (schema, name?: string) => {
      if (name && form.elements[name] === null) {
        return;
      }
      if (schema.type === 'object' && schema.properties) {
        return Object.entries(schema.properties).forEach(
          ([key, definition]) => {
            convert(definition, key);
          }
        );
      } else if (schema.type === 'array') {
        return;
      } else {
        form.elements[name] = schema;
      }
    };

    convert(form.schema);
  }

  if (!form.elements) {
    throw new Error('No form elements given');
  }

  /**
   * Create copies of the values and the validities with Proxies on them
   * and track what values are accessed
   */
  const createValueValidityCopies = () => {
    const validities = {};
    const needed = [];

    const rejectFunction = (target, field) => {
      if (!import.meta.env.PROD) {
        throw new Error('Cannot write properties on object directly');
      }
    };

    const handlers = {
      get: (target, field) => {
        if (needed.indexOf(field) === -1) {
          needed.push(field);
        }
        return target[field];
      },
      set: rejectFunction,
      defineProperty: rejectFunction,
      deleteProperty: rejectFunction
    };

    const currentValues = new Proxy(values, handlers);

    Object.entries(fields).forEach(([field, inputs]) => {
      validities[field] = inputs.input?.validity;
    });

    const validity = new Proxy(validities, handlers);

    return {
      currentValues,
      validity,
      needed
    };
  };

  /**
   * Set the text of the allow configured buttons to remove the custom
   * value of a field
   *
   * @param name Name of field to set the label for
   */
  const setAllowConfiguredButton = (name: string) => {
    const element = form.elements[name];

    if (!element) {
      throw new Error(`Could not find element ${name}`);
    }

    if (element.allowConfigured) {
      if (values[name] !== null) {
        fields[
          name
        ].configured.innerText = `Use ${element.allowConfigured} value`;
        fields[name].configured.disabled = false;
      } else {
        fields[
          name
        ].configured.innerText = `Using ${element.allowConfigured} value`;
        fields[name].configured.disabled = true;
      }
    }
  };

  /**
   * Set an attribute on a field. If the attribute is hidden, if the field
   * is a button, the hidden attribute will be placed on a button, otherwise
   * a display style will placed on the field label
   *
   * @param field Field to set the attribute on
   * @param attribute Attribute to set
   * @param value Value to set the attribute to
   */
  const setAttribute = (field: string, attribute: string, value: any) => {
    if (!fields[field]) {
      return;
    }
    if (attribute === 'hidden') {
      const el = fields[field].label || fields[field].input;

      if (el) {
        el.style.display = value ? 'none' : '';
      }

      return;
    }

    if (fields[field].input) {
      if (value === false || value === null || typeof value === 'undefined') {
        fields[field].input.removeAttribute(attribute);
      } else {
        fields[field].input.setAttribute(
          attribute,
          value === true ? '' : value
        );
      }
    }
  };

  /**
   * Run and attribute setting functions on all fields, or all fields that
   * need the given field if given
   *
   * @param field Run only the attribute setters that require this field
   */
  const checkAttributeSetters = (field?: string) => {
    // Create copy of values and validity
    const { currentValues, validity, needed } = createValueValidityCopies();

    Object.entries(form.elements).forEach(([name, element]) => {
      if (element === null) {
        return;
      }

      if (!lastNeeded[name]) {
        lastNeeded[name] = {};
      }
      if (element.attributes && fields[name] && fields[name].input) {
        Object.entries(element.attributes).forEach(([attribute, value]) => {
          if (typeof value === 'function') {
            if (
              !field ||
              !lastNeeded[name][attribute] ||
              lastNeeded[name][attribute].indexOf(field) !== -1
            ) {
              setAttribute(name, attribute, value(currentValues, validity));
              lastNeeded[name][attribute] = needed.splice(0);
            }
          }
        });
      }
    });

    if (form.disabled && submit) {
      if (
        !field ||
        !submitLastNeeded ||
        submitLastNeeded.indexOf(field) !== -1
      ) {
        submit.disabled = form.disabled(currentValues, validity);
        submitLastNeeded = needed.splice(0);
      }
    }
  };

  const updateOriginalValues = (newValues: FormData) => {
    originalValues = newValues;

    Object.entries(form.elements).forEach(([name, element]) => {
      if (element === null) {
        return;
      }

      if (fields[name]) {
        switch (element.type) {
          case 'boolean': {
            const checked =
              typeof originalValues[name] !== 'undefined' &&
              originalValues[name] !== null
                ? originalValues[name]
                : element.default;

            if (checked) {
              fields[name].input.setAttribute('checked', '');
            } else {
              fields[name].input.removeAttribute('checked');
            }
            break;
          }
          case 'file':
            break;
          case 'number':
          case 'integer':
            if (element.range && fields[name].range) {
              if (
                originalValues[name] === null ||
                typeof originalValues[name] === 'undefined'
              ) {
                if (fields[name].range.hasAttribute('value')) {
                  fields[name].range.removeAttribute('value');
                }
              } else {
                fields[name].range.setAttribute('value', originalValues[name]);
              }
            }
            break;
          default:
            if (
              originalValues[name] === null ||
              typeof originalValues[name] === 'undefined'
            ) {
              if (fields[name].input.hasAttribute('value')) {
                fields[name].input.removeAttribute('value');
              }
            } else {
              fields[name].input.setAttribute('value', originalValues[name]);
            }
            break;
        }
      }
    });
  };

  /**
   * Reset the form values to either the given values, or the default field
   * values
   */
  const resetValues = (newValues?: FormData) => {
    if (!originalValues) {
      values = {};
    } else {
      values = { ...originalValues };
    }

    Object.entries(form.elements).forEach(([name, element]) => {
      if (element === null) {
        return;
      }

      if (element.type === 'file' && fields[name]) {
        fields[name].label.innerText = 'Choose a file';
      }

      if (!Object.prototype.hasOwnProperty.call(values, name)) {
        if (Object.prototype.hasOwnProperty.call(element, 'default')) {
          values[name] = element.default;
        } else {
          values[name] = null;
        }
      }
    });
  };

  const formObject = {
    context: context || null,
    reset: (newValues?: FormData) => {
      if (newValues) {
        updateOriginalValues(newValues);
      }
      formElement.reset();
      resetValues();
    },
    get value() {
      if (!formElement) {
        throw new Error('Form removed');
      }
      if (!form.schema) {
        return { ...values };
      }
    },
    get valid() {
      Object.entries(form.elements).forEach(([name, element]) => {
        if (fields[name] && fields[name].input) {
          // Run custom validation functions
          if (element.validation) {
            fields[name].input.setCustomValidity(
              element.validation(values[name], values) || ''
            );
          }
        }
      });
      return formElement.checkValidity();
    },
    showErrors: () => {
      const errors = [];
      Object.entries(form.elements).forEach(([name, element]) => {
        if (fields[name] && fields[name].input) {
          if (!fields[name].input.validity.valid) {
            errors.push(
              `${element.title}: ${fields[name].input.validationMessage}`
            );
          }
        }
      });

      if (errors.length) {
        formObject.setMessage(
          'Please correct the following errors:\n- ' + errors.join('\n- '),
          'error'
        );
      } else {
        formObject.clearMessage();
      }
    },
    /**
     * Sets a form value. If handlers is true, will also run the handler
     * functions
     *
     * @param name Name of form value to change the value of
     * @param value New value for the form value
     * @param Whether or not to call the input handlers for the form and field
     */
    setValue: (name: string, value: any, handlers?: boolean) => {
      if (!formElement) {
        throw new Error('Form removed');
      }

      values[name] = value;

      // Find field in form
      const element = form.elements[name];

      if (element && element.type !== 'custom') {
        // Set value
        if (element.type === 'boolean') {
          fields[name].input.checked = Boolean(value);
          values[name] = Boolean(value);
        } else {
          fields[name].input.value = value;
          values[name] = value;
        }
      }

      checkAttributeSetters(name);

      if (handlers) {
        if (form.input) {
          form.input(
            name,
            values[name],
            fields[name].input ? fields[name].input.validity : true
          );
        }

        // Run value listeners
        if (listeners[name]) {
          listeners[name].forEach((handler) => {
            handler(values[name]);
          });
        }

        if (listeners.__) {
          listeners[name].forEach((handler) => {
            handler(name, values[name]);
          });
        }
      }
    },
    remove: () => {
      if (!formElement) {
        throw new Error('Form removed');
      }
      formElement.remove();
      formElement = null;
      fields = null;
      submit = null;
    },
    setMessage: (message: string, type?: string) => {
      if (!formElement) {
        throw new Error('Form removed');
      }
      errorDiv.innerText = message;
      errorDiv.className = `message`;
      if (type) {
        errorDiv.className += ' ' + type;
      }
    },
    clearMessage: () => {
      if (!formElement) {
        throw new Error('Form removed');
      }
      if (errorDiv.innerText) {
        errorDiv.innerText = '';
      }
    },
    addValueWatch: (key: string | null, handler) => {
      key = key || '__';

      if (!listeners[key]) {
        listeners[key] = [handler];
      } else if (listeners[key].indexOf(handler) === -1) {
        listeners[key].push(handler);
      }
    },
    removeValueWatch: (key: string | null, handler) => {
      key = key || '__';

      const index = listeners[key].indexOf(handler);

      if (index !== -1) {
        listeners[key].splice(index, 1);
      }
    }
  };

  formElement.addEventListener('submit', (event: Event) => {
    event.preventDefault();

    if (form.handler) {
      const { currentValues, validity } = createValueValidityCopies();
      form.handler(currentValues, validity, formObject);
    }
  });

  const handleInput = (event: Event, clear: boolean) => {
    formObject.clearMessage();
    let name;
    if (event.target.type === 'range' && event.target.name.endsWith('-range')) {
      name = event.target.name.slice(0, -6);
    } else {
      name = event.target.name;
    }

    if (!fields[name]) {
      return;
    }

    // Find element
    const element = form.elements[name];

    if (!element) {
      return;
    }

    let value;

    if (element.type === 'string' && element.format === 'color' && clear) {
      value = null;
    } else if (element.type === 'boolean') {
      value = fields[name].input.checked;
    } else if (element.type === 'file') {
      value = fields[name].input.files;
    } else if (
      event.target.type === 'range' &&
      event.target.name.endsWith('-range')
    ) {
      value = fields[name].range.valueAsNumber;
      fields[name].input.value = value;
    } else if (element.type === 'number') {
      value = Number(fields[name].input.value);
    } else {
      value = fields[name].input.value;
    }

    if (element.validation) {
      const result = element.validation(value, values);

      if (result) {
        fields[name].input.setCustomValidity(result);
        return;
      } else {
        fields[name].input.setCustomValidity('');
      }
    }

    values[name] = value;

    if (element.type === 'string' && element.format === 'color' && clear) {
      fields[name].input.value = null;
    } else if (element.type === 'boolean' || element.type === 'file') {
      void 0;
    } else if (
      event.target.type === 'range' &&
      event.target.name.endsWith('-range')
    ) {
      fields[name].input.setCustomValidity('');
    } else {
      if (element.type === 'number' || element.type === 'integer') {
        let valid = true;
        if (fields[name].input.validity.patternMismatch) {
          fields[name].input.setCustomValidity('Please enter a number');
          valid = false;
        }
        if (valid && element.attributes) {
          const value = fields[name].input.value;
          // Check validity of input as not checked natively
          if (element.attributes.step) {
            if (value % element.attributes.step !== 0) {
              valid = false;
              fields[name].input.setCustomValidity(
                'Number must be a multiple of ' + element.attributes.step
              );
            }
          }
          const min =
            typeof element.attributes.min === 'number'
              ? element.attributes.min
              : null;
          const max =
            typeof element.attributes.max === 'number'
              ? element.attributes.max
              : null;
          if ((min !== null && value < min) || (max !== null && value > max)) {
            valid = false;
            let message = 'Number must be ';
            if (min !== null && max !== null) {
              message += `between ${min} and ${max}`;
            } else if (min !== null) {
              message += `greater than ${min}`;
            } else {
              message += `less than ${min}`;
            }
            fields[name].input.setCustomValidity(message);
          } else {
            fields[name].input.setCustomValidity('');
          }
        }
        if (element.range) {
          fields[name].range.value = values[name];
        }
      }
    }

    if (element.allowConfigured) {
      setAllowConfiguredButton(name);
    }

    if (form.input) {
      form.input(
        name,
        values[name],
        fields[name].input ? fields[name].input.validity : true
      );
    }

    // Run value listeners
    if (listeners[name]) {
      listeners[name].forEach((handler) => {
        handler(values[name]);
      });
    }

    if (listeners.__) {
      listeners[name].forEach((handler) => {
        handler(name, values[name]);
      });
    }

    // Run attribute mappers
    checkAttributeSetters(name);
  };

  formElement.addEventListener('input', handleInput);

  {
    let el: HTMLElement, input: HTMLElement;

    if (form.title) {
      el = document.createElement('h1');
      el.innerHTML = form.title;
      formElement.appendChild(el);
    }

    if (form.description) {
      const parts = form.description.split('\n\n');
      parts.forEach((part) => {
        el = document.createElement('p');
        el.innerHTML = part;
        formElement.appendChild(el);
      });
    }

    // Create value
    resetValues();

    if (form.instructions) {
      const parts = form.instructions.split(/\n+/g);

      parts.forEach((part) => {
        el = document.createElement('p');
        el.innerHTML = part;
        formElement.appendChild(el);
      });
    }

    Object.entries(form.elements).forEach(([name, element]) => {
      if (element === null) {
        return;
      }

      if (element.type === 'custom') {
        el = document.createElement('div');

        element.create(el, formObject);

        formElement.appendChild(el);

        fields[name] = {
          div: el
        };

        return;
      }

      if (element.type === 'button') {
        input = document.createElement('button');
        input.innerText = element.title;

        fields[name] = { input };

        if (element.attributes) {
          for (const attribute in element.attributes) {
            if (typeof value !== 'function') {
              setAttribute(name, attribute, element.attributes[attribute]);
            }
          }
        }

        if (element.handler) {
          input.addEventListener('click', element.handler);
        }

        formElement.appendChild(input);
        return;
      }

      el = document.createElement('label');

      if (element.tooltip) {
        el.title = element.tooltip;
      }

      if (element.type === 'options') {
        input = document.createElement('select');

        fields[name] = {
          input,
          label: el
        };
        input.name = name;

        if (element.title) {
          el.appendChild(document.createTextNode(element.title));
        }
        el.appendChild(input);

        if (element.options) {
          for (let i = 0; i < element.options.length; i++) {
            const option = element.options[i];

            if (typeof option === 'object') {
              const elo = document.createElement('option');
              elo.value = option.value;
              elo.innerText = option.label;

              if (option.value === values[name]) {
                elo.selected = true;
              }
              input.appendChild(elo);
            } else {
              const elo = document.createElement('option');
              elo.value = option;
              elo.innerText = option;

              if (option === values[name]) {
                elo.selected = true;
              }
              input.appendChild(elo);
            }
          }
        }

        if (element.attributes) {
          for (const attribute in element.attributes) {
            if (typeof value !== 'function') {
              setAttribute(name, attribute, element.attributes[attribute]);
            }
          }
        }

        if (element.handler) {
          input.addEventListener('click', element.handler);
        }

        if (element.description) {
          const description = document.createElement('div');
          description.innerText = element.description;
          description.className = 'description';
          el.appendChild(description);
        }

        formElement.appendChild(el);
        return;
      }

      input = document.createElement('input');
      fields[name] = {
        input,
        label: el
      };
      input.name = name;

      if (element.type === 'boolean') {
        input.type = 'checkbox';

        el.appendChild(input);
        if (element.title) {
          el.appendChild(document.createTextNode(element.title));
        }

        if (values[name]) {
          input.checked = true;
          input.setAttribute('checked', '');
        }
      } else if (element.type === 'file') {
        el.appendChild(input);
        if (element.title) {
          el.appendChild(document.createTextNode(element.title));
        }

        input.type = 'file';
        el.className = 'file';
        const fileLabel = document.createElement('span');
        fields[name].label = fileLabel;
        const fileInput = input;
        el.appendChild(fileLabel);
        fileLabel.innerText = 'Choose a file';
        input.addEventListener('change', (event) => {
          if (fileInput.files.length) {
            fileLabel.innerText = fileInput.files[0].name;
            if (fileInput.files.length > 2) {
              fileLabel.innerText +=
                ' and ' + (fileInput.files.length - 1) + ' others';
            } else if (fileInput.files.length === 2) {
              fileLabel.innerText += ' and 1 other';
            }
          } else {
            fileLabel.innerText = 'Choose a file';
          }
        });
        el.appendChild(input);
      } else {
        switch (element.type) {
          case 'number':
            input.type = 'tel';
            if (input.step && input.step >= 1) {
              input.pattern = '[0-9]*';
            } else {
              input.pattern = '[0-9]*(.[0-9]+)?';
            }
            break;
          case 'string':
            if (formatFieldTypes.indexOf(element.format) !== -1) {
              input.type = element.format;
            }
            // Add regex to require a domain
            if (element.format === 'email') {
              input.pattern =
                "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$";
            }
            break;
        }

        if (element.attributes) {
          Object.entries(element.attributes).forEach(([attribute, value]) => {
            if (typeof value !== 'function') {
              setAttribute(name, attribute, element.attributes[attribute]);
            }
          });
        }

        if (element.title) {
          el.appendChild(document.createTextNode(element.title));
        }

        if (
          (element.type === 'number' || element.type === 'integer') &&
          element.range
        ) {
          const div = document.createElement('div');
          div.className = 'flex gap';
          const range = document.createElement('input');
          range.className = 'grow';
          range.type = 'range';
          range.name = `${name}-range`;
          if (element.attributes) {
            if (element.attributes.step) {
              range.step = element.attributes.step;
            }
            if (element.attributes.min) {
              range.min = element.attributes.min;
            }
            if (element.attributes.max) {
              range.max = element.attributes.max;
            }
          }
          input.style =
            'width: ' +
            (element.attributes?.max?.toString().length || 3) +
            'em';
          fields[name].range = range;

          if (typeof values[name] !== 'undefined' && values[name] !== null) {
            range.value = values[name];
            range.setAttribute('value', values[name]);
          }
          div.appendChild(range);
          div.appendChild(input);
          el.appendChild(div);
        } else {
          el.appendChild(input);
        }

        if (typeof values[name] !== 'undefined' && values[name] !== null) {
          input.value = values[name];
          input.setAttribute('value', values[name]);
        }

        if (element.allowConfigured) {
          input = document.createElement('button');
          input.className = 'allowConfigured';
          input.name = name;
          input.addEventListener('click', (event) => {
            handleInput(event, true);
          });
          fields[name].configured = input;
          setAllowConfiguredButton(name);
          el.appendChild(input);
        }
      }

      if (element.attributes) {
        for (const attribute in element.attributes) {
          if (typeof element.attributes[attribute] !== 'function') {
            setAttribute(name, attribute, element.attributes[attribute]);
          }
        }
      }

      if (element.description) {
        const description = document.createElement('div');
        description.className = 'description';
        description.innerHTML = element.description;
        el.appendChild(description);
      }

      formElement.appendChild(el);
    });

    el = document.createElement('div');
    el.className = 'buttons';
    formElement.appendChild(el);

    // Error message div
    errorDiv = document.createElement('div');
    errorDiv.className = 'message';
    formElement.appendChild(errorDiv);

    if (form.buttons) {
      form.buttons.forEach((button) => {
        if (button.type) {
          input = document.createElement('input');
          input.type = button.type;
          input.value = button.label;
        } else {
          input = document.createElement('button');
          input.innerText = button.label;
        }
        if (button.handler) {
          input.addEventListener('click', (event: Event) => {
            if (button.type) {
              event.preventDefault();
            }

            const { currentValues, validity } = createValueValidityCopies();
            button.handler(currentValues, validity, formObject);
          });
        }
        el.appendChild(input);
      });
    } else if (form.handler) {
      input = document.createElement('input');
      input.type = 'submit';
      input.value = form.submitLabel || 'Submit';
      submit = input;
      el.appendChild(input);
    }
  }

  checkAttributeSetters();

  container.appendChild(formElement);

  return formObject;
};
