import { mergeDraftUpdate } from '../../lib/draft';
import * as Messages from '../../typings/Message';
import { createDrawing } from './drawing/paper';
import { createFileManager } from './files';
import { createTools } from './tools';
import { App } from './typings/App';
import { createUser } from './user';

import makeId from '../../lib/id';

interface Options {
  id: string;
  noSetName?: boolean;
  toolbar?: Toolbar;
  status?: Status;
}

const createNewDraft = (): Draft => ({
  config: {},
  drawings: [
    {
      layers: [
        {
          name: 'images',
          children: []
        },
        {
          name: 'drawing',
          children: []
        },
        {
          name: 'annotation',
          children: []
        }
      ]
    }
  ],
  version: 1
});

/**
 * TODO Change canvas to be containing div. Then each drawing will create its
 * own canvas (or whatever it needs
 */
export const startDraft = (
  canvas: Element,
  config: Config,
  socket: SocketIO,
  options: Options
) => {
  let initial: boolean = true;
  let drawing;
  let tools;
  let draft;
  let requestId;

  const app: App = {
    draftId: options.id,
    status: options.status,
    draft: null,
    config,
    socket,
    user: null
  };

  const draftInstance = {
    get draft() {
      return draft && JSON.parse(JSON.stringify(draft));
    },
    get drawing() {
      return drawing;
    },
    get tools() {
      return tools;
    }
  };
  Object.freeze(draftInstance);

  app.user = createUser(app);
  app.files = createFileManager(app);

  app.user.addEventListener('update', () => {
    if (tools && app.draft) {
      tools.updateTools(app.draft, true);
    }
  });

  app.socket.on('connect', () => {
    requestId = makeId();
    const request = {
      id: requestId,
      name: options.id,
      getDraft: initial
    };
    app.socket.emit('draft:connect', request);
    app.user.reauthenticate(null);
  });

  app.socket.on('draft:error', (data) => {
    console.error('error received', data);
  });

  /**
   * Setup stuff, including the title, for the current draft
   */
  const setupFromDraft = () => {
    if (app.draft.config && app.draft.config.name) {
      document.title = `${app.draft.config.name} - first-draft`;
    }

    // TODO Rebuild tools config

    tools.updateTools(app.draft, true);
  };

  /**
   * Merge an update into an existing draft
   *
   * @param update Update to merge into the current draft
   */
  const mergeUpdate = (update) => {
    app.draft = mergeDraftUpdate(app.draft, update);

    setupFromDraft();
  };

  const updateView = () => {
    if (!drawing) {
      return;
    }

    drawing.updateViewFromUrl();
  };

  app.socket.on('draft:current', (data: Messages.DraftMessage) => {
    if (drawing) {
      window.removeEventListener('popstate', updateView);
    }

    if (Object.prototype.hasOwnProperty.call(data, 'draft')) {
      app.draft = data.draft;

      // TODO Remove old context
      //if (!initial) {
      //}
      initial = false;
      let promise;

      if (app.draft) {
        draft = app.draft;
        promise = createDrawing(canvas, app, draft, draft.drawings[0]);
        if (draft.config && draft.config.editPassword) {
          app.user.reauthenticate(app.draftId);
        }
      } else {
        draft = createNewDraft();
        if (!options.noSetName) {
          draft.config.name = options.id;
        }
        app.draft = draft;
        promise = createDrawing(canvas, app, draft, draft.drawings[0]);
        app.socket.emit('draft:create', {
          name: options.id,
          draft
        });
      }

      if (app.draft.config && app.draft.config.name) {
        document.title = `${app.draft.config.name} - first-draft`;
      }

      promise.then((drawingInstance) => {
        drawing = drawingInstance;

        window.addEventListener('popstate', updateView);

        drawing.addEventListener('draw', (event) => {
          app.socket.emit('draft:draw', event);
        });

        tools = createTools(app, options, [drawing]);

        tools.addEventListener('draftUpdate', (event) => {
          app.socket.emit(app.config.baseEvent + 'update', {
            id: makeId(),
            draftId: app.draftId,
            data: event
          });

          if (event.config && event.config.editPassword) {
            event = {
              ...event,
              config: {
                ...event.config,
                editPassword: true
              }
            };
          }

          mergeUpdate(event);
        });
      });
    }
  });

  app.socket.on(app.config.baseEvent + 'update', (data) => {
    if (app.draft && data.draftId === app.draftId) {
      mergeUpdate(data.data);
    }
  });

  //TODO app.socket.on('draft:view', (data) => {});

  return draftInstance;
};
