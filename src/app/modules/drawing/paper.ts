import { ThumbnailConfig } from '../../../typings/Config';
import { ViewChangeMessage, CursorMessage } from '../../../typings/Message';
import { App } from '../../../typings/App';
import * as Draft from '../../../typings/Draft';
import * as DraftEvents from '../../../typings/Event';

import Paper from 'paper/dist/paper-core.js';
import ResizeObserver from 'resize-observer-polyfill';

import makeId from '../../../lib/id';

export interface DrawingManager {
  addEventListener: (type: string, handler: Event.Handler) => void;
  removeEventListener: (type: string, handler: Event.Handler) => void;
}

interface NamedFile {
  name: string;
  file: File;
}

interface View {
  top?: number | null;
  bottom?: number | null;
  left?: number | null;
  right?: number | null;
  scale?: number | null;
}

const internalEvents = ['draw'];
const hitTestEvents = ['mousedown', 'click', 'touchstart'];

export const createDrawing = (
  canvas: Element,
  app: App,
  draft: Draft.Draft,
  drawing?: Draft.Drawing
) => {
  const paper = new Paper.PaperScope();
  let urlAdded = app.config.alwaysReplaceUrl || false;
  paper.setup(canvas);
  let imagesLayer;
  let drawingLayer;
  let annotationLayer;
  // Offset of the 0,0 point of drawing from the view 0,0 point
  // +ve means the 0,0 point of the drawing is off the top-left of the view
  let offset = new paper.Point([0, 0]);
  const eventHandlers = {
    draw: []
  };

  let lastPointerPosition = null;
  let lastPointerCoordinates = null;
  let lastPointerMovement = null;

  let viewUpdateMessageDebounce = null;
  let cursorMessageDebounce = null;

  const canvasCover = document.getElementById('canvasCover');

  const rawEventHandlers = [];
  let editingTextbox;

  /// When true, the url will be updated with the current view
  let setUrl = false;

  const imageLoadingStatus = app.status
    ? app.status.createStatus('Loading images...', {
        hideAfterPromises: true,
        updateStatus: (promises) => {
          if (promises.length > 2) {
            return `Loading ${promises.length} images...`;
          } else if (promises.length === 2) {
            return `Loading images "${decodeURIComponent(
              promises[0].data.filename
            )}" and "${decodeURIComponent(promises[1].data.filename)}"...`;
          } else if (promises.length === 1) {
            return `Loading image "${decodeURIComponent(
              promises[0].data.filename
            )}"...`;
          } else {
            return 'Loading images...';
          }
        }
      })
    : null;

  const imageUploadingStatus = app.status
    ? app.status.createStatus('Uploading images', {
        hideAfterPromises: true,
        updateStatus: (promises) => {
          // Calculate progress
          let progress = 0;
          let inProgress = 0;
          promises.forEach((promise) => {
            if (promise.data.progress !== null) {
              progress += promise.data.progress;
              inProgress++;
            }
          });

          let text;
          if (promises.length > 2) {
            text = `Uploading ${promises.length} images`;
          } else if (promises.length === 2) {
            text = `Uploading images "${decodeURIComponent(
              promises[0].data.filename
            )}" and "${decodeURIComponent(promises[1].data.filename)}"`;
          } else if (promises.length === 1) {
            text = `Uploading image "${decodeURIComponent(
              promises[0].data.filename
            )}"`;
          } else {
            text = 'Uploading images';
          }

          if (inProgress) {
            text +=
              ' (' + Math.round((progress / inProgress) * 100) + '% complete)';
          }

          return text + '...';
        }
      })
    : null;

  let updateUploadingStatusTimeout = null;
  const debounceUpdateUploadingStatus = () => {
    if (updateUploadingStatusTimeout === null) {
      updateUploadingStatusTimeout = setTimeout(() => {
        updateUploadingStatusTimeout = null;
        if (imageUploadingStatus && imageUploadingStatus.isShown()) {
          imageUploadingStatus.update();
        }
      }, 1000);
    }
  };

  paper.settings.hitTolerance = 5;

  canvas.addEventListener('mousemove', (event: Event) => {
    const rect = canvas.getBoundingClientRect();
    const newPointerPosition = new paper.Point(
      event.clientX - rect.left,
      event.clientY - rect.top
    );
    if (lastPointerPosition) {
      lastPointerMovement = new paper.Point(
        newPointerPosition.x - lastPointerPosition.x,
        newPointerPosition.y - lastPointerPosition.y
      );
    }
    lastPointerPosition = newPointerPosition;
    lastPointerCoordinates = toDrawingCoordinates(event.clientX, event.clientY);
    sendCursorPosition();
  });
  const touchPositionHandler = (event: Event) => {
    if (event.touches.length) {
      const rect = canvas.getBoundingClientRect();
      const newPointerPosition = new paper.Point(
        event.touches[0].clientX - rect.left,
        event.touches[0].clientY - rect.top
      );
      if (lastPointerPosition) {
        lastPointerMovement = new paper.Point(
          newPointerPosition.x - lastPointerPosition.x,
          newPointerPosition.y - lastPointerPosition.y
        );
      }
      lastPointerPosition = newPointerPosition;
      lastPointerCoordinates = toDrawingCoordinates(
        event.touches[0].clientX,
        event.touches[0].clientY
      );

      sendCursorPosition();
    }
  };
  canvas.addEventListener('touchmove', touchPositionHandler);
  canvas.addEventListener('touchstart', touchPositionHandler);

  const sendCursorPosition = (second?: boolean) => {
    if (app.config.share && app.config.share.cursor) {
      if (cursorMessageDebounce === null) {
        cursorMessageDebounce = setTimeout(() => {
          cursorMessageDebounce = null;

          if (!second) {
            sendCursorPosition(true);
          }
        }, 1000);

        app.socket.emit(app.config.baseEvent + 'cursor', {
          draftId: app.draftId,
          drawingId: '0',
          x: lastPointerCoordinates.x,
          y: lastPointerCoordinates.y,
          time: new Date().getTime()
        } as CursorMessage);
      }
    }
  };

  /**
   * Update the center of the drawing based on the offset
   */
  const updateCenter = () => {
    const centerOffset = new paper.Point(
      paper.view.viewSize.divide(2).divide(paper.view.zoom)
    );
    paper.view.center = centerOffset.add(offset);

    updateEditTextbox();
    // Update images
    debounceUpdateImages();

    updateCanvasPosition();
  };

  /**
   * Update the offset based on the center of the drawing
   */
  const resetOffset = () => {
    offset = new paper.Point(
      paper.view.viewSize
        .divide(-2)
        .divide(paper.view.zoom)
        .add(paper.view.center)
    );
    updateCanvasPosition();
  };

  /**
   * Update the zoom on the drawing to the given zoom, keeping the coordinate
   * at the same pixel position if given.
   *
   * @param amount New zoom level
   * @param point Coordinate to keep in same place when zooming
   * @param Do not round to a minimum zoom value
   */
  const updateZoom = (
    amount: number,
    point?: Point | null,
    noMinLimit?: boolean
  ) => {
    const currentZoom = paper.view.zoom;
    const newZoom = Math.min(
      10,
      Math.max(noMinLimit ? 0 : app.config.minZoomLevel, amount)
    );

    if (currentZoom !== newZoom) {
      if (point) {
        // Calculate the real pixel distance between the point and the center
        const delta = point.subtract(paper.view.center);
        // at the current zoom
        const currentRealDelta = delta.multiply(currentZoom);
        // at the new zoom
        const newRealDelta = delta.multiply(newZoom);
        // Subtract and convert back into canvas pixels
        const difference = newRealDelta
          .subtract(currentRealDelta)
          .divide(newZoom);
        // Change zoom
        paper.view.zoom = newZoom;
        // Adjust canvas center based on difference
        paper.view.center = paper.view.center.add(difference);
      } else {
        paper.view.zoom = newZoom;
      }

      // Update offset for new zoom
      resetOffset();

      updateEditTextbox();

      // Update images
      debounceUpdateImages();
    }
  };

  /**
   * Update the canvas position div
   */
  const updateCanvasPosition = () => {
    const postionEl = document.getElementById('canvasPosition');

    if (postionEl) {
      let position =
        Math.round(offset.x || 0) + ', ' + Math.round(offset.y || 0) + ' (';

      if (paper.view.zoom >= 0.1) {
        position += paper.view.zoom.toFixed(1);
      } else if (paper.view.zoom >= 0.01) {
        position += paper.view.zoom.toFixed(2);
      } else {
        position += paper.view.zoom.toFixed(3);
      }

      position += 'x)';

      postionEl.innerText = position;
    }

    // Update the url with the new position
    if (app.config.viewUrl && setUrl) {
      debounceUpdateUrlView();
    }

    if (app.config.share && app.config.share.view) {
      // Send a view update message
      if (viewUpdateMessageDebounce) {
        clearTimeout(viewUpdateMessageDebounce);
      }

      viewUpdateMessageDebounce = setTimeout(() => {
        const rect = canvas.getBoundingClientRect();

        app.socket.emit(app.config.baseEvent + 'view', {
          draftId: app.draftId,
          drawingId: '0',
          x: offset.x,
          y: offset.y,
          width: rect.width,
          height: rect.height,
          zoom: Math.round(10 * paper.view.zoom) / 10,
          time: new Date().getTime()
        } as ViewChangeMessage);
        viewUpdateMessageDebounce = null;
      }, 1000);
    }
  };

  /**
   * Update the URL with the current view
   */
  const updateUrlView = () => {
    const url = new URL(window.location);
    const box = canvas.getBoundingClientRect();
    url.searchParams.set('l', Math.floor(offset.x));
    url.searchParams.set('t', Math.floor(offset.y));
    url.searchParams.set(
      'r',
      Math.ceil(offset.x + box.width / paper.view.zoom)
    );
    url.searchParams.set(
      'b',
      Math.ceil(offset.y + box.height / paper.view.zoom)
    );
    const zoom = paper.view.zoom.toFixed(2).replace(/\.?0+$/, '');
    url.searchParams.set('z', zoom);
    if (urlAdded) {
      history.replaceState(null, '', url.toString());
    } else {
      history.pushState(null, '', url.toString());
      urlAdded = true;
    }
    updateUrlDebounce = null;
  };

  let updateUrlDebounce = null;
  /**
   * Debounce the updating of the URL with the current view by updating
   * only after a set timeout
   */
  const debounceUpdateUrlView = () => {
    if (updateUrlDebounce !== null) {
      clearTimeout(updateUrlDebounce);
    }

    updateUrlDebounce = setTimeout(updateUrlView, 1000);
  };

  /**
   * Find the bounding coordinates of everything within the drawing
   */
  const getFullBounds = () => {
    let minX = 0,
      maxX = 0,
      minY = 0,
      maxY = 0;

    paper.project.layers.forEach((layer) => {
      const bounds = layer.strokeBounds;

      if (bounds.width !== 0 || bounds.height !== 0) {
        minX = Math.min(bounds.x, minX);
        maxX = Math.max(bounds.x + bounds.width, maxX);
        minY = Math.min(bounds.y, minY);
        maxY = Math.max(bounds.y + bounds.height, maxY);
      }
    });

    return new paper.Rectangle({
      x: minX,
      y: minY,
      width: maxX - minX,
      height: maxY - minY
    });
  };

  const itemWithinView = (item: Item, padding: number = 0) => {
    const bounds = item.bounds;
    const viewBounds = paper.view.bounds;

    // Check that one of the bounds is in the view, or the item bounds
    // contains the view
    return (
      ((bounds.left >= viewBounds.left && bounds.left <= viewBounds.right) ||
        (bounds.right >= viewBounds.left && bounds.right <= viewBounds.right) ||
        (bounds.left <= viewBounds.left && bounds.right >= viewBounds.right)) &&
      ((bounds.top >= viewBounds.top && bounds.top <= viewBounds.bottom) ||
        (bounds.bottom >= viewBounds.top &&
          bounds.bottom <= viewBounds.bottom) ||
        (bounds.top <= viewBounds.top && bounds.bottom >= viewBounds.bottom))
    );
  };

  /**
   * Pan the canvas so the given item is on the screen. If the item is larger
   * than the screen size for a dimension, position the item in the middle of
   * the screen for that dimension
   *
   * @param item Item to pan to
   * @param zoom Zoom to fit the item on screen if it is not already fully
   *   on screen
   * @param padding Padding to give to the item
   * @param extraPadding Add extra padding to the bottom and right if the item
   *   isn't already fully on screen
   */
  const panToItem = (
    item: Item,
    zoom: boolean = false,
    padding: number = 10,
    extraPadding: number = 0
  ) => {
    const bounds = item.bounds;
    let viewBounds = paper.view.bounds;

    // Check if currently on the screen
    if (
      bounds.top >= viewBounds.y + padding &&
      bounds.left >= viewBounds.x + padding &&
      bounds.bottomRight.x <= viewBounds.x + viewBounds.width - padding &&
      bounds.bottomRight.y <= viewBounds.y + viewBounds.height - padding
    ) {
      return;
    }

    if (
      zoom &&
      (bounds.width + 2 * padding > viewBounds.width ||
        bounds.height + 2 * padding > viewBounds.height)
    ) {
      const newZoom =
        paper.view.zoom /
        Math.min(
          bounds.width + 2 * padding + extraPadding > viewBounds.width
            ? (bounds.width + 2 * padding + extraPadding) / viewBounds.width
            : Infinity,
          bounds.height + 2 * padding + extraPadding > viewBounds.height
            ? bounds.height + 2 * padding + extraPadding / viewBounds.height
            : Infinity
        );
      updateZoom(newZoom, bounds.center.add(extraPadding / 2));
      viewBounds = paper.view.bounds;
      // Add extraPadding to the bounds so that it is shifted correctly
      bounds.width += extraPadding;
      bounds.height += extraPadding;
    }

    let offsetY = null;
    let offsetX = null;

    // Check if currently on the screen and too big for the screen
    // If the bounds is bigger than the view bounds, center the bounds
    // If the bounds is within the view bounds, keep same offset
    // If the bounds is off the top/left, set offset to top/left of bounds
    // If the bounds is off the bottom/rigt, set the offset to so bounds is just on screen (with extraPadding)
    if (
      bounds.left >= viewBounds.left + padding &&
      bounds.right <= viewBounds.right - padding
    ) {
      // bounds within current view: keep same offset
      offsetX = offset.x;
    } else if (bounds.width > viewBounds.width) {
      // bounds bigger: center
      offsetX = bounds.center.x - viewBounds.width / 2;
    } else if (bounds.right + padding > viewBounds.right) {
      // bounds off right
      offsetX = bounds.x - (viewBounds.width - bounds.width - padding);
    } else if (bounds.left - padding < viewBounds.left) {
      // bounds off left
      offsetX = bounds.left - padding;
    }

    // Check if currently on the screen and too big for the screen
    if (
      bounds.top >= viewBounds.top + padding &&
      bounds.bottom <= viewBounds.bottom - padding
    ) {
      // bounds within current view: keep same offset
      offsetY = offset.y;
    } else if (bounds.height > viewBounds.height) {
      // bounds bigger: center
      offsetY = bounds.center.y - viewBounds.height / 2;
    } else if (bounds.bottom + padding > viewBounds.bottom) {
      // bounds off bottom
      offsetY = bounds.y - (viewBounds.height - bounds.height - padding);
    } else if (bounds.top - padding < viewBounds.top) {
      // bounds off top
      offsetY = bounds.top - padding;
    }

    if (offsetX) {
      offsetX = Math.round(offsetX * 10) / 10;
    }
    if (offsetY) {
      offsetY = Math.round(offsetY * 10) / 10;
    }

    if (offsetX === offset.x && offsetY === offset.y) {
      return;
    }

    offset = new paper.Point(offsetX, offsetY);
    updateCenter();
  };

  /**
   * Zoom out to ensure the given item is visible in the current view
   *
   * @param item Item to ensure that is currently visible
   * @param padding Padding of view
   */
  const zoomToItem = (item, padding: number = 10) => {
    const itemBounds = item.bounds;
    const viewBounds = paper.view.bounds;

    // Calculate encompassing bounds
    const bounds = new paper.Rectangle(viewBounds);

    if (itemBounds.x < bounds.x) {
      bounds.width = bounds.width + bounds.x - itemBounds.x;
      bounds.x = itemBounds.x;
    }

    if (itemBounds.y < bounds.y) {
      bounds.height = bounds.height + bounds.y - itemBounds.y;
      bounds.y = itemBounds.y;
    }

    if (itemBounds.bottom > bounds.bottom) {
      bounds.height = bounds.height + itemBounds.bottom - bounds.bottom;
    }

    if (itemBounds.right > bounds.right) {
      bounds.width = bounds.width + itemBounds.right - bounds.right;
    }

    panToItem({ bounds }, true, padding);
  };

  /**
   * Change the zoom and offset to show everything in the drawing
   */
  const zoomToExtent = () => {
    const bounds = getFullBounds();
    const viewSize = paper.view.viewSize;

    if (bounds.width || bounds.height) {
      const scale = Math.min(
        1,
        viewSize.width / bounds.width,
        viewSize.height / bounds.height
      );
      updateZoom(scale, null, true);
      offset = new paper.Point(bounds.x, bounds.y);
      updateCenter();
    } else {
      // Reset zoom and offset if no width or height
      updateZoom(1, null, true);
      offset = new paper.Point(0, 0);
      updateCenter();
    }
  };

  /**
   * Updates the view from the URL
   *
   * @returns Whether or not the view was changed
   */
  const updateViewFromUrl = () => {
    let fixUrl = false;
    // Check Url for bounds
    const url = new URL(window.location);
    let top = url.searchParams.get('t');
    let right = url.searchParams.get('r');
    let bottom = url.searchParams.get('b');
    let left = url.searchParams.get('l');
    let scale = url.searchParams.get('z');
    top = top === null || isNaN(Number(top)) ? null : Number(top);
    right = right === null || isNaN(Number(right)) ? null : Number(right);
    bottom = bottom === null || isNaN(Number(bottom)) ? null : Number(bottom);
    left = left === null || isNaN(Number(left)) ? null : Number(left);
    scale = scale === null || isNaN(Number(scale)) ? null : Number(scale);
    // Ensure scale is not 0
    if (scale === 0) {
      scale = null;
      fixUrl = true;
    }

    const updated = updateView({ top, right, bottom, left, scale });
    if (fixUrl) {
      updateUrlView();
    }
    return updated;
  };

  /**
   * Updates the view from the given View
   *
   * @param view View containing the new bounds and/or scale
   *
   * @returns Whether or not the view was changed
   */
  const updateView = (view: View) => {
    const box = canvas.getBoundingClientRect();
    const { top, right, bottom, left } = view;
    let { scale } = view;
    if (top !== null || left !== null || bottom !== null || right !== null) {
      let x = 0,
        y = 0;
      // Calculate scale if not in url
      if (scale === null) {
        scale = 1;
        if (top !== null && bottom !== null) {
          scale = Math.min(scale, box.height / Math.abs(bottom - top));
        }
        if (left !== null && bottom !== null) {
          scale = Math.min(scale, box.width / Math.abs(right - left));
        }
      }
      // Calculate y offset
      if (top !== null && bottom !== null) {
        const viewHeight = Math.abs(bottom - top) * scale;
        y = Math.min(top, bottom) - (box.height - viewHeight) / 2 / scale;
      } else if (top !== null) {
        y = top;
      } else if (bottom !== null) {
        y = bottom - box.height / scale;
      }
      // Calculate x offset
      if (left !== null && right !== null) {
        const viewWidth = Math.abs(right - left) * scale;
        x = Math.min(left, right) - (box.width - viewWidth) / 2 / scale;
      } else if (left !== null) {
        x = left;
      } else if (right !== null) {
        x = left - box.width / scale;
      }
      updateZoom(scale);
      offset = new paper.Point(x, y);
      updateCenter();
      return true;
    }
    return false;
  };

  /**
   * Convert clientX and clientY values into drawing coordinates
   *
   * @param clientX clientX coordinate
   * @param clientY clientY coordinate
   *
   * @returns The coordinates in the drawing [x, y]
   */
  const toDrawingCoordinates = (clientX: number, clientY: number) => {
    const rect = canvas.getBoundingClientRect();

    return new paper.Point(
      (clientX - rect.left) / paper.view.zoom + offset.x,
      (clientY - rect.top) / paper.view.zoom + offset.y
    );
  };

  /**
   * Convert x and y drawing coordinates into clientX and clientY coordinates
   *
   * @param x Drawing x coordinate
   * @param y Drawing y coordinate
   * @param allowOutside Allow client coordinates to be outside of the
   *   container element (if the drawing coordinates are currently  off screen)
   * @param client If true, return position relative to the document instead
   *   of relative to the canvas container
   *
   * @returns The client coordinates of the coordinates in the drawing or
   *   null if the coordinates are not visible
   */
  const fromDrawingCoordinates = (
    x: number,
    y: number,
    allowOutside: boolean = false,
    client?: boolean
  ) => {
    const rect = canvas.getBoundingClientRect();

    const clientX = (x - offset.x) * paper.view.zoom;
    const clientY = (y - offset.y) * paper.view.zoom;

    if (
      !allowOutside &&
      (clientX < 0 ||
        clientX > rect.width ||
        clientY < 0 ||
        clientY > rect.height)
    ) {
      return null;
    }

    if (client) {
      return new paper.Point(clientX + rect.left, clientY + rect.top);
    } else {
      return new paper.Point(clientX, clientY);
    }
  };

  /**
   * Convert a standard event into a DrawingEvent
   *
   * @param event Standard event
   *
   * @returns DrawingEvent
   */
  const drawingEvent = (event: Event) => {
    if (!eventHandlers[event.type]) {
      return;
    }

    const drawingEvent: DraftEvents.DrawingEvent = {
      currentTarget: thisDrawing,
      type: event.type,
      originalEvent: event,
      get defaultPrevented() {
        return event.defaultPrevented;
      },
      preventDefault: () => event.preventDefault(),
      stopPropagation: () => event.stopPropagation()
    };

    if (typeof event.clientX === 'number') {
      drawingEvent.middleCoordinates = toDrawingCoordinates(
        event.clientX,
        event.clientY
      );
      const rect = canvas.getBoundingClientRect();
      drawingEvent.middlePixels = new paper.Point(
        event.clientX - rect.left,
        event.clientY - rect.top
      );
      if (hitTestEvents.indexOf(event.type) !== -1) {
        const hitResult = paper.project.hitTest(drawingEvent.middleCoordinates);
        if (hitResult && hitResult.item) {
          drawingEvent.item = hitResult.item;
        }
      }
    } else if (event.touches) {
      if (event.touches.length) {
        let x = 0;
        let y = 0;

        for (let i = 0; i < event.touches.length; i++) {
          x += event.touches[i].clientX;
          y += event.touches[i].clientY;
        }

        x = x / event.touches.length;
        y = y / event.touches.length;

        drawingEvent.middlePixels = new paper.Point(x, y);
        drawingEvent.middleCoordinates = toDrawingCoordinates(x, y);

        if (
          event.touches.length === 1 &&
          hitTestEvents.indexOf(event.type) !== -1
        ) {
          const hitResult = paper.project.hitTest(
            drawingEvent.middleCoordinates
          );
          if (hitResult && hitResult.item) {
            drawingEvent.item = hitResult.item;
          }
        }
      } else {
        drawingEvent.middleCoordinates = null;
      }
    }

    if (event.type === 'mousemove' || event.type === 'touchmove') {
      drawingEvent.movement = lastPointerMovement;
      drawingEvent.coordinateMovement =
        lastPointerMovement && lastPointerMovement.divide(paper.view.zoom);
    }
    drawingEvent.lastPointerPosition = lastPointerPosition;

    // Select textbox group if textbox hit
    if (
      drawingEvent.item &&
      drawingEvent.item.parent &&
      drawingEvent.item.parent.type === 'Textbox'
    ) {
      drawingEvent.item = drawingEvent.item.parent;
    }

    // Duplicate the handlers array so can iterate through the array without
    // it being changed while running through the handlers
    const handlers = [...eventHandlers[event.type]];
    handlers.forEach((handler, index) => {
      // Check the handler is still in the event handlers
      if (eventHandlers[event.type].indexOf(handler) === -1) {
        return;
      }

      handler(drawingEvent);
    });
  };

  /**
   * Convert an element into an object to be stored
   */
  const makeObject = (element) => {
    switch (element.type) {
      case 'Path': {
        const json = element.exportJSON({ asString: false })[1];
        const object = {
          type: 'Path',
          name: json.name,
          segments: json.segments,
          strokeColor: element.strokeColor.toCSS(true),
          strokeCap: !json.strokeCap ? 'none' : json.strokeCap,
          strokeWidth: json.strokeWidth
        };
        if (element.commit === false) {
          object.commit = false;
        }
        if (json.opacity) {
          object.opacity = json.opacity;
        }
        return object;
      }
      case 'Textbox': {
        const object = {
          type: 'Textbox',
          ...element.data
        };
        if (typeof object.editTextbox !== 'undefined') {
          delete object.editTextbox;
        }
        if (element.commit === false) {
          object.commit = false;
        }

        return object;
      }
    }
  };

  const emit = (type, event) => {
    event.type = type;
    // Used to identify the change
    event.id = makeId();

    if (eventHandlers[type]) {
      eventHandlers[type].forEach((handler) => handler(event));
    }
  };

  const debounceTimeouts = {};

  const debounceEmit = (type, event) => {
    if (!debounceTimeouts[type]) {
      debounceTimeouts[type] = {
        timeout: setTimeout(() => {
          emit(type, debounceTimeouts[type].event);
          delete debounceTimeouts[type];
        }, 100),
        event
      };
    } else {
      debounceTimeouts[type].event = event;
    }
  };

  const debounceElementsTimeouts = {};

  const debounceElementsEmit = (action, elements) => {
    if (!debounceElementsTimeouts[action]) {
      debounceElementsTimeouts[action] = {
        timeout: setTimeout(() => {
          emit('draw', {
            action: action,
            elements: debounceElementsTimeouts[action].elements
          });
          delete debounceElementsTimeouts[action];
        }, 100),
        elements
      };
    } else {
      for (let i = 0; i < elements.length; i++) {
        // Find element in current elements
        const index = debounceElementsTimeouts[action].elements.findIndex(
          (element) =>
            element.layerName === elements[i].layerName &&
            element.element.name === elements[i].element.name
        );

        if (index !== -1) {
          Object.assign(
            debounceElementsTimeouts[action].elements[index],
            elements[i]
          );
        } else {
          debounceElementsTimeouts[action].elements.push(elements[i]);
        }
      }
    }
  };

  const addImageLoader = (image) => {
    if (imageLoadingStatus) {
      const promise = new Promise((resolve, reject) => {
        image.on('load', resolve);
        image.on('error', () => {
          resolve();
          app.status.createStatus(`Error loading image ${image.data.source}`, {
            type: 'error',
            timeout: true
          });
        });
      });
      imageLoadingStatus.addPromise(promise, { filename: image.data.source });
      if (image.data.data) {
        imageLoadingStatus.onceAllResolved().then(() => {
          preloadImage(image.data.data);
        });
      }

      return promise;
    }

    return null;
  };

  /**
   * Ensure items only contain real draft elements (textbox instead of
   * textbox parts etc)
   *
   * @param items Items to clean
   */
  const cleanItems = (items) => {
    const cleaned = [];

    items.forEach((item) => {
      if (item.className === 'Layer') {
        return;
      }
      if (item.parent && item.parent.type === 'Textbox') {
        if (cleaned.indexOf(item.parent) === -1) {
          cleaned.push(item.parent);
        }
      } else if (cleaned.indexOf(item) === -1) {
        cleaned.push(item);
      }
    });

    return cleaned;
  };

  /**
   * Create a textbox
   *
   * @param options
   */
  const createTextbox = (options) => {
    options = {
      padding: 5,
      fontSize: 12,
      background: '#ffffffcc',
      color: '#ff0000',
      ...options
    };

    const point = new paper.Point(options.position);

    let background = null;
    if (!options.noBackground) {
      background = new paper.Path.Rectangle({
        topLeft: point.subtract(options.padding),
        bottomRight: point.add(options.padding)
      });
      background.fillColor = options.background;
    }

    const textPoint = new paper.PointText({
      point: point.add(new paper.Point(0, options.fontSize)),
      fontSize: options.fontSize,
      fillColor: options.color
    });
    textPoint.content = options.content || '';

    // Make the rectangle the right size for the text
    if (!options.noBackground) {
      const size = new paper.Point(
        textPoint.bounds.width,
        textPoint.bounds.height
      ).add(options.padding * 2);
      background.bounds.size = size;
    }

    // Create a paper.Group to store everything in
    let group;
    if (!options.noBackground) {
      group = new paper.Group([background, textPoint]);
    } else {
      group = new paper.Group([textPoint]);
    }
    group.type = 'Textbox';
    group.data = options;

    if (options.name) {
      group.name = options.name;
    }

    return group;
  };

  /**
   * Set the given textbox up to be edited
   *
   * @param textbox Textbox to edit
   * @param initial Whether it is the initial edit of the textbox
   */
  const editTextbox = (textbox: Group, initial?: boolean) => {
    if (textbox.type !== 'Textbox') {
      throw new Error('Trying to edit a non-textbox');
    }

    // Ensure only textbox is selected
    paper.project.deselectAll();
    textbox.selected = true;

    // TODO Change to given container parameter
    const canvasContainer = document.getElementById('canvasContainer');
    const textDiv = document.createElement('div');
    textDiv.contentEditable = true;
    textDiv.className = 'editTextbox';

    // Store as editingTextbox so can resize on zoom etc
    const editing = {
      textDiv,
      options: textbox.data,
      originalOptions: { ...textbox.data },
      elementName: textbox.name,
      layer: textbox.parent
    };
    editingTextbox = editing;

    // Set the content of the div from the content of the textbox, replacing
    // newlines with <br>s
    if (textbox.data.content) {
      textDiv.innerHTML = textbox.data.content.replace(/\n/g, '<br>');
    }

    // Hide the current textbox and show the text div
    textbox.visible = false;
    canvasContainer.appendChild(textDiv);

    textDiv.focus();

    // Move caret to the end of the text
    if (textbox.data.content) {
      const range = document.createRange();
      range.selectNodeContents(textDiv);
      range.collapse(false);
      const sel = getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    }

    updateEditTextbox();

    const inputHandler = (event) => {
      // TODO Emit an edit event
      editing.options.content = textDiv.innerText.replace(/\n$/, '');
      emit('draw', {
        action: initial ? 'create' : 'modify',
        layerName: editing.layer.name,
        element: makeObject({
          data: editing.options,
          type: 'Textbox',
          id: editing.elementName
        }),
        active: true
      });

      const padding = editing.options.noBackground
        ? 0
        : editing.options.padding;
      const divPoint = new paper.Point(editing.options.position).subtract(
        padding
      );
      const htmlBounds = textDiv.getBoundingClientRect();
      const bounds = new paper.Rectangle(
        divPoint.x,
        divPoint.y,
        Math.ceil(htmlBounds.width / paper.view.zoom),
        Math.ceil(htmlBounds.height / paper.view.zoom)
      );

      panToItem(
        {
          bounds
        },
        true,
        10,
        40
      );
    };

    // Listen for change events
    textDiv.addEventListener('input', inputHandler);

    // Return function to stop editing
    const editTextboxControl = {
      close: () => {
        textbox.remove();
        if (editingTextbox === editing) {
          delete editing.options.editTextbox;
          // TODO Need to check if should remove
          delete editing.options.commit;

          // Remove the editable textbox
          textDiv.removeEventListener('input', inputHandler);
          textDiv.remove();

          // Redraw the textbox
          const newTextbox = createTextbox(editing.options);
          newTextbox.name = editing.elementName;

          editing.layer.addChild(newTextbox);

          // Emit update event
          emit('draw', {
            action: initial ? 'create' : 'modify',
            layerName: editing.layer.name,
            element: {
              ...editing.options,
              type: 'Textbox'
            },
            active: false
          });
          editingTextbox = null;

          return newTextbox;
        }
      },
      cancel: () => {
        textbox.remove();
        textDiv.removeEventListener('input', inputHandler);
        textDiv.remove();
        delete editing.options.editTextbox;

        // Redraw the textbox
        const newTextbox = createTextbox(editing.originalOptions);
        newTextbox.name = editing.elementName;

        editing.layer.addChild(newTextbox);

        // Emit update event
        emit('draw', {
          action: initial ? 'create' : 'modify',
          layerName: editing.layer.name,
          element: {
            ...editing.options,
            type: 'Textbox'
          }
        });
        editingTextbox = null;

        return newTextbox;
      },
      get content() {
        return textDiv.innerText.replace(/\n$/, '');
      },
      get removed() {
        return textbox.parent === null;
      },
      get div() {
        return textDiv;
      }
    };

    textbox.data.editTextbox = editTextboxControl;

    return editTextboxControl;
  };

  /**
   * Update position and size of editing textbox
   */
  const updateEditTextbox = () => {
    if (editingTextbox) {
      const { options, textDiv } = editingTextbox;

      const padding = options.noBackground ? 0 : options.padding;
      // Convert the position to screen pixels
      const divPoint = new paper.Point(options.position).subtract(padding);
      const pixels = fromDrawingCoordinates(divPoint.x, divPoint.y, true);

      // Create style of text div based on textbox options
      let cssText =
        `left: ${Math.round(pixels.x)}px; ` +
        `top: ${Math.round(pixels.y)}px; ` +
        `font-size: ${Math.round(options.fontSize * paper.view.zoom)}px; ` +
        `padding: ${Math.round(padding * paper.view.zoom)}px; ` +
        `color: ${options.color};` +
        `min-height: ${(options.fontSize + 2 * padding) * paper.view.zoom}px;`;

      if (!options.noBackground) {
        cssText += ` background: ${options.background};`;
      }

      textDiv.style.cssText = cssText;

      const htmlBounds = textDiv.getBoundingClientRect();
      const bounds = new paper.Rectangle(
        divPoint.x,
        divPoint.y,
        Math.ceil(htmlBounds.width / paper.view.zoom),
        Math.ceil(htmlBounds.height / paper.view.zoom)
      );

      panToItem(
        {
          bounds
        },
        true,
        10,
        50
      );
    }
  };

  /**
   * Update a property on a textbox
   *
   * @param textbox Textbx group item to update the property on
   * @param property Property to update
   * @param value New value for the property
   */
  const updateTextProperty = (textbox, property, value) => {
    if (
      ['padding', 'background'].indexOf(property) !== -1 &&
      textbox.data.noBackground
    ) {
      return;
    }

    let background, textPoint;
    // Get children
    for (let i = 0; i < textbox.children.length; i++) {
      if (textbox.children[i] instanceof paper.Path) {
        background = textbox.children[i];
      } else if (textbox.children[i] instanceof paper.PointText) {
        textPoint = textbox.children[i];
      }
    }

    let redoBackground = false;
    let backgroundColor = textbox.data.background;

    switch (property) {
      case 'addBackground':
        if (value) {
          if (!background) {
            redoBackground = true;
          }
          if (textbox.data.noBackground) {
            delete textbox.data.noBackground;
          }
        } else {
          if (background) {
            background.remove();
            background = null;
            if (textbox.data.editTextbox) {
              textbox.data.editTextbox.div.style.background = 'none';
            }
          }
          textbox.data.noBackground = true;
        }
        break;
      case 'padding':
        if (!background) {
          throw new Error('Could not find text background');
        }
        redoBackground = true;
        textbox.data.padding = value;
        break;
      case 'backgroundOpacity':
        backgroundColor =
          backgroundColor.slice(0, 7) +
          ((Math.round(value * 2.55) || 0) < 16 ? '0' : '') +
          (Math.round(value * 2.55) || 0).toString(16);
        background.fillColor = backgroundColor;
        textbox.data.background = backgroundColor;

        if (textbox.data.editTextbox) {
          textbox.data.editTextbox.div.style.background = backgroundColor;
        }
        break;
      case 'backgroundColor':
        backgroundColor = value + backgroundColor.slice(7);
        background.fillColor = backgroundColor;
        textbox.data.background = backgroundColor;

        if (textbox.data.editTextbox) {
          textbox.data.editTextbox.div.style.background = backgroundColor;
        }
        break;
      case 'fontSize': {
        const change = value - textbox.data.fontSize;
        textbox.data.fontSize = value;
        textPoint.fontSize = value;
        textPoint.point.y = textPoint.point.y + change;
        redoBackground = true;

        if (textbox.data.editTextbox) {
          textbox.data.editTextbox.div.style.fontSize =
            value * paper.view.zoom + 'px';
        }
        break;
      }
      case 'color':
        textPoint.fillColor = value;
        textbox.data.color = value;

        if (textbox.data.editTextbox) {
          textbox.data.editTextbox.div.style.color = value;
        }
        break;
    }

    if (redoBackground) {
      if (background) {
        background.remove();
      }

      if (!textbox.data.noBackground) {
        const newProperties = {
          x: textPoint.bounds.x - textbox.data.padding,
          y: textPoint.bounds.y - textbox.data.padding,
          width: textPoint.bounds.width + 2 * textbox.data.padding,
          height: textPoint.bounds.height + 2 * textbox.data.padding,
          fillColor: backgroundColor
        };
        background = new paper.Path.Rectangle(newProperties);
        textbox.insertChild(0, background);
        background.selected = true;

        if (textbox.data.editTextbox) {
          // Convert the position to screen pixels
          const divPoint = new paper.Point(textbox.data.position).subtract(
            textbox.data.padding
          );
          const pixels = fromDrawingCoordinates(divPoint.x, divPoint.y);
          textbox.data.editTextbox.div.style.padding =
            textbox.data.padding * paper.view.zoom + 'px';
          textbox.data.editTextbox.div.style.left = pixels.x + 'px';
          textbox.data.editTextbox.div.style.top = pixels.y + 'px';
          textbox.data.editTextbox.div.style.background = backgroundColor;
          textbox.data.editTextbox.div.style.minHeight =
            (textbox.data.fontSize + 2 * textbox.data.padding) *
              paper.view.zoom +
            'px';
        }
      }
    }
  };

  /**
   * Update a textbox stored position based on its current position
   *
   * @param item Textbox to update the stored position for
   */
  const updateTextboxPosition = (item) => {
    item.data.position = [
      item.bounds.x + item.data.padding,
      item.bounds.y + item.data.padding
    ];
  };

  /**
   * Create an image on the drawing. Returns a Promise that will resolve
   * once the image has loaded
   *
   * @param file URL to image or File of image to insert
   * @param position Position to insert the image
   *
   * @return Promise that will resolve to the created image once the image
   *   is loaded
   */
  const createImage = (file: string | File | NamedFile, position?: Point) => {
    const id = makeId();
    const element = {
      type: 'Raster',
      name: id,
      position: null,
      source: null
    };
    const emitObject = {
      action: 'create',
      layerName: imagesLayer
        ? imagesLayer.name
        : paper.project.activeLayer.name,
      element: null
    };

    paper.project.deselectAll();

    let promise;

    if (typeof file === 'string') {
      element.source = file;
      promise = Promise.resolve();
    } else {
      let filename;
      if (file instanceof File) {
        filename = file.name;
      } else if (file.name && file.file) {
        filename = file.name;
        file = file.file;
      }
      promise = app.files
        .requestUpload(app.draftId, file.name, file.size)
        .then((result) => {
          filename = result.newFilename || result.filename;
          const fileUri = `files/${filename}`;
          // TODO Change to join and URI config
          const uploadData = {
            filename,
            progress: null
          };
          const uploadPromise = app.files
            .upload(app.draftId, result.nonce, filename, file, {
              progress: (event) => {
                if (
                  typeof event.loaded === 'number' &&
                  typeof event.total === 'number'
                ) {
                  uploadData.progress = event.loaded / event.total;
                  debounceUpdateUploadingStatus();
                }
              }
            })
            .then((response) => {
              if (response.status >= 200 && response.status < 300) {
                // Activate image layer
                // Encode URL
                const encodedUri = encodeURI(fileUri);

                element.source = encodedUri;
              }
            });

          if (imageUploadingStatus) {
            imageUploadingStatus.addPromise(uploadPromise, uploadData);
          }

          return uploadPromise;
        })
        .catch((error) => {
          if (app.status) {
            if (error?.response?.status === 413) {
              app.status.createStatus(
                `File ${filename} was too large to upload. Please resize and try again.`,
                {
                  type: 'error',
                  timeout: true
                }
              );
            } else {
              app.status.createStatus(
                `Error uploading file: ${error.message}`,
                {
                  type: 'error',
                  timeout: true
                }
              );
            }
          } else {
            console.error(`Error uploading file: ${error.message}`);
          }
        });
    }

    return promise
      .then(() => insertImage(element, { noEmit: true }))
      .then((raster) => {
        return new Promise((resolve, reject) => {
          // Move image to correct position
          const callback = () => {
            raster.position = position.add(raster.size.divide(2));
            raster.data.data.position = [raster.position.x, raster.position.y];
            raster.data.data.imageWidth = raster.width;
            raster.data.data.imageHeight = raster.height;
            raster.selected = true;
            emitObject.element = raster.data.data;
            emit('draw', emitObject);
            raster.off('load', callback);
            updateImage(raster).then((updatedRaster) => {
              resolve(updatedRaster);
            });
          };
          if (raster.loaded) {
            callback();
          } else {
            raster.on('load', callback);
          }
        });
      });
  };

  /**
   * Find the thumbnail that should be currently being used for the given
   * image.
   *
   * @param image Image to get the correct thumbnailLevel for
   * @param ignoreScale Whether or not to ignore the current view scale
   *
   * TODO Add image scaling to zoomlevel to create image zoom
   */
  const findThumbnailLevel = (image: ImageLayer, ignoreScale?: boolean) => {
    if (ignoreScale) {
      return app.config.thumbnails.find(
        (level) =>
          image.imageWidth * image.imageHeight >= level.minimumImageSize
      );
    } else {
      return app.config.thumbnails.find(
        (level) =>
          level.scale > paper.view.zoom &&
          image.imageWidth * image.imageHeight >= level.minimumImageSize
      );
    }
  };

  const thumbnailUrl = (source: string, level: ThumbnailConfig) =>
    'thumbs/' + source + '?size=' + level.thumbnailBigDimension;

  const preloadImage = (image: ImageLayer) => {
    // Preload thumbnails
    if (app.config.preloadThumbnails) {
      app.config.thumbnails.forEach((level) => {
        if (!(image.imageWidth * image.imageHeight >= level.minimumImageSize)) {
          return;
        }

        const cached = new Image();
        cached.src = thumbnailUrl(image.source, level);
      });
    }

    // Preload original
    if (app.config.preloadOriginal) {
      const cached = new Image();
      cached.src = image.source;
    }
  };

  /**
   * Update the imags to ensure using the thumbnail or the original image
   *
   * @param forceFullsize If true, all images will be forced to their
   *   full-sized version
   *
   * @returns A Promise that resolves once all images have been updated
   */
  const updateImages = (forceFullsize?: boolean): Promise<void> => {
    if (app.config.thumbnails && app.config.thumbnails.length) {
      const promises = [];
      paper.project.layers.forEach((layer) => {
        // Create a copy of children as will change during updating of images
        const children = [...layer.children];
        children.forEach((item) => {
          if (
            item instanceof paper.Raster &&
            item.data.data.source.startsWith('files/')
          ) {
            promises.push(updateImage(item, forceFullsize));
          }
        });
      });

      return Promise.all(promises);
    }

    return Promise.resolve();
  };

  /**
   * Ensure the raster is using the right thumbnail for the scale and the
   * current offset
   *
   * @param raster Raster to check the current source of
   *
   * @returns A Promise that resolves to the updated raster
   */
  const updateImage = (
    raster: paper.Raster,
    forceFullsize?: boolean
  ): Promise<paper.Raster> => {
    // Ensure image is stored on the server
    if (!raster.data.data.source.startsWith('files/')) {
      return Promise.resolve(raster);
    }

    // Check if the raster is visible or within the padding
    const thumbnailLevel = forceFullsize
      ? null
      : findThumbnailLevel(raster.data.data, !itemWithinView(raster));

    if (!thumbnailLevel) {
      return setImageSource(raster, raster.data.data.source);
    } else {
      return setImageSource(
        raster,
        thumbnailUrl(raster.data.data.source, thumbnailLevel)
      );
    }
  };

  let imageUpdateDebounce = null;

  /**
   * Debounce calls to update images by waiting until there are no more calls
   * before updating
   */
  const debounceUpdateImages = () => {
    if (imageUpdateDebounce !== null) {
      clearTimeout(imageUpdateDebounce);
    }

    imageUpdateDebounce = setTimeout(() => {
      imageUpdateDebounce = null;
      updateImages();
    }, 500);
  };

  /**
   * Updates a raster's source if needed by creating a new raster with the
   * new source and replacing that existing raster once it has loaded
   *
   * @param raster Raster to update
   * @param source New source URL
   *
   * @returns A Promise that resolves to either the original raster, if the
   *   source didn't need to be updated, or the new raster, if the source
   *   did need to be updated
   */
  const setImageSource = (
    raster: paper.Raster,
    source: string
  ): Promise<paper.Raster> => {
    if (raster.data.source !== source) {
      // Create a new raster
      const newRaster = new paper.Raster(source, raster.position);
      newRaster.visible = false;
      newRaster.data = {
        data: raster.data.data,
        source
      };
      newRaster.insertAbove(raster);

      const promise = resetImageDimensions(newRaster);
      addImageLoader(newRaster);
      return promise.then(() => {
        newRaster.visible = true;
        raster.remove();
        newRaster.name = newRaster.data.data.name;
        return newRaster;
      });
    } else {
      return Promise.resolve(raster);
    }
  };

  /**
   * Reset the dimensions of a raster to the stored size or the image size
   *
   * @param raster Raster to fix the size on
   *
   * @returns A Promise that resolves once the image has been resized
   */
  const resetImageDimensions = (raster: paper.Raster): Promise<void> => {
    return new Promise((resolve, reject) => {
      if (raster.loaded) {
        const width = raster.data.data.width || raster.data.data.imageWidth;
        if (width) {
          raster.width = width;
        }
        const height = raster.data.data.height || raster.data.data.imageHeight;
        if (height) {
          raster.height = height;
        }
        resolve();
      } else {
        const callback = () => {
          const width = raster.data.data.width || raster.data.data.imageWidth;
          if (width) {
            raster.width = width;
          }
          const height =
            raster.data.data.height || raster.data.data.imageHeight;
          if (height) {
            raster.height = height;
          }

          raster.off('load', callback);
          resolve();
        };
        raster.on('load', callback);
      }
    });
  };

  /**
   * Insert the given image into the given layer or the image layer
   *
   * @param image Image to insert
   * @param layerId Layer ID to insert the image into
   *
   * @returns A Promise that resolves to the instance of the inserted image
   *   once the image has loaded
   */
  const insertImage = (
    image: ImageLayer,
    {
      layerId,
      initial,
      noEmit
    }: {
      layerId?: string;
      initial?: boolean;
      noEmit?: boolean;
    }
  ) => {
    return new Promise((resolve, reject) => {
      let oldActiveLayer;
      if (layerId) {
        oldActiveLayer = paper.project.activeLayer;
        const layer = paper.project.layers.find(
          (item) => item.name === layerId
        );
        if (!layer) {
          return Promise.reject(new Error('No layer with given id ' + layerId));
        }
        layer.activate();
      } else if (imagesLayer) {
        oldActiveLayer = paper.project.activeLayer;
        imagesLayer.activate();
      }

      if (!(image.source.startsWith('files/') && app.config.thumbnails)) {
        // Load image normally if thumbs disabled or not a local file
        const raster = new paper.Raster(image.source, image.position);
        raster.name = image.name;
        raster.data = {
          source: image.source,
          data: image
        };
        if (image.width || image.height) {
          raster.width = image.width;
          raster.height = image.height;
        }
        addImageLoader(raster);
        const callback = () => {
          resolve(raster);
          raster.off('load', callback);
        };
        raster.on('load', callback);
        raster.on('error', (error) => {
          // Remove and silently fail
          raster.off('load', callback);
          raster.remove();
          resolve(null);
        });
      } else if (image.imageWidth && image.imageHeight) {
        const thumbnailLevel = findThumbnailLevel(
          image,
          initial || !withinView()
        );

        let source;
        if (!thumbnailLevel) {
          source = image.source;
        } else {
          source =
            'thumbs/' +
            image.source +
            '?size=' +
            thumbnailLevel.thumbnailBigDimension;
        }
        const raster = new paper.Raster(source, image.position);
        raster.name = image.name;
        raster.data = {
          source: source,
          data: image
        };
        resetImageDimensions(raster);
        addImageLoader(raster);
        const callback = () => {
          resolve(raster);
          raster.off('load', callback);
        };
        raster.on('load', callback);
        raster.on('error', (error) => {
          // Remove and silently fail
          raster.off('load', callback);
          raster.remove();
          resolve(null);
        });
      } else {
        // Load original image, then update with width and height
        const raster = new paper.Raster(image.source, image.position);
        raster.name = image.name;
        raster.data = {
          source: image.source,
          data: image
        };
        if (image.width || image.height) {
          raster.width = image.width;
          raster.height = image.height;
        }
        addImageLoader(raster);

        const callback = () => {
          image.imageWidth = raster.image.width;
          image.imageHeight = raster.image.height;

          if (!noEmit) {
            // Emit update for image
            const emitObject = {
              action: 'modify', // TODO ??
              layerName: layerId || raster.layer.name,
              element: { ...image }
            };

            // TODO Check it is an image
            emitObject.element.imageWidth = raster.image.width;
            emitObject.element.imageHeight = raster.image.height;

            // Emit update
            emit('draw', emitObject);

            // Update to the thumbnail if required
            updateImage(raster);
          }

          raster.off('load', callback);
        };

        raster.on('load', callback);

        resolve(raster);
      }

      if (oldActiveLayer) {
        oldActiveLayer.activate();
      }
    });
  };

  // Set up ResizeObserver
  const resizeObserver = new ResizeObserver(() => {
    const bounds = canvas.getBoundingClientRect();
    if (canvas.width !== bounds.width || canvas.height !== bounds.height) {
      canvas.width = bounds.width;
      canvas.height = bounds.height;
      paper.view.setViewSize(bounds);
      updateCenter();
    }
  });
  resizeObserver.observe(canvas);

  const promises = [];

  if (drawing) {
    paper.project.clear();
    canvasCover.hidden = false;
    // Load project
    drawing.layers.forEach((layer) => {
      const layerItem = new paper.Layer();
      layerItem.name = layer.name;

      layer.children.forEach((element) => {
        switch (element.type) {
          case 'Textbox':
            createTextbox(element);
            break;
          case 'Raster':
            promises.push(
              insertImage(element, { layer: layer.name, initial: true })
            );
            break;
          default:
            layerItem.importJSON([element.type, element]);
        }
      });
    });

    drawingLayer = paper.project.layers.find(
      (layer) => layer.name === 'drawing'
    );
    annotationLayer = paper.project.layers.find(
      (layer) => layer.name === 'annotation'
    );
    imagesLayer = paper.project.layers.find((layer) => layer.name === 'images');

    // TODO Add config check
  } else {
    // Create 3 default layers - text, drawing and images
    imagesLayer = new paper.Layer({ name: 'images' });
    drawingLayer = new paper.Layer({ name: 'drawing' });
    annotationLayer = new paper.Layer({ name: 'annotation' });
  }

  // Default active layer to drawing layer
  if (drawingLayer) {
    drawingLayer.activate();
  }

  // Attach for draw events
  app.socket.on('draft:draw', (data) => {
    if (typeof data !== 'object' || data === null) {
      return;
    }
    // TODO Check if event is for this drawing

    if (data.action === 'position' && data.elements) {
      data.elements.forEach((element) => {
        const layer = paper.project.layers.find(
          (item) => item.name === element.layerName
        );

        if (layer) {
          const child = layer.children.find(
            (item) => item.name === element.elementName
          );

          if (child) {
            if (child.type === 'Textbox') {
              const position = new paper.Point([
                element.position[0] + child.bounds.width / 2,
                element.position[1] + child.bounds.height / 2
              ]);
              child.position = position.subtract(child.data.padding);
              updateTextboxPosition(child);
            } else {
              child.position = new paper.Point(element.position);
            }
          }
        }
      });

      return;
    } else if (data.action === 'deleteMultiple') {
      if (!data.elements) {
        return;
      }

      data.elements.forEach((element) => {
        const layer = paper.project.layers.find(
          (item) => item.name === element.layerName
        );
        if (!layer) {
          // TODO out of sync?
          return;
        }

        const item = layer.children.find(
          (child) => child.name === element.elementName
        );

        if (!item) {
          // TODO out of sync?
          return;
        }

        item.remove();
      });
      return;
    } else if (data.action === 'modifyMultiple') {
      if (!data.elements) {
        return;
      }

      data.elements.forEach((element) => {
        const layer = paper.project.layers.find(
          (layer) => layer.name === element.layerName
        );
        if (!layer) {
          // TODO out of sync?
          return;
        }

        // Try and find element if already in the layer
        const index = layer.children.findIndex(
          (child) => child.name === element.element.name
        );

        let item;
        switch (element.element.type) {
          case 'Textbox':
            item = createTextbox(element.element);
            break;
          default:
            item = layer.importJSON([element.element.type, element.element]);
        }

        if (index === -1) {
          if (typeof data.index === 'number') {
            layer.insertChild(index, item);
          } else {
            layer.addChild(item);
          }
        } else {
          layer.children[index].replaceWith(item);
        }
      });
      return;
    }

    {
      // Find the layer
      const layerIndex = paper.project.layers.findIndex(
        (item) => item.name === data.layerName
      );
      const layer = layerIndex !== -1 ? paper.project.layers[layerIndex] : null;

      if (data.action === 'createLayer') {
        // Add layer to drawing
        paper.project.addLayer(data.element);
        return;
      }

      if (layer) {
        if (data.action === 'delete') {
          const item = layer.children.find(
            (child) => child.name === data.elementName
          );

          if (!item) {
            // TODO out of sync?
            return;
          }

          item.remove();
          return;
        }

        // Try and find element if already in the layer
        const index = layer.children.findIndex(
          (element) => element.name === data.element.name
        );

        let item;
        switch (data.action) {
          case 'create':
          case 'modify':
            // Create item from element
            switch (data.element.type) {
              case 'Textbox':
                item = createTextbox(data.element);
                break;
              case 'Path':
                item = new paper.Path(data.element);
                break;
              case 'Raster': // TODO
                item = new paper.Raster(data.element);
                break;
              default:
                return;
            }

            if (index === -1) {
              if (typeof data.index === 'number') {
                layer.insertChild(index, item);
              } else {
                layer.addChild(item);
              }
            } else {
              layer.children[index].replaceWith(item);
            }
            break;
        }
      }
    }
  });

  // Attach for errors
  app.socket.on('draft:reject', (data) => {
    if (data.elementName) {
      const layer = paper.project.layers.find(
        (item) => item.name === data.layerName
      );

      if (layer) {
        const item = layer.children.find(
          (child) => child.name === data.elementName
        );

        if (item) {
          if (item.type === 'Textbox' && item.data.editTextbox) {
            item.data.editTextbox.cancel();
          }
          item.remove();
        }
      }
    }
  });

  const thisDrawing: DrawingManager = {
    Point: paper.Point,
    addEventListener: (type, handler, raw?: boolean) => {
      if (raw) {
        canvas.addEventListener(type, handler);
      } else if (eventHandlers[type]) {
        eventHandlers[type].push(handler);
      } else {
        eventHandlers[type] = [handler];
        if (internalEvents.indexOf(type) === -1) {
          canvas.addEventListener(type, drawingEvent);
        }
      }
    },
    removeEventListener: (type, handler) => {
      // Check raw event handlers
      const rawIndex = rawEventHandlers.findIndex(
        (rawHandler) =>
          rawHandler.type === type && rawHandler.handler === handler
      );

      if (rawIndex !== -1) {
        canvas.removeEventListener(type, handler);
      } else if (eventHandlers[type]) {
        const index = eventHandlers[type].indexOf(handler);
        if (index !== -1) {
          eventHandlers[type].splice(index, 1);
          if (!eventHandlers[type].length) {
            delete eventHandlers[type];
            if (internalEvents.indexOf(type) === -1) {
              canvas.removeEventListener(type, drawingEvent);
            }
          }
        }
      }
    },
    get lastPointerPosition() {
      return lastPointerPosition;
    },
    showCover() {
      canvasCover.hidden = false;
    },
    hideCover() {
      canvasCover.hidden = true;
    },
    get lastPointerCoordinates() {
      return lastPointerCoordinates;
    },
    get lastPointerMovement() {
      return new paper.Point(lastPointerMovement);
    },
    get canvas() {
      return canvas;
    },
    get drawing() {
      const copy = paper.project
        .exportJSON({ asString: false })
        .filter(([type, item]) => type === 'Layer')
        .map(([type, item]) => item);
      return copy;
    },
    toDrawingCoordinates,
    fromDrawingCoordinates,
    selectAll: () => {
      paper.project.selectAll();
    },
    deselectAll: () => {
      paper.project.deselectAll();
    },
    select: (items, addition?: boolean) => {
      if (!addition) {
        paper.project.deselectAll();
      }
      if (Array.isArray(items)) {
        for (let i = 0; i < items.length; i++) {
          items[i].selected = true;
        }
      } else {
        items.selected = true;
      }
    },
    selectWithin: (coordinate1, coordinate2) => {
      const rectangle = new paper.Rectangle(coordinate1, coordinate2);

      paper.project.deselectAll();

      // TODO Act on only active layer?
      for (let i = 0; i < paper.project.layers.length; i++) {
        const layer = paper.project.layers[i];

        for (let j = 0; j < layer.children.length; j++) {
          const item = layer.children[j];

          if (item.isInside(rectangle)) {
            item.selected = true;
          }
        }
      }
    },
    selectCrossing: (coordinate1, coordinate2) => {
      const rectangle = new paper.Rectangle(coordinate1, coordinate2);
      const rectangleItem = new paper.Path.Rectangle(coordinate1, coordinate2);

      paper.project.deselectAll();

      // TODO Act on only active layer?
      for (let i = 0; i < paper.project.layers.length; i++) {
        const layer = paper.project.layers[i];

        for (let j = 0; j < layer.children.length; j++) {
          const item = layer.children[j];
          if (item === rectangle) {
            continue;
          }

          if (item.isInside(rectangle) || item.intersects(rectangleItem)) {
            item.selected = true;
          }
        }
      }

      rectangleItem.remove();
    },
    get selected() {
      return cleanItems(paper.project.selectedItems);
    },
    deleteSelected: () => {
      const ids = [];
      const items = cleanItems(paper.project.selectedItems);
      items.forEach((item) => {
        ids.push({
          elementName: item.name,
          layerName: item.layer.name
        });
        item.remove();
      });

      emit('draw', {
        action: 'deleteMultiple',
        elements: ids
      });
    },
    delete: (items) => {
      if (!Array.isArray(items)) {
        items = [items];
      }

      const ids = [];

      items.forEach((item) => {
        ids.push({
          elementName: item.name,
          layerName: item.layer.name
        });
        item.remove();
      });

      emit('draw', {
        action: 'deleteMultiple',
        elements: ids
      });
    },
    get offset() {
      return offset;
    },
    setOffset: (x: number, y: number) => {
      offset = new paper.Point(x, y);
      updateCenter();
    },
    getStyle: (style: string) => {
      return canvas.style[style];
    },
    setStyle: (style: string, value: any) => {
      canvas.style[style] = value;
    },
    /**
     * Scroll the canvas by a given amount delta
     *
     * @param delta A Point containing the amount to scroll the drawing by.
     *  A positive x/y value will scroll the drawing right/down.
     *  A negative number will scroll the drawing to the left/up.
     * @param point A new offset from which to start scroll
     */
    scrollBy: (delta: Point, point?: Point) => {
      if (point === true) {
        offset = new paper.Point(0, 0).add(delta);
        updateCenter();
      } else if (point) {
        offset = point.add(delta);
        updateCenter();
      } else {
        offset = offset.add(delta);
        updateCenter();
      }
    },
    zoomIn: (point?: Point, amount?: number) => {
      if (!amount || amount < 0) {
        amount = 0.1 * paper.view.zoom;
      }

      updateZoom(paper.view.zoom + amount, point);
    },
    zoomOut: (point?: Point, amount?: number) => {
      if (!amount || amount < 0) {
        amount = -0.1 * paper.view.zoom;
      }

      updateZoom(paper.view.zoom + amount, point);
    },
    get zoom() {
      return paper.view.zoom;
    },
    zoomTo: (amount: number, point?: Point) => {
      updateZoom(amount, point);
    },
    zoomToExtent: () => {
      zoomToExtent();
    },
    updateView,
    updateViewFromUrl,
    moveItems: (items, active) => {
      const positions = items.map((item) => {
        item.item.position = item.position;
        if (item.item.type === 'Textbox') {
          updateTextboxPosition(item.item);
          return {
            elementName: item.item.name,
            layerName: item.item.layer.name,
            position: item.item.data.position
          };
        } else {
          return {
            elementName: item.item.name,
            layerName: item.item.layer.name,
            position: [item.position.x, item.position.y]
          };
        }
      });

      debounceEmit('draw', {
        action: 'position',
        elements: positions,
        active
      });
    },
    setProperty: (items, property: string, value: any) => {
      if (!Array.isArray(items)) {
        items = [items];
      }

      const elements = [];
      items.forEach((item) => {
        if (item.parent.type === 'Textbox') {
          return;
        }
        if (item.type === 'Textbox') {
          updateTextProperty(item, property, value);
        } else {
          item[property] = value;
        }
        elements.push({
          layerName: item.parent.name,
          element: makeObject(item)
        });
      });

      debounceElementsEmit('modifyMultiple', elements);
    },
    setProperties: (items, properties: { [key: string]: any }) => {
      if (!Array.isArray(items)) {
        items = [items];
      }

      const keys = Object.keys(properties);
      let item;
      let elements;
      for (let i = 0; i < items.length; i++) {
        for (let k = 0; k < keys.length; k++) {
          item = items[i];

          if (item.parent.type === 'Textbox') {
            continue;
          }

          if (item.type === 'Textbox') {
            updateTextProperty(item, property, value);
          } else {
            item[property] = value;
          }
        }
        elements.push({
          layerName: item.parent.name,
          element: makeObject(item)
        });
      }

      debounceElementsEmit('modifyMultiple', elements);
    },
    createRectangle: (options = {}, share: boolean = true) => {
      // Typescript can't handle atm const { deselectAll, ...options } = options;
      const deselectAll = options.deselectAll;
      delete options.deselectAll;
      options = {
        type: 'Rectangle',
        strokeColor: 'black',
        strokeWidth: 2,
        strokeCap: 'round',
        ...options
      };
      let timeout = null;
      if (deselectAll !== false) {
        paper.project.deselectAll();
      }
      const path = new paper.Path.Rectangle({
        ...options,
        selected:
          typeof options.selected !== 'undefined' ? options.selected : true
      });
      const id = makeId();
      path.name = id;
      const layerName = paper.project.activeLayer.name;

      if (share) {
        // TODO simplify as progressing?
        // Paper exports as [ 'Path', object ], so just grab object
        const pathObject = path.exportJSON({ asString: false })[1];
        emit('draw', {
          action: 'create',
          layerName,
          element: {
            ...options,
            name: id,
            matrix: pathObject.matrix,
            segments: pathObject.segments
          }
        });
      }

      return {
        select: () => {
          path.selected = true;
        },
        unselect: () => {
          path.selected = false;
        },
        get removed() {
          return path.parent === null;
        },
        remove: () => {
          path.remove();
          if (timeout !== null) {
            clearTimeout(timeout);
            timeout = null;
          }
          if (share) {
            // Emit a delete
            emit('draw', {
              action: 'delete',
              layerName,
              elementName: id,
              active: true
            });
          }
        },
        path
      };
    },
    createPath: (options = {}, share: boolean = true) => {
      options = {
        type: 'Path',
        strokeColor: 'red',
        strokeWidth: 2,
        strokeCap: 'round',
        ...options
      };
      if (options.strokeCap === 'none') {
        options.strokeCap = 'butt';
      }
      let timeout = null;
      let lastActive = false;
      paper.project.deselectAll();
      const path = new paper.Path({
        ...options,
        selected:
          typeof options.selected !== 'undefined' ? options.selected : true,
        commit: false
      });
      const id = makeId();
      path.name = id;
      const layerName = paper.project.activeLayer.name;

      const pathEmit = (active: boolean = false) => {
        lastActive = active;
        // TODO send emit instance on path completion
        // Debounce emitting of events for performance
        if (timeout === null) {
          timeout = setTimeout(() => {
            const element = makeObject(path);
            emit('draw', {
              action: 'create',
              layerName,
              element,
              active: lastActive
            });
            timeout = null;
          }, 100);
        }
      };

      return {
        addPoint: (point, drawing?: boolean) => {
          path.add(point);
          if (share) {
            pathEmit(drawing);
          }
        },
        removePoint: (index, drawing?: boolean) => {
          path.removeSegment(index);
          if (share) {
            pathEmit(drawing);
          }
        },
        join: (otherPath) => {
          // Get segments from the path
          if (!otherPath.path) {
            throw new Error('Not given a path to join on to path');
          }

          path.addSegments(otherPath.path.segments);

          if (share) {
            pathEmit(drawing);
          }
        },
        select: () => {
          path.selected = true;
        },
        get removed() {
          return path.parent === null;
        },
        remove: () => {
          path.remove();
          if (timeout !== null) {
            clearTimeout(timeout);
            timeout = null;
          }
          // Emit a delete
          if (share) {
            emit('draw', {
              action: 'delete',
              layerName,
              elementName: id,
              active: true
            });
          }
        },
        smooth: () => {
          path.simplify(10);
        },
        finish: (simplify?: boolean | number | null) => {
          if (simplify === null || simplify === true) {
            simplify = 10;
          }
          if (simplify) {
            path.simplify(simplify);
          }

          // TODO Need to check if should be removed
          delete path.commit;

          if (share) {
            pathEmit();
          }
        },
        path
      };
    },
    createImage,
    /**
     * Insert images into the current drawing
     *
     * @param imageFiles Image files to insert
     * @param position Position for the images to be inserted from
     * @param rowData Row data from a previous insertion to continue
     */
    createImages: (
      imageFiles: Array<string | File | NamedFile>,
      position?: Point,
      rowData?: Draft.RowData
    ) => {
      const maxRow = app.config.maxImageRow || 4;
      const spacing = app.config.imageSpacing;

      let nextPosition;
      let rowOffset = 0;
      let currentRowHeight = 0;
      let currentRowWidth = 0;
      let maximumRowWidth = 0;

      if (rowData) {
        ({
          nextPosition,
          maximumRowWidth = 0,
          currentRowHeight = 0,
          currentRowWidth = 0,
          nextOffset: rowOffset = 0,
          position
        } = rowData);

        if (!nextPosition) {
          nextPosition = position;
        }
      } else {
        if (!position) {
          position = new paper.Point(10).add(offset);
        }
        nextPosition = position;
      }

      let promise = Promise.resolve();

      // TODO!!!
      // Rewrite so that:
      //   it calculates the next postion after each image
      imageFiles.forEach((image, imageIndex) => {
        promise = promise.then(() => {
          return createImage(image, nextPosition).then((imageElement) => {
            // Move all images except the first one, which will be in the
            // coorect position to begin with
            if (imageIndex) {
              const imagePosition = new paper.Point(
                nextPosition.x + imageElement.width / 2,
                nextPosition.y + imageElement.height / 2
              );

              imageElement.position = imagePosition;
              imageElement.data.data.position = [
                imagePosition.x,
                imagePosition.y
              ];

              emit('draw', {
                action: 'position',
                elements: [
                  {
                    elementName: imageElement.name,
                    layerName: imageElement.parent.name,
                    position: [imagePosition.x, imagePosition.y]
                  }
                ]
              });
            }

            currentRowHeight = Math.max(currentRowHeight, imageElement.height);

            // Calculate position for next image
            // Start new row if image should be in new row
            if ((rowOffset + imageIndex + 1) % maxRow === 0) {
              currentRowWidth += imageElement.width + spacing;
              maximumRowWidth = Math.max(maximumRowWidth, currentRowWidth);

              nextPosition = new paper.Point(
                position.x,
                nextPosition.y + currentRowHeight + spacing
              );

              currentRowHeight = 0;
              currentRowWidth = 0;
            } else {
              currentRowHeight = Math.max(
                imageElement.height,
                currentRowHeight
              );
              currentRowWidth += imageElement.width + spacing;
              nextPosition = new paper.Point(
                nextPosition.x + imageElement.width + spacing,
                nextPosition.y
              );
            }

            return imageElement;
          });
        });
      });

      return promise.then(() => {
        // Add the current row height and max width to the current row
        // position to make the bounds
        maximumRowWidth = Math.max(maximumRowWidth, currentRowWidth);

        const bounds = new paper.Rectangle(
          position,
          new paper.Point(
            position.x + maximumRowWidth,
            nextPosition.y + currentRowHeight + spacing
          )
        );

        // Ensure image is in view
        zoomToItem({ bounds });

        return {
          bounds,
          position,
          maximumRowWidth,
          currentRowHeight,
          currentRowWidth,
          nextPosition,
          nextOffset: (rowOffset + imageFiles.length) % maxRow
        };
      });
    },
    createTextbox: (position: Point, text?: string, options = {}) => {
      if (text) {
        options.content = text;
      }
      options.position = [position.x, position.y];
      options.name = makeId();

      const currentLayer = paper.project.activeLayer;
      if (annotationLayer) {
        annotationLayer.activate();
      }
      paper.project.deselectAll();

      const textbox = createTextbox({
        ...options,
        commit: false
      });
      const layerName = paper.project.activeLayer.name;
      textbox.selected = true;

      const emitObject = {
        action: 'create',
        layerName,
        element: {
          ...textbox.data,
          type: 'Textbox'
        },
        active: true
      };

      // Emit an active event
      emit('draw', emitObject);

      // Start editing the textBox if no text given
      const edit = editTextbox(textbox, true);

      // Reactivate current layer
      if (annotationLayer) {
        currentLayer.activate();
      }

      return {
        textbox,
        edit
      };
    },
    editTextbox,
    remove: () => {
      Object.entries(eventHandlers).forEach(([type, handlers]) => {
        if (internalEvents.indexOf(type) === -1) {
          handlers.forEach((handler) => canvas.removeEventListener(handler));
        }
      });

      if (resizeObserver) {
        resizeObserver.unobserve(canvas);
        resizeObserver.disconnect();
      }
    },
    exportSVG: (options = {}) => {
      if (options.bounds === 'content') {
        return new updateImages(true)
          .then(() => {
            return paper.project.exportSVG({
              ...options,
              asString: true
            });
          })
          .then((svg) => {
            return updateImages().then(() => {
              return svg;
            });
          });
      } else {
        return new Promise((resolve) => {
          resolve(
            paper.project.exportSVG({
              ...options,
              asString: true
            })
          );
        });
      }
    },
    exportPNG: (options = {}) => {
      if (options.bounds === 'content') {
        const bounds = getFullBounds();
        const oldViewSize = paper.view.viewSize;
        const oldZoom = paper.view.zoom;
        const oldOffset = offset;
        // Test size of full canvas and error if too large (bigger than IE can handle)
        // see https://github.com/jhildenbiddle/canvas-size#test-results
        if (bounds.width * bounds.height > 67108864) {
          app.status.createStatus(
            'Image is too large to export. Try exporting views or as SVG instead',
            { type: 'error' }
          );
          return null;
        }
        canvas.style.width = bounds.width + 'px';
        canvas.style.height = bounds.height + 'px';

        return new Promise((resolve) => {
          // Redraw
          setTimeout(() => {
            paper.view.zoom = 1;
            offset = new paper.Point(0, 0);
            updateCenter();
            // Redraw
            setTimeout(() => {
              updateImages(true).then(() => {
                // Redraw
                setTimeout(() => {
                  canvas.toBlob(resolve);
                  canvas.style.width = '';
                  canvas.style.height = '';
                  // Redraw
                  setTimeout(() => {
                    paper.view.zoom = oldZoom;
                    offset = oldOffset;
                    updateCenter();
                  }, 0);
                }, 0);
              });
            }, 0);
          }, 0);
        }).then((png) => {
          //canvas.width = oldWidth;
          //canvas.height = oldHeight;
          paper.view.viewSize = oldViewSize;
          offset = oldOffset;
          updateCenter();
          return updateImages().then(() => {
            return png;
          });
        });
      } else {
        return new Promise((resolve) => {
          canvas.toBlob(resolve);
        });
      }
    }
  };

  return Promise.all(promises).then(() => {
    if (!updateViewFromUrl()) {
      zoomToExtent();
      urlAdded = true;
    }

    setUrl = true;
    canvasCover.hidden = true;
    return thisDrawing;
  });
};
