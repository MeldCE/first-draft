import { Config } from '../../typings/Config';

import { StatusOptions as Options, StatusModule } from '../../typings/Modules';

interface StatusPromises {
  promise: Promise<any>;
  data?: any;
}

const defaultTimeouts = {
  default: 2000,
  error: 5000
};

export const createStatus = (el: Element, config: Config) => {
  /* Store the status element of each status to tell how status messages are
   * showing
   */
  const statuses = [];
  let bannerElement;

  if (config.banner) {
    bannerElement = document.createElement('div');
    const text = document.createElement('div');
    bannerElement.appendChild(text);
    text.innerHTML = config.banner;
    el.appendChild(bannerElement);
  }

  /**
   * Called every time the statuses are changed
   */
  const statusesChanged = () => {
    // Show the banner if no status messages showing, or hide otherwise
    if (config.banner) {
      // See if there is a showing statusElement
      const showing = statuses.find((element) => element.parentElement);

      if (showing) {
        if (bannerElement.parentElement) {
          bannerElement.remove();
        }
      } else {
        if (!bannerElement.parentElement) {
          el.appendChild(bannerElement);
        }
      }
    }
  };

  return {
    createStatus: (status: string, options: Options = {}) => {
      let hidden = false;
      let statusElement = document.createElement('div');
      if (options.type) {
        statusElement.classList.add(options.type);
      }
      if (typeof options.timeout === 'undefined' && !options.promise) {
        options.timeout = true;
      }
      let statusText = document.createElement('div');
      let promises: null | Array<StatusPromises> = null;
      const waitingOnResolved: Array<{ resolve: () => void }> = [];
      statusText.innerHTML = status;
      statusElement.appendChild(statusText);

      el.appendChild(statusElement);
      statuses.push(statusElement);

      const addPromise = (promise: Promise<any>, data?: any) => {
        if (promises.indexOf(promise) === -1) {
          promises.push({ promise, data });
          promise
            .catch(() => Promise.resolve())
            .then(() => {
              const index = promises.findIndex(
                (item) => item.promise === promise
              );
              if (index !== -1) {
                promises.splice(index, 1);
                if (promises.length === 0) {
                  if (!hidden) {
                    statusElement.remove();
                    hidden = true;
                    statusesChanged();
                  }
                  const length = waitingOnResolved.length;
                  if (length) {
                    for (let i = 0; i < length; i++) {
                      waitingOnResolved[i].resolve();
                    }
                    waitingOnResolved.splice(0, length);
                  }
                } else if (options.updateStatus) {
                  statusText.innerText = options.updateStatus(promises);
                }
              }
            });
          if (options.updateStatus) {
            statusText.innerText = options.updateStatus(promises);
          }
        }
      };

      const remove = () => {
        statusElement.remove();
        const index = statuses.indexOf(statusElement);
        if (index !== -1) {
          statuses.splice(index, 1);
        }
        statusText = null;
        statusElement = null;

        // Update statuses
        statusesChanged();
      };

      const hide = () => {
        hidden = true;
        statusElement.remove();
      };

      if (options.hideAfterPromises) {
        promises = [];
        if (options.promise) {
          if (Array.isArray(options.promise)) {
            if (options.promise.length) {
              options.promise.forEach(addPromise);
            } else {
              hide();
            }
          } else {
            addPromise(options.promise);
          }
        } else {
          hide();
        }
      } else {
        if (options.promise) {
          if (!Array.isArray(options.promise)) {
            options.promise = [options.promise];
          }

          Promise.all(options.promise)
            .catch(() => Promise.resolve())
            .then(remove);
        }
      }

      // Update statuses
      statusesChanged();

      if (options.timeout) {
        if (options.timeout === true) {
          options.timeout =
            defaultTimeouts[options.type] || defaultTimeouts.default;
        }

        setTimeout(() => {
          if (options.hideAfterPromises) {
            hide();
          } else {
            remove();
          }
        }, options.timeout);
      }

      return {
        update: (newStatus?: string) => {
          if (!statusText) {
            throw new Error('Status message has already been removed');
          }
          if (newStatus) {
            statusText.innerHTML = newStatus;
          } else if (options.updateStatus) {
            statusText.innerHTML = options.updateStatus(promises);
          }
        },
        isShown: () => !hidden,
        hide,
        addPromise: (promise, data?: any) => {
          if (hidden) {
            el.appendChild(statusElement);
            hidden = false;
          }
          addPromise(promise, data);
          statusesChanged();
        },
        onceAllResolved: () => {
          if (promises && promises.length) {
            return new Promise((resolve) => {
              waitingOnResolved.push({ resolve });
            });
          } else {
            return Promise.resolve();
          }
        },
        remove
      };
    }
  } as StatusModule;
};
