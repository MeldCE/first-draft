import { App } from '../../typings/App';

import makeId from '../../lib/id';
import urlJoin from 'url-join';
import axios from 'axios';

interface RequestResult {
  nonce: string;
  exists: boolean;
}

interface UploadOptions {
  // Whether or not to replace the existing file with the same name
  replace?: boolean;
  // Callback to call every time a progress event is received for the upload
  progress?: (event: ProgressEvent) => void;
}

export const createFileManager = (app: App) => {
  const requests = {};

  app.socket.on('draft:file:upload:response', (data) => {
    const request = requests[data.id];
    if (request) {
      if (data.error) {
        request.reject(data.error);
      } else {
        request.resolve(data.result);
      }
      delete requests[data.id];
    }
  });

  return {
    requestUpload: (
      draftId: string,
      filename: string,
      filesize: number
    ): Promise<RequestResult> => {
      return new Promise((resolve, reject) => {
        if (filesize >= 10 * 1024 * 1024) {
          reject(new Error('File is too large'));
          return;
        }

        const id = makeId();

        requests[id] = {
          resolve,
          reject,
          filename,
          draftId
        };

        app.socket.emit('draft:file:upload:request', {
          id,
          draftId,
          filename
        });
      });
    },
    upload: (
      draftId: string,
      nonce: string,
      filename: string,
      file: File,
      options?: Options
    ) => {
      filename = encodeURIComponent(filename);
      const url = urlJoin('/d', app.draftId, 'files', filename);
      const fetchOptions = {
        url,
        method: options && options.replace ? 'PATCH' : 'PUT',
        headers: {
          'Content-Type': file.type,
          nonce
        },
        data: file
      };
      if (options && options.progress) {
        fetchOptions.onUploadProgress = options.progress;
      }
      return axios.request(fetchOptions);
    }
  };
};
