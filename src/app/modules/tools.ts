import { ToolChangeMessage } from '../../typings/Message';
import { App } from '../../typings/App';

import { getSha } from '../../lib/sha';
import { getDeep } from '../../lib/utils';
import tools from '../tools';
import { createForm } from './form';

import draftSettingsSchema from '../../definitions/schema/instanceOptions';

interface EventAction {
  tool: string;
  mapping: Mapping;
  parentEventAction?: EventAction;
  eventsCache: Array<{
    event: Event;
    ignoreEventActions: Array<ElementAction>;
  }>;
  startCoordinates: Point | null;
  neededMovement?: boolean | null;
  movement?: boolean | null;
  chainEventActions: boolean;
}

interface ElementAction {
  element: Element;
  eventAction: EventAction;
}

interface EventActions {
  [event: string]: Array<ElementAction>;
}

type Listener = (data: any) => void;

const mouseMoveThreshold = 0;
const touchMoveThreshold = 10;

const documentEvents = ['keyup', 'keydown', 'paste'];
const movementFilterTypes = ['mousedown', 'mouseup', 'touchstart', 'touchstop'];
const movementCheckEvents = [
  'mousedown',
  'mousemove',
  'mouseup',
  'touchstart',
  'touchmove',
  'touchend'
];

/**
 * Check if an event matches the filters
 *
 * @param event Event to check
 * @param filter Filter to check event against
 * @param or Only require one filter to match
 *
 * @returns Whether the event matches the filters
 */
const eventFilterScore = (event, filter, or: boolean = false) => {
  if (event.originalEvent) {
    event = event.originalEvent;
  }

  const entries = Object.entries(filter);
  let property;
  let value;
  let score;
  let total = 0;

  for (let i = 0; i < entries.length; i++) {
    //[ property, value ] = entries[i];
    property = entries[i][0];
    value = entries[i][1];

    score = -1;
    switch (property) {
      case '$or':
      case '$and':
        score = eventFilterScore(event, value, property === '$or') || -1;
        break;
      case 'buttons':
        if (event.buttons & value) {
          score = 1;
        }
        break;
      case 'button':
      case 'shiftKey':
      case 'ctrlKey':
      case 'altKey':
        if (event[property] === value) {
          score = 1;
        }
        break;
      case 'key':
        if (event.key === value) {
          score = 1;
        }
        break;
      case 'touches':
        if (event.touches && event.touches.length === value) {
          score = 1;
        }
        break;
      case 'movement':
      default:
        score = 0;
        break;
    }

    if (or) {
      if (score === -1 && total === 0) {
        total = -1;
      } else if (score > total) {
        total = score;
      }
    } else {
      if (score === -1) {
        return -1;
      }

      total += score;
    }
  }

  return total;
};

const mappingNeedsMovement = (mapping) =>
  mapping.type === 'click' ||
  (movementFilterTypes.indexOf(mapping.type) !== -1 &&
    mapping.filter &&
    typeof mapping.filter.movement === 'boolean');

const optionsOpenClass = 'optionsOpen';

export const createTools = (
  app: App,
  options,
  drawings: Array<DrawingManager>
) => {
  const eventActions: EventActions = {};
  let activeTool = null;
  const toolElements = {};
  const toolbar: HTMLElement = options.toolbar;
  const lastDrawing = drawings[0];
  const toolContexts = {};
  let lastToolChangeTime = null;
  let openContainer: null | {
    container: HTMLElement;
    closeCallback: () => void;
  } = null;
  const outsideListeners: { [key: string]: Array<Listener> } = {
    update: []
  };
  /// Pointer/keyboard mappings
  let toolMappings;
  /// Tools that have been enabled for the draft
  let enabledTools;
  let showToolbar;
  /// Set tool options
  let toolOptions;
  /// Allow draft settings to be modified
  let draftSettingsEnabled;

  const dropTarget = document.getElementById('dropTarget');
  const dropInfo = document.getElementById('dropInfo');
  const cover = document.getElementById('canvasCover');

  /**
   * Returns if the given container is open
   *
   * @param cotainer Container to see if currently open
   */
  const containerIsOpen = (container: HTMLElement) =>
    openContainer && openContainer.container === container;

  /**
   * Toggle or force a container open/close
   *
   * @param container Container HTML element
   * @param open A boolean to force whether the container should be opened
   *   or closed
   * @param closeCallback The function that will be called when the container
   *   is closed.
   */
  const toggleToolsContainer = (
    container?: HTMLElement | null | Event,
    open?: boolean,
    closeCallback?: () => void
  ) => {
    if (container instanceof Event) {
      if (container.defaultPrevented) {
        return;
      } else {
        container.preventDefault();
      }
    }
    if (!container || container instanceof Event) {
      if (!openContainer) {
        return;
      }
      open = false;
      container = openContainer.container;
    } else if (typeof open !== 'boolean') {
      open = !container.classList.contains(optionsOpenClass);
    }

    if (open) {
      if (!cover.classList.contains(optionsOpenClass)) {
        cover.hidden = false;
        cover.classList.add(optionsOpenClass);
        cover.addEventListener('click', toggleToolsContainer);
      }
      if (openContainer && openContainer.container !== container) {
        openContainer.container.classList.remove(optionsOpenClass);
        if (openContainer.closeCallback) {
          openContainer.closeCallback();
        }
      }
      openContainer = {
        container,
        closeCallback
      };
      container.classList.add(optionsOpenClass);
    } else {
      if (cover.classList.contains(optionsOpenClass)) {
        cover.classList.remove(optionsOpenClass);
        cover.hidden = true;
        cover.removeEventListener('click', toggleToolsContainer);
      }
      if (container.classList.contains(optionsOpenClass)) {
        container.classList.remove(optionsOpenClass);
        if (openContainer && openContainer.container === container) {
          if (openContainer.closeCallback) {
            openContainer.closeCallback();
          }
        }
      }
      if (openContainer && openContainer.container === container) {
        openContainer = null;
      }
    }

    return open;
  };

  // Add listener to toolbar to close menus if open only if toolbar is the
  // target
  toolbar.addEventListener('click', (event) => {
    if (event.target === toolbar) {
      toggleToolsContainer(event);
    }
  });

  /**
   * Create context for the given tool
   *
   * @param tool Tool to create a context for
   */
  const createToolContext = (tool: string): void => {
    toolContexts[tool] = {};
    Object.defineProperty(toolContexts[tool], 'reset', {
      value: () => {
        createToolContext(tool);
      },
      writable: false
    });
  };

  /**
   * Add the additional events to the element for detecting movement
   *
   * TODO Use setPointerCapture instead of this
   *
   * @param event Event that triggered the adding of additional events
   * @param eventAction Event action to add/remove
   * @param remove Whether the eventAction should be removed instead of
   *   added
   */
  const additionalEventActionListeners = (
    triggerEvent: Event,
    eventAction: EventAction,
    remove?: boolean
  ) => {
    const element = triggerEvent.currentTarget;
    let events;

    if (triggerEvent.type.startsWith('mouse')) {
      events = ['mousemove', 'mouseup'];
    } else if (triggerEvent.type.startsWith('touch')) {
      events = ['touchmove', 'touchend'];
    }

    if (remove) {
      events.forEach((type) => {
        doRemoveListener(type, eventAction.mapping, element);
      });
    } else {
      events.forEach((type) => {
        doAddListener(type, eventAction, element);
      });
    }
  };

  /**
   * Set option(s) for a given tool
   *
   * @param tool Tool to set the option for
   * @param option Option name or Object of object values to set
   */
  const setOptions = (
    tool: string,
    option: string | { [key: string]: any },
    value?: any
  ) => {
    const form = toolElements[tool] && toolElements[tool].form;
    if (form) {
      if (typeof option === 'object') {
        Object.assign(toolOptions[tool], option);
        Object.entries(option).forEach(([key, value]) => {
          form.setValue(key, value);
        });
      } else {
        toolOptions[tool][option] = value;
        form.setValue(toolOptions[tool][option], value);
      }
    }
  };

  const runHandler = (tool: string, action: string, event: any) => {
    const handler = getDeep(enabledTools, [tool, 'actions', action, 'handler']);
    if (handler) {
      handler(makeInstance(tool, lastDrawing), event, toolContexts[tool]);
      return true;
    }

    return false;
  };

  /**
   * Make the drawing instance to be given to tool actions
   *
   * @param tool The tool name that the instance will be passed to
   */
  const makeInstance = (tool: string, drawing) => ({
    app,
    options: toolOptions[tool],
    setOptions: (option: string | { [key: string]: any }, value?: any) =>
      setOptions(tool, option, value),
    drawing,
    setCursor: (cursor?: string) => {
      if (cursor) {
        drawing.setStyle('cursor', cursor);
      } else {
        drawing.setStyle(
          'cursor',
          (activeTool && enabledTools[activeTool].cursor) || ''
        );
      }
    },
    changeTool,
    color: app.config.color
  });

  /**
   * Event handler that is used to handle all events
   *
   * @param event Event to handle
   */
  // TODO With document events. Need to only action event if correct drawing
  // Maybe add target drawing to EventAction?
  // TODO Move to main Drawing class
  const eventHandler = (
    event: Event,
    ignoreEventActions?: EventAction[]
  ): boolean => {
    let handled = false;
    let allowDefault = false;

    // Don't handle document events if a container is open
    if (openContainer && documentEvents.indexOf(event.type) !== -1) {
      return;
    }

    if (eventActions[event.type]) {
      // Create an ordered list of handlers to try based on element, filter,
      // filter score (the more matches, the higher priority) and order
      let filteredActions = eventActions[event.type].reduce(
        (acc, elementAction: ElementAction, position: number) => {
          // Ignore if ignored eventAction
          if (
            ignoreEventActions &&
            ignoreEventActions.indexOf(elementAction.eventAction) !== -1
          ) {
            return acc;
          }

          // Ignore if wrong target element
          if (!elementAction || elementAction.element !== event.currentTarget) {
            return acc;
          }

          const score = elementAction.eventAction.mapping.filter
            ? eventFilterScore(event, elementAction.eventAction.mapping.filter)
            : 0;

          if (score === -1) {
            return acc;
          }

          acc.push({
            position,
            score,
            elementAction
          });

          return acc;
        },
        []
      );

      // Sort
      filteredActions = filteredActions.sort((a, b) => {
        if (a.score && b.score) {
          return b.score - a.score;
        }
        if (a.score) {
          return -1;
        }

        if (b.score) {
          return 1;
        }

        return a.position - b.position;
      });

      filteredActions.find(({ elementAction }) => {
        const eventAction = elementAction.eventAction;
        const element = elementAction.element;

        // TODO Need a better way to do this
        let drawing;
        if (element.canvas) {
          drawing = element;
        } else {
          drawing = drawings[0];
        }

        // Add parameters
        if (eventAction.mapping.parameters) {
          event.parameters = {};
          Object.entries(eventAction.mapping.parameters).forEach(
            ([parameter, value]) => {
              if (typeof value === 'function') {
                event.parameters[parameter] = value(event);
              } else if (typeof event.originalEvent[value] !== 'undefined') {
                event.parameters[parameter] = event.originalEvent[value];
              }
            }
          );
        }

        if (
          eventAction.movement === null &&
          ['mousedown', 'touchstart'].indexOf(event.type) !== -1
        ) {
          if (eventAction.eventsCache.length) {
            // TODO Do something with cached events?
          }

          let mainEventAction = eventAction;
          while (mainEventAction.parentEventAction) {
            mainEventAction = mainEventAction.parentEventAction;
          }

          // Reset eventAction
          eventAction.eventsCache = [
            {
              event,
              ignoreEventActions
            }
          ];
          eventAction.movement = null;
          eventAction.startPixels = event.middlePixels;
          eventAction.startCoordinates = event.middleCoordinates;

          // Add listeners for move and up/end
          if (event.type.startsWith('mouse')) {
            ['mousemove', 'mouseup'].forEach((type) => {
              doAddListener(type, eventAction, element, true);
            });
          } else if (event.type.startsWith('touch')) {
            ['touchmove', 'touchend'].forEach((type) => {
              doAddListener(type, eventAction, element, true);
            });
          }
          handled = true;
        } else if (
          eventAction.neededMovement !== null &&
          eventAction.movement === null
        ) {
          // Check movement
          // Check for movement
          let threshold;
          if (event.type.startsWith('touch')) {
            threshold = touchMoveThreshold;
          } else if (event.type.startsWith('mouse')) {
            threshold = mouseMoveThreshold;
          }

          // Calculate movement based on event coordinates
          const movement = Math.sqrt(
            Math.pow(event.middlePixels.x - eventAction.startPixels.x, 2) +
              Math.pow(event.middlePixels.y - eventAction.startPixels.y, 2)
          );

          if (movement > threshold) {
            eventAction.movement = true;
          }

          if (['mousemove', 'touchmove'].indexOf(event.type) !== -1) {
            if (eventAction.movement) {
              // Remove listeners (real ones will be added if keeping events)
              additionalEventActionListeners(event, eventAction, true);

              // If detected movement when we don't want it, remove and clear
              if (!eventAction.neededMovement) {
                // Reset movement on current eventAction
                eventAction.movement = null;
                // Replay the events so another handler can handle them
                eventAction.eventsCache.find(
                  (cachedEvent) =>
                    !eventHandler(cachedEvent.event, [
                      ...(cachedEvent.ignoreEventActions || []),
                      ...(ignoreEventActions || []),
                      eventAction
                    ])
                );
                eventAction.eventsCache = [];
              } else {
                // Add current event to cache so will be replayed
                eventAction.eventsCache.push({ event });

                // Replay the events
                eventAction.eventsCache.forEach((cachedEvent) => {
                  eventHandler(
                    cachedEvent.event,
                    cachedEvent.ignoreEventActions
                  );
                });

                // Remove the events in the cache
                eventAction.eventsCache = [];

                // TODO Do some other way
                const finishEvent =
                  event.type === 'mousemove' ? 'mouseup' : 'touchend';
                const endHandler = () => {
                  eventAction.movement = null;
                  drawing.removeEventListener(finishEvent, endHandler);
                };
                drawing.addEventListener(finishEvent, endHandler);
              }
            }
          } else if (['mouseup', 'touchend'].indexOf(event.type) !== -1) {
            if (!eventAction.movement) {
              eventAction.movement = false;

              if (eventAction.mapping.type === 'click') {
                // Replay first event
                runAction(
                  drawing,
                  eventAction.eventsCache[0].event,
                  eventAction
                );

                // Clear the previous events
                eventAction.eventsCache = [];

                eventAction.movement = null;
              } else if (eventAction.neededMovement === false) {
                // Add current event to cache so will be replayed
                eventAction.eventsCache.push({ event });

                // Replay the events
                eventAction.eventsCache.forEach((cachedEvent) => {
                  eventHandler(
                    cachedEvent.event,
                    cachedEvent.ignoreEventActions
                  );
                });

                // Remove the events in the cache
                eventAction.eventsCache = [];
              } else {
                // Add current event to cache so will be replayed
                eventAction.eventsCache.push({ event });

                // Reset movement on current eventAction
                eventAction.movement = null;

                // Replay the events so another handler can handle them
                // as long as they are handled
                eventAction.eventsCache.find(
                  (cachedEvent) =>
                    !eventHandler(cachedEvent.event, [
                      ...(ignoreEventActions || []),
                      eventAction
                    ])
                );

                // Ignore and clear the events
                eventAction.eventsCache = [];
              }

              // Remove listeners (real ones will be added if keeping events)
              additionalEventActionListeners(event, eventAction, true);

              eventAction.movement = null;
            } else {
              console.warn('TODO got movement unexpectedly');
            }
          }
          handled = true;
        } else {
          // Run action
          allowDefault = runAction(drawing, event, eventAction);
          handled = true;
        }
        return handled;
      });

      if (handled && !allowDefault) {
        event.preventDefault();
      }
    }

    return handled;
  };

  /**
   * Run the action for the given event and eventAction
   */
  const runAction = (drawing, event, eventAction: EventAction) => {
    if (eventAction.mapping.action === 'activate') {
      changeTool(eventAction.tool);
      return false;
    }

    const action =
      eventAction.mapping.action &&
      getDeep(enabledTools, [
        eventAction.tool,
        'actions',
        eventAction.mapping.action
      ]);

    const parentEventAction = eventAction.parentEventAction || eventAction;
    let mainEventAction = parentEventAction;
    while (mainEventAction.parentEventAction) {
      mainEventAction = mainEventAction.parentEventAction;
    }

    let inChain = null;
    if (action && action.handler) {
      inChain =
        action.handler(
          makeInstance(eventAction.tool, drawing),
          event,
          toolContexts[eventAction.tool]
        ) || false;
    } else if (eventAction.mapping.endChain) {
      inChain = false;
    }

    // Create the chain even handlers if we have a context
    if (
      inChain !== false &&
      !eventAction.chainEventActions &&
      eventAction.mapping.chainActions
    ) {
      eventAction.mapping.chainActions.forEach((chainAction) => {
        addMappingListener(
          mainEventAction.tool,
          chainAction,
          drawing,
          eventAction
        );
      });
      eventAction.chainEventActions = true;
    } else if (!inChain && parentEventAction.chainEventActions) {
      parentEventAction.mapping.chainActions.forEach((chainAction) => {
        removeMappingListener(mainEventAction.tool, chainAction, drawing);
      });
      parentEventAction.chainEventActions = false;
      if (parentEventAction === mainEventAction) {
        toolContexts[eventAction.tool].reset();
      }
    }

    return (
      action &&
      (typeof action.allowDefault === 'function'
        ? action.allowDefault(drawing, event, toolContexts[eventAction.tool])
        : action.allowDefault)
    );
  };

  const dragSetups = [];

  /**
   * Setup the element to be a drop target
   *
   * @param element Element to set up as a drop target
   */
  const setupDrop = (element: Element) => {
    {
      const index = dragSetups.findIndex((setup) => setup.element === element);
      if (index !== -1) {
        return;
      }
    }

    let gotHandler: boolean = false;

    const clearDropInfo = () => {
      dropInfo.innerHTML = '';
      dropTarget.hidden = true;
    };

    const setup = {
      dragOver: (event: DragEvent) => {
        if (gotHandler) {
          event.preventDefault();
        }
      },
      dragEnd: (event: DragEvent) => {
        clearDropInfo();
        gotHandler = false;
      },
      dragStart: (event: DragEvent) => {
        if (!eventActions['drop']) {
          return;
        }

        const elementActions = eventActions['drop'].filter(
          (elementAction) => elementAction.element === element
        );

        const types = elementActions.reduce((acc, elementAction) => {
          const type = getDeep(elementAction.eventAction.mapping, [
            'filter',
            'type'
          ]);
          if (Array.isArray(type)) {
            type.forEach((item) => {
              if (acc.indexOf(item) === -1) {
                acc.push(item);
              }
            });
          } else if (type) {
            if (acc.indexOf(type) === -1) {
              acc.push(type);
            }
          }

          return acc;
        }, []);

        let images = 0;
        let pdfs = 0;
        for (let i = 0; i < event.dataTransfer.items.length; i++) {
          const item = event.dataTransfer.items[i];
          if (item.kind === 'file') {
            if (item.type.match(/^image\/[a-z]+/)) {
              images++;
            }
            if (item.type === 'application/pdf') {
              pdfs++;
            }
          }
        }

        if ((images || pdfs) && types.indexOf('image') !== -1) {
          gotHandler = true;
          dropInfo.innerHTML =
            '<h1>Drop images here to insert in draft</h1><p>' +
            (images ? `${images} image${images > 1 ? 's' : ''}` : '') +
            (images && pdfs ? ' & ' : '') +
            (pdfs ? `${pdfs} PDF${pdfs > 1 ? 's' : ''}` : '') +
            '</p>';
          dropTarget.hidden = false;

          gotHandler = true;
          event.preventDefault();
        }
      },
      drop: (event: DragEvent) => {
        if (!eventActions['drop']) {
          return;
        }

        // TODO Need a better way to do this
        let drawing;
        if (element.canvas) {
          drawing = element;
        } else {
          drawing = drawings[0];
        }

        const elementActions = eventActions['drop'].filter(
          (elementAction) => elementAction.element === element
        );

        let handled = false;

        elementActions.forEach((elementAction) => {
          const actionName = elementAction.eventAction.mapping.action;
          const action = getDeep(enabledTools, [
            elementAction.eventAction.tool,
            'actions',
            actionName,
            'handler'
          ]);

          if (action) {
            action(
              makeInstance(elementAction.eventAction.tool, drawing),
              event
            );
            handled = true;
          }
        });

        if (handled) {
          event.preventDefault();
        }

        clearDropInfo();
      }
    };

    element.addEventListener('dragenter', setup.dragStart, true);
    element.addEventListener('dragleave', setup.dragEnd, true);
    element.addEventListener('dragend', setup.dragEnd, true);
    element.addEventListener('dragover', setup.dragOver, true);
    element.addEventListener('drop', setup.drop, true);

    dragSetups.push({
      element,
      setup
    });
  };

  /**
   * Teardown the setup for being a drop target on the element
   *
   * @param element Element to remove the drop target setup from
   */
  const teardownDrop = (element: Element) => {
    const index = dragSetups.findIndex((setup) => setup.element === element);
    if (!index) {
      return;
    }

    const setup = dragSetups[index].setup;

    element.removeEventListener('dragenter', setup.dragStart);
    element.removeEventListener('dragleave', setup.dragEnd);
    element.removeEventListener('dragend', setup.dragEnd);
    element.removeEventListener('dragover', setup.dragOver);
    element.removeEventListener('drop', setup.drop);

    dragSetups.splice(index, 1);
  };

  /**
   * Add handlers for a given mapping to the given elements (drawings)
   *
   * @param tool Tool the mapping is from
   * @param mapping Mapping to add the handlers for
   * @param elements Elements to add the handlers to,  or null to add the
   *   handlers to all drawings
   * @param parentEventAction Parent eventAction to mapping being added
   * @param priority Whether the mapping should be added as a priority or not
   */
  const addMappingListener = (
    tool: string,
    mapping,
    elements?,
    parentEventAction?,
    priority?: boolean
  ) => {
    if (documentEvents.indexOf(mapping.type) !== -1) {
      elements = [document];
    } else if (!elements) {
      elements = drawings;
    } else if (!Array.isArray(elements)) {
      elements = [elements];
    }

    elements.forEach((element) => {
      // If have a movement dependent event, add Listener to mousedown and
      // touchstart to detect (the lack of) movement before adding for the real
      // handler
      if (mappingNeedsMovement(mapping)) {
        const neededMovement =
          mapping.type === 'click' ? false : mapping.filter.movement;
        const action: EventAction = {
          tool,
          mapping,
          parentEventAction,
          eventsCache: [],
          startCoordinates: null,
          neededMovement,
          movement: null,
          context: {}
        };
        ['mousedown', 'touchstart'].forEach((event) => {
          doAddListener(
            event,
            action,
            element,
            priority || Boolean(parentEventAction)
          );
        });
      } else {
        const action: EventAction = {
          tool,
          mapping,
          parentEventAction,
          startCoordinates: null,
          context: {}
        };
        doAddListener(
          mapping.type,
          action,
          element,
          priority || Boolean(parentEventAction)
        );
      }
    });
  };

  /**
   * Called by addMappingListener and eventHandler to do the actual adding of
   * the eventAction and handler to the eventActions and the document
   *
   * @param event Event type
   * @param action EventAction to add
   */
  const doAddListener = (
    event: string,
    action: EventAction,
    element,
    priority?: boolean
  ) => {
    let handler;
    let setup;
    if (event === 'drop') {
      setup = setupDrop;
    } else {
      handler = eventHandler;
    }

    if (!eventActions[event]) {
      // TODO Is there a benefit to attach to document? Will mean checking
      // that it is the targetted canvas harder
      if (setup) {
        setup(element);
      }
      if (handler) {
        element.addEventListener(event, handler);
      }
      eventActions[event] = [
        {
          element,
          eventAction: action
        }
      ];
    } else {
      // Check to see if there is already an eventAction for this element
      const index = eventActions[event].findIndex(
        (elementAction) => elementAction.element === element
      );

      let elementEventAction;

      if (index === -1) {
        if (setup) {
          setup(element);
        }
        if (handler) {
          element.addEventListener(event, handler);
        }
      } else {
        // Find if already have eventActions for the given element
        elementEventAction = eventActions[event].find(
          (elementAction) =>
            elementAction.element === element &&
            elementAction.eventAction === action
        );
      }

      if (!elementEventAction) {
        if (priority) {
          eventActions[event].unshift({
            element,
            eventAction: action
          });
        } else {
          eventActions[event].push({
            element,
            eventAction: action
          });
        }
      }
    }
  };

  /**
   * Remove a mapping from the given elements (drawings)
   *
   * @param tool Tool the mapping is from
   * @param mapping Mapping to remove the handlers for
   * @param elements Elements to remove the handlers to,  or null to add the
   *   handlers to all drawings
   */
  const removeMappingListener = (
    tool: string,
    mapping,
    elements?,
    removeChain?: boolean
  ) => {
    if (documentEvents.indexOf(mapping.type) !== -1) {
      elements = [document];
    } else if (!elements) {
      elements = drawings;
    } else if (!Array.isArray(elements)) {
      elements = [elements];
    }

    elements.forEach((element) => {
      if (mappingNeedsMovement(mapping)) {
        // Ensure removed from all potential event listeners
        movementCheckEvents.forEach((event) => {
          doRemoveListener(event, mapping, element, removeChain);
        });
      } else {
        doRemoveListener(mapping.type, mapping, element, removeChain);
      }
    });
  };

  /**
   * Called by removeMappingListener to do the actual removing of a
   * mapping and the handler from eventActions and the document
   *
   * @param event Event type to remove the mapping from
   * @param mapping Mapping to remove
   * @param element Element to remove the mapping for
   */
  const doRemoveListener = (
    event: string,
    mapping: Mapping,
    element,
    removeChain?: boolean
  ) => {
    // Find mapping for element
    if (eventActions[event]) {
      let index = eventActions[event].findIndex(
        (elementAction) =>
          elementAction.element === element &&
          elementAction.eventAction.mapping === mapping
      );

      if (index !== -1) {
        let handler, teardown;
        if (event === 'drop') {
          teardown = teardownDrop;
        } else {
          handler = eventHandler;
        }

        // Remove chainActions if added
        if (
          removeChain &&
          eventActions[event][index].eventAction.chainEventActions
        ) {
          const eventAction = eventActions[event][index].eventAction;
          let mainEventAction = eventAction;
          while (mainEventAction.parentEventAction) {
            mainEventAction = mainEventAction.parentEventAction;
          }
          // Call action to stop chain action
          const actionName = eventAction.mapping.stopAction || 'stop';
          const action = getDeep(enabledTools, [
            eventAction.tool,
            'actions',
            actionName,
            'handler'
          ]);

          if (action) {
            action(element, { type: 'stop' }, toolContexts[eventAction.tool]);
          }

          eventAction.mapping.chainActions.forEach((chainAction) => {
            removeMappingListener(eventAction.tool, chainAction, element);
          });

          // Find the new index for mapping as it may have moved
          index = eventActions[event].findIndex(
            (elementAction) =>
              elementAction.element === element &&
              elementAction.eventAction.mapping === mapping
          );
        }
        if (eventActions[event].length === 1) {
          // Remove listener and event actions as will be empty
          if (handler) {
            element.removeEventListener(event, handler);
          }
          if (teardown) {
            teardown(element);
          }
          delete eventActions[event];
        } else {
          eventActions[event].splice(index, 1);

          // Check if there are any other eventActions for this element
          index = eventActions[event].findIndex(
            (elementAction) => elementAction.element === element
          );

          if (index === -1) {
            if (handler) {
              element.removeEventListener(event, handler);
            }
            if (teardown) {
              teardown(element);
            }
          }
        }
      }
    }
  };

  /**
   * Change the currently active tool
   *
   * @param newTool Id of tool to change to
   *
   * @returns A Promise that resolves with whether the tool was activated
   */
  const changeTool = (newTool: string): Promise<boolean> => {
    if (!enabledTools[newTool]) {
      throw new Error('Unknown tool: ' + newTool);
    }

    if (newTool === activeTool) {
      return;
    }

    // Check if have access to new tool
    if (
      enabledTools[newTool].requiredPermissions &&
      !app.user.hasPermissions(enabledTools[newTool].requiredPermissions)
    ) {
      // Request authentication
      return app.user
        .requestAuthentication(enabledTools[newTool].requiredPermissions)
        .then(
          () => {
            return setupTool(newTool);
          },
          (error) => {
            app.status.createStatus(
              'Tool change ' + (error && (error.message || error)),
              {
                type: 'info'
              }
            );

            return false;
          }
        );
    }

    return setupTool(newTool);
  };

  /**
   * Setup the new active tool once it has been confirmed it should be
   * activated
   *
   * @param newTool New active tool
   */
  const setupTool = (newTool: string): Promise<boolean> => {
    // Ensure given tool exists
    const tool = enabledTools[newTool];
    if (!tool) {
      return;
    }
    const oldTool = activeTool;

    let promise;
    if (tool.activate) {
      promise = tool.activate();
    } else {
      promise = Promise.resolve(true);
    }

    return promise.then((shouldActivate) => {
      if (shouldActivate) {
        activeTool = newTool;
        const cursor = enabledTools[newTool].cursor || '';
        drawings.forEach((drawing) => {
          drawing.canvas.style.cursor = cursor;
        });

        attachListeners(newTool, oldTool);

        if (newTool && toolElements[newTool]) {
          toolElements[newTool].button.setAttribute('aria-selected', true);
        }
        if (oldTool && toolElements[oldTool]) {
          toolElements[oldTool].button.setAttribute('aria-selected', false);
        }

        if (app.config.share && app.config.share.tool) {
          // Emit tool change
          const time = new Date().getTime();

          app.socket.emit(app.config.baseEvent + 'changeTool', {
            draftId: app.draftId,
            drawingId: '0',
            tool: newTool,
            time,
            last: lastToolChangeTime
          } as ToolChangeMessage);

          lastToolChangeTime = time;
        }
      }

      return shouldActivate;
    });
  };

  /**
   * Add event handlers to the drawing and the document required for the
   * tool mappings
   *
   * @param activeTool New active tool
   * @param oldActiveTool Previous active tool
   */
  const attachListeners = (activeTool, oldActiveTool) => {
    if (oldActiveTool && toolMappings[oldActiveTool]) {
      toolMappings[oldActiveTool].forEach((mapping) => {
        if (mapping.active) {
          removeMappingListener(oldActiveTool, mapping, null, true);
        }
      });
    }

    if (activeTool && toolMappings[activeTool]) {
      toolMappings[activeTool].forEach((mapping) => {
        if (mapping.active) {
          addMappingListener(activeTool, mapping, null, null, true);
        }
      });
    }
  };

  /**
   * Emit an event to any attached listeners on this module
   *
   * @param event Type of event to emit
   * @param data Data for event
   */
  const emit = (event: string, data: { [key: string]: any } = {}) => {
    if (outsideListeners[event] && outsideListeners[event].length) {
      outsideListeners[event].forEach((listener) => {
        listener(data);
      });
    }
  };

  /**
   * Generate the activate button for the given tool
   *
   * @param id The ID of the tool
   * @param tool The tool
   *
   * @returns The generated HTML element
   */
  const generateToolButton = (id, tool) => {
    if (!tool.icon) {
      return;
    }

    let toolClassName = '';
    if (
      tool.requiredPermissions &&
      !app.user.hasPermissions(tool.requiredPermissions)
    ) {
      toolClassName = 'locked';
    }

    let icon: HTMLElement;

    if (!toolElements[id]) {
      const button = document.createElement('button');
      button.title = tool.description || tool.name;
      icon = document.createElement('i');
      icon.className = `icon-${tool.icon}`;
      button.appendChild(icon);
      button.addEventListener('click', () => {
        toggleToolsContainer(null, false);
        changeTool(id);
      });

      button.setAttribute('aria-selected', id === activeTool);

      if (tool.options) {
        const settings =
          app?.user?.settings?.tools?.[id] ||
          app.draft?.config?.tools?.settings?.[id] ||
          app.config?.tools?.settings?.[id] ||
          null;
        const container = document.createElement('div');
        container.className = 'buttonContainer ' + id;
        const optionsButton = document.createElement('button');
        optionsButton.className = 'options';
        optionsButton.title = `${tool.name} options`;
        optionsButton.addEventListener('click', (event) => {
          const open = toggleToolsContainer(container, null, () => {
            runHandler(id, 'optionsClose', {
              type: 'optionsClose',
              form: formObject
            });
            icon.className = 'icon-caret-down';
          });

          if (open) {
            runHandler(id, 'optionsOpen', {
              type: 'optionsOpen',
              form: formObject
            });
            icon.className = 'icon-caret-up';
          }
          event.preventDefault();
        });
        icon = document.createElement('i');
        icon.className = 'icon-caret-down';
        const form = document.createElement('div');
        form.className = 'toolOptions';
        const elements = Object.entries(tool.options).reduce(
          (acc, [name, option]) => {
            // Map button actions to handler function
            if (option.type !== 'button') {
              acc[name] = { ...option };
              return acc;
            }

            acc[name] = {
              ...option,
              handler: (event) => {
                runHandler(id, option.handler, {
                  type: 'click',
                  name: option.name,
                  option,
                  form: formObject
                });

                event.preventDefault();
              }
            };

            return acc;
          },
          {}
        );
        const formObject = createForm(
          {
            elements,
            input: (option, value, validity) => {
              if (validity === true || validity.valid) {
                toolOptions[id][option] = value;

                runHandler(id, 'optionChange', {
                  type: 'optionChange',
                  option,
                  value,
                  form: formObject
                });
              }
            }
          },
          form,
          settings,
          {
            app
          }
        );
        optionsButton.appendChild(icon);
        container.appendChild(button);
        container.appendChild(optionsButton);
        container.appendChild(form);
        toolElements[id] = {
          container,
          button,
          form: formObject
        };
      } else {
        toolElements[id] = {
          button
        };
      }
    }

    toolElements[id].button.className = toolClassName;

    return toolElements[id];
  };

  /**
   * Generate an action button for a tool
   */
  const generateToolActionButton = (id, tool, button) => {
    const action = getDeep(tool, ['actions', button.action, 'handler']);
    if ((!button.form && !action) || !button.icon) {
      return;
    }

    let toolClassName = '';
    const requiredPermissions = [
      ...(button.requiredPermissions || []),
      ...(tool.requiredPermissions || [])
    ];
    if (
      requiredPermissions.length &&
      !app.user.hasPermissions(requiredPermissions)
    ) {
      toolClassName = 'locked';
    }

    const buttonElement = document.createElement('button');
    buttonElement.title = button.description || button.label;
    if (toolClassName) {
      buttonElement.className = toolClassName;
    }
    const icon = document.createElement('i');
    icon.innerHTML = button.label;
    if (button.icon) {
      icon.className = `icon-${button.icon}`;
    }
    buttonElement.appendChild(icon);
    if (button.action) {
      buttonElement.addEventListener('click', (event) => {
        toggleToolsContainer(null, false);
        if (!button.active || activeTool === id) {
          // TODO active drawing
          action(makeInstance(id, drawings[0]), event);
        }
      });

      return buttonElement;
    } else if (button.form) {
      // Can't be const as needs to be assigned last
      /* eslint-disable-next-line prefer-const */
      let formObject;
      const container = document.createElement('div');
      container.className = 'buttonContainer ' + id;
      container.appendChild(buttonElement);
      const form = document.createElement('div');
      buttonElement.addEventListener('click', (event) => {
        toggleToolsContainer(container);
        event.preventDefault();
      });
      form.className = 'toolOptions';
      const elements = Object.entries(button.form.elements).reduce(
        (acc, [name, element]) => {
          if (
            element.type !== 'button' ||
            typeof element.handler !== 'string'
          ) {
            acc[name] = element;
            return acc;
          }

          acc[name] = {
            ...element,
            handler: (event) => {
              toggleToolsContainer(container, false);

              runHandler(id, element.handler, {
                type: 'click',
                name: element.name,
                element,
                form: formObject
              });

              event.preventDefault();
            }
          };

          return acc;
        },
        {}
      );
      const formConfig = {
        elements,
        title: button.form.title,
        description: button.form.description,
        submitLabel: button.form.submitLabel
      };
      if (button.form.action) {
        const action = getDeep(tool, [
          'actions',
          button.form.action,
          'handler'
        ]);
        if (action) {
          formConfig.handler = (data, validity, formObject) => {
            action(
              makeInstance(id, drawings[0]),
              {
                type: 'submit',
                data
              },
              toolContexts[id]
            );
            formObject.reset();
            toggleToolsContainer(container, false);
          };
        }
      }
      if (button.form.input) {
        const action = getDeep(tool, ['actions', button.form.input, 'handler']);
        if (action) {
          formConfig.input = (name, value, validity) => {
            action(
              makeInstance(id, drawings[0]),
              {
                type: 'input',
                name,
                value,
                validity
              },
              toolContexts[id]
            );
            container.classList.remove('optionsOpen');
          };
        }
      }

      formObject = createForm(formConfig, form);
      container.appendChild(form);

      return container;
    }
  };

  const generateSettings = () => {
    const container = document.createElement('div');
    container.className = 'buttonContainer';

    const buttonElement = document.createElement('button');
    buttonElement.title = 'Draft Settings';
    if (!app.user.hasPermissions('edit')) {
      buttonElement.className = 'locked';
    }
    const icon = document.createElement('i');
    icon.innerHTML = 'Draft Settings';
    icon.className = `icon-settings`;
    buttonElement.appendChild(icon);

    container.appendChild(buttonElement);
    const form = document.createElement('div');
    buttonElement.addEventListener('click', (event) => {
      // TODO Get required permission from config?
      const hasPermission = app.user.hasPermissions('edit');
      if (!hasPermission) {
        // Request authentication
        app.user.requestAuthentication(['edit']).catch((message) => {
          app.status.createStatus('Draft settings change ' + message, {
            type: 'info'
          });
        });
      }

      if (containerIsOpen(container) || hasPermission) {
        toggleToolsContainer(container);
      }
      event.preventDefault();
    });
    form.className = 'toolOptions';
    const formConfig = {
      title: 'Draft Settings',
      schema: draftSettingsSchema,
      elements: {
        name: undefined,
        editPassword: null
      },
      buttons: [
        {
          label: 'Save',
          handler: (data, validity, formObject) => {
            if (formObject.valid) {
              // Create the new settings object
              let settings = app.draft.config ? { ...app.draft.config } : {};

              let promise = null;

              // Check for changes in the password
              if (
                app.config.draftEditPasswords &&
                (data.newPassword || data.removePassword)
              ) {
                if (app.draft.config && app.draft.config.editPassword) {
                  // Validate the old password is correct
                  promise = app.user.checkPassword(
                    data.currentPassword,
                    app.draftId
                  );
                }
              }

              if (promise === null) {
                promise = Promise.resolve();
              }

              promise.then((checkResult?: boolean) => {
                if (checkResult === false) {
                  formObject.setValue('currentPassword', null);
                  formObject.setMessage(
                    'Incorrect current password. Please try again',
                    'error'
                  );
                  return;
                }

                if (app.config.draftEditPasswords) {
                  if (data.newPassword) {
                    // Create hash of new password
                    settings.editPassword = getSha(data.newPassword);
                  } else if (data.removePassword) {
                    settings.editPassword = null;
                  }
                }

                Object.entries(draftSettingsSchema.properties).forEach(
                  ([key, schema]) => {
                    if (key === 'editPassword') {
                      return;
                    }

                    if (
                      typeof data[key] !== 'undefined' &&
                      data[key] !== null
                    ) {
                      settings[key] = data[key];
                    } else if (typeof settings[key] !== 'undefined') {
                      delete settings[key];
                    }
                  }
                );

                emit('draftUpdate', {
                  config: settings
                });

                if (app.config.draftEditPasswords) {
                  // Store password
                  if (data.newPassword) {
                    app.user.storePassword(data.newPassword, app.draftId, {
                      check: true,
                      remember: data.rememberPassword || false
                    });
                  } else if (data.removePassword) {
                    app.user.storePassword(null, app.draftId, {
                      check: false
                    });
                  }
                  if (settings.editPassword) {
                    settings = {
                      ...settings,
                      editPassword: true
                    };
                  }
                }

                formObject.reset(settings);
                toggleToolsContainer(container, false);
                app.status.createStatus('Draft settings saved', {
                  timeout: true,
                  type: 'success'
                });
              });
            } else {
              formObject.showErrors();
            }
          },
          type: 'submit'
        }
      ]
    };
    if (app.config.draftEditPasswords) {
      Object.assign(formConfig.elements, {
        newPassword: {
          type: 'string',
          format: 'password',
          title: 'New edit password',
          attributes: {
            hidden: (values) => values.removePassword,
            autocomplete: 'off'
          }
        },
        confirmPassword: {
          type: 'string',
          format: 'password',
          title: 'Confirm new edit password',
          validation: (value, values) => {
            if (values.newPassword) {
              if (value !== values.newPassword) {
                return 'Passwords do not match';
              }
            }

            return null;
          },
          attributes: {
            hidden: (values) => !values.newPassword,
            autocomplete: 'off'
          }
        },
        rememberPassword: {
          type: 'boolean',
          format: 'password',
          title: 'Remember new password',
          attributes: {
            hidden: (values) => !values.newPassword
          }
        },
        removePassword: {
          type: 'boolean',
          title: 'Remove current edit password',
          attributes: {
            hidden: (values) => !values.editPassword || values.newPassword
          }
        },
        currentPassword: {
          type: 'string',
          format: 'password',
          title: 'Current edit password',
          attributes: {
            hidden: (values) =>
              !values.editPassword ||
              !(values.removePassword || values.newPassword),
            required: (values) =>
              (values.editPassword &&
                (values.removePassword || values.newPassword)) ||
              null
          }
        }
      });
    }
    createForm(formConfig, form, app.draft.config || {});
    container.appendChild(form);

    return container;
  };

  /**
   * Use the server, draft and user configs to determine what tools should
   * be active and what mappings should be used
   */
  const computeToolsAndMappings = () => {
    if (
      (app.draft &&
        app.draft.config &&
        app.draft.config.tools &&
        app.draft.config.tools.toolbar === false) ||
      (app.config.tools && app.config.tools.toolbar === false)
    ) {
      showToolbar = false;
    } else {
      showToolbar = true;
    }

    const toolList =
      (app.draft &&
        app.draft.config &&
        app.draft.config.tools &&
        app.draft.config.tools.enabledTools) ||
      (app.config.tools && app.config.tools.enabledTools);

    if (toolList) {
      enabledTools = {};
      toolList.forEach((tool) => {
        if (typeof tool === 'string') {
          if (Object.prototype.hasOwnProperty.call(tools, tool)) {
            enabledTools[tool] = tools[tool];
          }
        }
      });
      draftSettingsEnabled = toolList.indexOf('settings') !== -1;
    } else {
      enabledTools = tools;
      draftSettingsEnabled = true;
    }

    toolMappings = {};

    if (
      !(
        (app.draft &&
          app.draft.config &&
          app.draft.config.tools &&
          app.draft.config.tools.mappings &&
          app.draft.config.tools.keepMappings === false) ||
        (app.config.tools &&
          app.config.tools.mappings &&
          app.config.tools.keepMappings === false)
      )
    ) {
      Object.entries(enabledTools).forEach(([id, tool]) => {
        if (tool.mappings) {
          toolMappings[id] = tool.mappings;
        }
      });
    }

    const mappings =
      (app.draft &&
        app.draft.config &&
        app.draft.config.tools &&
        app.draft.config.mappings) ||
      (app.config.tools && app.config.tools.mappings);

    if (mappings) {
      mappings.forEach((mapping) => {
        if (!toolMappings[mapping.tool]) {
          toolMappings[mapping.tool] = [];
        }
        toolMappings[mapping.tool].unshift(mapping);
      });
    }
  };

  const redrawToolbar = () => {
    if (!toolbar) {
      return;
    }

    toolbar.innerHTML = '';

    Object.entries(enabledTools).forEach(([id, tool]) => {
      const element = generateToolButton(id, tool);
      if (element) {
        toolbar.appendChild(element.container || element.button);
      }

      if (tool.buttons) {
        tool.buttons.forEach((button) => {
          const buttonEl = generateToolActionButton(id, tool, button);
          if (buttonEl) {
            toolbar.appendChild(buttonEl);
          }
        });
      }
    });

    if (draftSettingsEnabled) {
      toolbar.appendChild(generateSettings());
    }
  };

  /**
   * Set up enabled tools non-active handlers and default tool options
   */
  const setupEnabledTools = () => {
    // Create tool configurations
    toolOptions = {};

    // Add non-active handlers
    Object.entries(enabledTools).forEach(([name, tool]) => {
      if (toolMappings[name]) {
        toolMappings[name].forEach((mapping) => {
          if (!mapping.active) {
            addMappingListener(name, mapping);
          }
        });
      }
      if (tool.options) {
        toolOptions[name] = {};
        const drawingOptions = getDeep(app.config, ['tools', name, options]);

        Object.entries(tool.options).forEach(([key, option]) => {
          if (drawingOptions && typeof drawingOptions[key] !== 'undefined') {
            toolOptions[name][key] = drawingOptions[key];
          } else if (typeof option.default !== 'undefined') {
            toolOptions[name][key] = option.default;
          } else {
            toolOptions[name][key] = null;
          }
        });
      }
    });
  };

  /**
   * Remove enabled tools non-active handlers
   */
  const removeToolNonActiveHandlers = () => {
    Object.entries(toolMappings).forEach(([name, mappings]) => {
      mappings.forEach((mapping) => {
        if (!mapping.active) {
          removeMappingListener(name, mapping);
        }
      });
    });
  };

  /**
   * Start the tools by determining the correct configuration, setting
   * everything up and drawing the toolbar if enabled
   */
  const startTools = () => {
    computeToolsAndMappings();
    setupEnabledTools();

    Object.keys(enabledTools).forEach((tool) => createToolContext(tool));

    if (toolbar && showToolbar) {
      redrawToolbar();
    }
  };

  /**
   * Restart the tools by removing all the non-active handlers and starting
   * tools again
   */
  const restartTools = () => {
    removeToolNonActiveHandlers();
    startTools();
  };

  const instance = {
    /**
     * Change draft
     */
    updateTools: (draft: Draft, force?: boolean) => {
      if (showToolbar || force) {
        redrawToolbar();
      }
    },
    changeTool,
    addEventListener: (event, listener) => {
      if (outsideListeners[event]) {
        outsideListeners[event].push(listener);
      } else {
        outsideListeners[event] = [listener];
      }
    },
    removeEventListener: (event, listener) => {
      if (outsideListeners[event]) {
        const index = outsideListeners[event].indexOf(listener);

        if (index !== -1) {
          outsideListeners[event].splice(index, 1);
        }
      }
    },
    remove: () => {
      // Remove all listeners
      Object.entries(listeners).forEach(([type, listener]) => {
        if (listener.handlers) {
          listener.handlers.forEach((handler) => {
            handler.element.removeEventListener(type, handler.handler);
          });
        }
      });
    }
  };

  startTools();

  // Add user update listener and redraw toolbar when authentication changes
  app.user.addEventListener('authenticate', () => {
    restartTools();
  });

  return instance;
};
