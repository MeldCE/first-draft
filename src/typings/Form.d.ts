import { JSONSchema7 } from 'json-schema';

interface FormData {
  [name: string]: any;
}

export interface Form {
  /// Title of the form
  title?: string;
  /// Instruction string. Will be broken into paragraphs on newlines
  instructions?: string;
  /// Buttons to add to the form. Will replace the standard submit button
  buttons?: Array<{
    label: string;
    type?: 'submit' | 'reset';
    handler?: (form: FormData) => void;
  }>;
  /// Form elements
  elements: {
    [name: string]: {
      title?: string;
      tooltip?: string;
      /// Description string. Will be broken into paragraphs on newlines
      description?: string;
      /// Value type
      type?: 'string' | 'integer' | 'number' | 'file' | 'boolean';
      /// Format of string if type is a string
      format?: 'color' | 'email' | 'url';
      /// Default value. Will be overridden by value given in showForm call
      default?: any;
      /// Function to check if the field is valid. If invalid, the function
      /// should return the reason that it is invalid
      validation: (value: any) => null | string;
      /// If the element is a number type, so a range slider below the input
      range?: boolean;
      attributes?: {
        min?: number;
        max?: number;
        step?: number;
        [key: string]:
          | number
          | string
          | boolean
          | null
          | ((
              values: { [field: string]: any },
              validity: { [field: string]: ValidityState }
            ) => void);
      };
    };
  };
  /// Instead of the elements array, use a schema to generate elements
  schema?: JSONSchema7;
  /// Handler that will be called when form is submitted
  handler?: (form: FormData) => void;
  /// Handler that will be called every time an input is changed
  input?: (name: string, value: any, validity: ValidityState) => void;
}
