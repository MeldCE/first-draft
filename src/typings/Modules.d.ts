import { ClientUser } from './User';
import { Eventer } from '../lib/eventer';

export interface StatusOptions {
  /// Length to display the status for (in ms)
  timeout?: true | number;
  /// Type of status message, eg error, warning etc. Will be added as a
  /// class to the status message
  type?: string;
  /// Promise(s) to use to time the romoval of the status message. Once
  /// all the Promise(s) have resolve, the status will be removed
  promise?: Promise<void> | Array<Promise<void>>;
  /// If true, will hide the status once the promises have resolved. New
  /// Promises can then be added to reshow the status
  hideAfterPromises?: boolean;
  /// Function to call to update the status label
  updateStatus: (promises: StatusPromises) => string;
}

interface StatusObject {
  /**
   * Update the status message text
   *
   * @param newStatus Text for the new status message
   */
  update(newStatus?: string): void;
  /**
   * Return whether the status message is currently shown
   */
  isShown(): boolean;
  /**
   * Hide the status message
   */
  hide(): void;
  /**
   * Add a Promise for the status message to wait for
   *
   * @param promise Promise to add to the status message
   * @param data Data for the Promise. Will be passed to updateStatus whenever
   *   it is called
   */
  addPromise(promise, data?: any): void;
  /**
   * Return a Promise that resolves once all Promises in the status message
   * have been resolved
   */
  onceAllResolved(): void;
  /**
   * Remove the status message
   */
  remove(): void;
}

export interface StatusModule {
  /**
   * Create a new status message
   */
  createStatus(status: string, options: StatusOptions = {}): StatusObject;
}

export interface UserModule {
  addEventListener: Eventer.addEventListener;
  removeEventListener: Eventer.removeEventListener;
  /**
   * Requests that the user authenticates to gain the given permissions or
   * a username
   *
   * @param permissions An arrary of permissions required
   * @param username If true, request the user gives a username
   *
   * @returns A Promise that resolves once the user has authenticated or
   *   rejects if the authentication is cancelled
   */
  requestAuthentication(
    permissions?: string[],
    username?: boolean
  ): Promise<void>;
  /**
   * Check the password is correct for the given type
   *
   * @param password Password to check
   * @param passwordFor Draft ID if the password is for a draft edit password,
   *   true for if the password is for the current user, or falsey for if the
   *   password is for the server edit password
   *
   * @returns A Promise that resolves to whether the password is correct or
   *   rejects is a code was returned from the check
   */
  checkPassword(
    password: string,
    passwordFor?: string | boolean | null
  ): Promise<boolean>;
  /**
   * Verify and store a password if the password is validated
   *
   * @param password Password to store
   * @param passwordFor Either the ID of the draft the password is for, true
   *   when the password is for the user, or falsey when the password is
   *   for the edit password
   *
   * @returns A Promise that resolves to whether the password was stored
   */
  storePassword(
    password: string,
    passwordFor: string | boolean,
    options: StorePasswordOptions
  ): Promise<boolean>;
  /**
   * Check authentication for the user, draft, or server
   *
   * @param draftId Either true to check the authentication of the user,
   *   a draft ID to check the authentication for a draft edit password, or
   *   falsey to check the server edit password
   *
   * @returns A Promise that resolves to the result of the authentication
   *   check
   */
  reauthenticate(draftId?: string | true | null): Promise<boolean>;
  /**
   * Check if the current user has the given permissions
   *
   * @param permissions Permissions to check the user has
   *
   * @returns Whether the user has all the given permissions
   */
  hasPermissions(permissions: string | string[]): boolean;
  /**
   * Get the current user's username
   *
   * @returns The current user's username or null if the user isn't logged in
   */
  username: string | null;
  /**
   * Get the current user object
   *
   * @returns The current User Object or an empty object if there is no user
   *   information
   */
  user: ClientUser | {};
  /**
   * Get the current token
   */
  token: string | null;
}
