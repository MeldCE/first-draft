import { StatusModule, UserModule } from './Modules';
import { AppConfig } from './Config';

export interface App {
  /// Current Draft ID
  draftId: string | null;
  /// Status module
  status: StatusModule;
  /// Current Draft
  draft: Draft | null;
  /// Config
  config: AppConfig;
  /// SocketIO Socket
  socket: socket.Socket;
  /// Usermodule
  user: UserModule | null;
}
