import { Mapping } from './Tool';

interface BaseThumbnailConfig {
  /// Scale under which to show thumbnail
  scale: number;
  /// Target size for the biggest dimension of the thumbnail
  thumbnailBigDimension: number;
}

interface DimensionThumbnailConfig extends BaseThumbnailConfig {
  /// Minimum dimension for which thumbnails should be created for
  minimumImageDimension: number;
  /// Minimum size for which thumbnails should be created for. If given,
  /// minimumImageDimension will be ignored
  minimumImageSize?: number;
}

interface SizeThumbnailConfig extends BaseThumbnailConfig {
  /// Minimum size for which thumbnails should be created for
  minimumImageSize: number;
}

export type ThumbnailConfig = DimensionThumbnailConfig | SizeThumbnailConfig;

export interface ToolsConfig {
  /// Whether or not to show the toolbar. Defaults to true
  toolbar?: false;
  /// List of tools to enable
  enabledTools?: string[];
  /// Keep the default pointer/keyboard mappings if mappings are specified
  keepMappings?: false;
  /// New pointer/keyboard mappings to use
  mappings?: ConfigToolMapping[];
  /// Default settings for the tools
  settings?: {
    [tool: string]: { [key: string]: any };
  };
}

interface ConfigToolMapping extends Mapping {
  // ID of tool mapping is for
  tool: string;
}

interface BaseConfig {
  /// Base URI for the first-draft instance
  baseUri: string;
  /// Base URI added append to the baseUri for accessing drafts
  draftUri: string;
  /// Base event string for first-draft events
  baseEvent: string;
  /// Create a draft for the accessed id if doesn't already exist
  create: boolean;
  /// Enabled per-draft edit passwords
  draftEditPasswords: boolean;
  /// Default color for the pen and text
  color: string;
  /// DPI to use when import PDFs
  pdfDpi: number;
  /// Maximum number of images/PDF pages to put in a row
  maxImageRow: number;
  /// Image spacing when placing images in rows
  imageSpacing: number;
  /// Default minimum zoom level
  minZoomLevel?: number;
  /// Configuration for generating thumbnails for large images
  thumbnails?: Array<ThumbnailConfig>;
  /// Whether or not to preload all the thumbnail images
  preloadThumbnails?: boolean;
  /// Whether or not to preload all the full-size images
  preloadOriginal?: boolean;
  /// Banner to show when no status messages are showing
  banner?: string;
  /// Whether or not to update the url with the current view
  viewUrl?: boolean;
  /// Whether or not to always replace the initial view url
  alwaysReplaceUrl?: boolean;
  /// Data to share between users
  share?: {
    cursor?: boolean;
    tool?: boolean;
    view?: boolean;
  };
}

interface Permission {
  type: 'user' | 'group';
  permissions: Array<string>;
}

export interface BaseCommonConfig {
  /// Tools configuration
  tools?: ToolsConfig;
  permissions?: Array<Permission>;
  /// Require a password to edit the draft. Should be a string on the app
  /// when receiving configs
  editPassword?: string | true;
}

export interface CommonConfig extends BaseCommonConfig {
  editPassword?: string;
  editPrivileges?: string[];
  cookie?: {
    id: string;
    lifetime?: number;
  };
}

export interface ClientCommonConfig extends BaseCommonConfig {
  editPassword?: true;
}

export interface Config extends BaseConfig, CommonConfig {}

export interface AppConfig extends BaseConfig {
  editPassword?: boolean;
}
