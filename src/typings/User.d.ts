import { ToolsConfig } from './Config';

export interface ClientUser {
  user?: string;
  name?: string;
  tools?: ToolsConfig;
}

export interface User extends ClientUser {
  hash: string;
  privileges?: Array<string>;
  groups?: Array<string>;
}

export interface UserConfig {
  users?: { [username: string]: User };
}
