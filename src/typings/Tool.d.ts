interface DrawingInstance {
  app: any; // TODO App;
  options: { [option: string]: any };
  setOptions: (option: string | { [key: string]: any }, value?: any) => void;
  drawing: any; //TODO Drawing;
  changeTool: (tool: string) => Promise<boolean>;
  color: string;
}

/**
 * Event Handler
 *
 * @param instance Drawing instance
 * @param event Event that needs to be handled
 * @param context Tool context
 */
type EventHandler<Context> = (
  instance: DrawingInstance,
  event: Event,
  context?: Context
) => boolean;

export interface ToolAction<Context> {
  /// Handler that should return a boolean of whether the event was handled or
  /// not
  handler?: EventHandler<Context>;
  /// Whether or not to, if the handler handles the event, allow the browser
  /// default handler to run. Can be a boolean, or a function that returns
  /// a boolean
  allowDefault?: boolean | EventHandler<Context>;
}

export interface Mapping {
  /// Event type mapping should be called for
  type: string;
  /// Action to run on event
  action: string;
  /// Filter to match the event to before running the action for
  filter?: {
    /// Type
    type?: string;
    /// The key pressed
    key?: string;
    /// Mouse button pressed
    button?: number;
  };
}

export interface Tool<Context> {
  /// Name of tool
  name: string;
  /// Description of tool
  description: string;
  /// Icon to use on tool button
  icon?: string;
  /// Permissions required to use all tool actions
  requiredPermissions?: Array<string>;
  /// Function to call when tool is activated
  activate?(): Promise<boolean>;
  /// Default mappings of user interactions to tool actions
  mappings?: Array<Mapping>;
  /// Actions of tool
  actions: {
    [name: string]: ToolAction<Context>;
  };
}
