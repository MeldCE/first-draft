import { Draft } from './Draft';
import { DraftMessage, DraftUpdateMessage } from './Message';

export interface Instance {
  close(): void;
  read(): Promise<Draft>;
  save(data: Draft): Promise<Draft>;
  update(data: DraftUpdateMessage): Promise<Draft>;
  modify(data: DraftMessage): Promise<Draft>;
}

export interface Storage {
  haveDraft(id: string): Promise<boolean>;
  loadDraft(id: string): Promise<Instance>;
  draftHasFile(id: string, filename: string): Promise<boolean>;
  storeFile(
    id: string,
    filename: string,
    contents: any,
    overwrite?: boolean
  ): Promise<void>;
  getFile(id: string, filename: string): Promise<Buffer>;
}
