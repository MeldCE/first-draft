import { Point } from 'paper';
import { CommonConfig, Permission } from './Config';

type Matrix = [number, number, number, number, number, number];

type Segment = [[number, number], [number, number], [number, number], number];

export interface BaseElement {
  name?: string;
  data?: {
    label?: string;
    permission?: Array<Permission>;
  };
}

/// Data used in the construction of rows of objects
export interface RowData {
  /// Start position of the rows
  position: Point;
  /// Maximum row width
  maximumRowWidth: number;
  /// Current row height
  currentRowHeight: number;
  /// Next position
  nextPosition: Point;
  /// Next offset on row
  nextOffset: number;
}

interface BasePositionalElement extends BaseElement {
  type: string;
  matrix?: Matrix;
}

interface RasterElement extends BasePositionalElement {
  type: 'Raster';
  source: string;
}

interface PathElement extends BasePositionalElement {
  type: 'Path';
  segments: Array<Segment>;
}

export interface Layer extends BaseElement {
  children: Array<DraftElement>;
}

export type DraftElement = RasterElement | PathElement;

export interface Drawing {
  layers: Array<Layer>;
}

export type DraftConfig = CommonConfig
export type ClientDraftConfig = CommonConfig

export interface DraftUpdate {
  name?: string;
  description?: string;
  created?: number;
  createdBy?: string;
  config?: DraftConfig;
}

export interface ClientDraftUpdate extends DraftUpdate {
  config?: ClientDraftConfig;
}

export interface Draft extends DraftUpdate {
  drawings: Array<Drawing>;
  version: number;
}

export interface ClientDraft extends ClientDraftUpdate {
  drawings: Array<Drawing>;
  version: number;
}
