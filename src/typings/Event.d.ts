export type Handler = (event: DrawingEvent) => void;

export interface DrawingEvent {
  type: string;
  originalEvent: Event;
  preventDefault: Event["preventDefault"];
  defaultPrevented: boolean;
  stopPropagation: Event["stopPropagation"];
  /// Middle drawing coordinates for the event
  middleCoordinates?: [number, number];
  /// Middle pixels, relative to the top-left of the drawing element, for the
  /// event
  middlePixels?: [number, number];
  coordinates?: { [identifier: string]: [number, number] };
}
