import * as Draft from './Draft';

export interface BaseMessage {
  id: string;
  [key: string]: any;
}

export interface ErrorMessage extends BaseMessage {
  error: {
    code?: number;
    message: string;
  };
}

interface BaseDrawMessage extends BaseMessage {
  action: string;
  user?: any;
}

export interface DraftMessage extends BaseMessage {
  draft?: Draft.Draft;
}

export interface DraftUpdateMessage extends BaseMessage {
  data: any;
}

export interface CreateElement extends BaseDrawMessage {
  action: 'create';
  element: Draft.DraftElement;
  layerName: string;
  index?: number;
}

export interface ModifyElement extends BaseDrawMessage {
  action: 'modify';
  element: Draft.DraftElement;
  layerName: string;
}

export interface ModifyMultipleElements extends BaseDrawMessage {
  action: 'ModifyMultiple';
  elements: Array<{
    element: Draft.DraftElement;
    layerName: string;
  }>;
}

export interface PositionElements extends BaseDrawMessage {
  action: 'position';
  elements: Array<{
    elementName: string;
    layerName: string;
    position: [number, number];
  }>;
}

export interface DeleteElement extends BaseDrawMessage {
  action: 'delete';
  elementName: string;
  layerName: string;
}

export interface DeleteMultipleElements extends BaseDrawMessage {
  action: 'deleteMultiple';
  elements: Array<{
    elementName: string;
    layerName: string;
  }>;
}

export interface CreateLayer extends BaseDrawMessage {
  action: 'createLayer';
  layer: Draft.BaseElement;
  index?: number;
}

export interface ModifyLayer extends BaseDrawMessage {
  action: 'modifyLayer';
  layer: Draft.BaseElement;
}

export interface DeleteLayer extends BaseDrawMessage {
  action: 'deleteLayer';
  layerName: string;
}

type DrawMessage =
  | CreateElement
  | ModifyElement
  | ModifyMultipleElements
  | PositionElements
  | DeleteElement
  | DeleteMultipleElements
  | CreateLayer
  | ModifyLayer
  | DeleteLayer;

interface BaseInteractionMessage {
  // Draft ID
  draftId: string;
  // Drawing ID
  drawingId: string;
}

export interface ViewChangeMessage extends BaseInteractionMessage {
  /// Horizontal offset of the top-left of the view from the drawings
  /// origin (0,0) in drawing pixels
  x: number;
  /// Vertical offset of the top-left of the view from the drawings
  /// origin (0,0) in drawing pixels
  y: number;
  /// Width of view in screen pixels
  width: number;
  /// Height of view in screen pixels
  height: number;
  /// Zoom factor of view
  zoom: number;
  /// Time value of change
  time: number;
}

export interface ToolChangeMessage extends BaseInteractionMessage {
  /// Tool id
  tool: string;
  /// Time value of change
  time: number;
  /// Last time value of tool change
  last: number | null;
}

export interface CursorMessage extends BaseInteractionMessage {
  /// Cursor x position in drawing coordinates
  x: number;
  /// Cursor y position in drawing coordinates
  y: number;
}

type InteractionMessage = ViewChangeMessage | ToolChangeMessage | CursorMessage;

export type Message = DrawMessage | InteractionMessage;

export interface AuthChallenge extends BaseMessage {
  challenge?: string;
  username?: string;
}

export interface AuthResult extends BaseMessage {
  success: boolean;
  username?: string;
}
