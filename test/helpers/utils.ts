import { ViewChangeMessage } from '../../src/typings/Message';

export const drafts = {
  simple: require('../drafts/simple.json'),
  big: require('../drafts/big.json'),
  shifted: require('../drafts/shifted.json'),
  textbox: require('../drafts/textbox.json')
};

/**
 * Array find method where the function can be async
 *
 * @param array Array to iterate through until function returns a positive
 *   result
 * @param test Test function
 */
export const asyncFind = async (array, test) => {
  for (let i = 0; i < array.length; i++) {
    if (await test(array[i])) {
      return array[i];
    }
  }
};

/**
 * Return an empty draft when requested for a draft
 */
export const blankDraft = () => {
  window['socketEvents'].addMocker('draft:connect', (data) => {
    window['socketEvents'].emitMock('draft:current', {
      id: data.id,
      draft: null
    });
  });
};

/**
 * Return the given draft when requested by first-draft
 *
 * @param draft Draft to given to first-draft
 */
export const loadDraft = (draft) => {
  window['socketEvents'].addMocker('draft:connect', (data) => {
    window['socketEvents'].emitMock('draft:current', {
      id: data.id,
      draft
    });
  });
};

/**
 * Browses to / or the given url, loads the given draft and waits for the
 * first view event
 *
 * @param draft Draft to load
 * @param url Url to go to
 */
export const loadAndWait = async (
  draft: string | null,
  url?: string
): Promise<ViewChangeMessage> => {
  await browser.url(url || '/');
  if (draft) {
    await browser.execute(loadDraft, drafts[draft]);
  } else {
    await browser.execute(blankDraft);
  }
  return await waitForEvent('draft:view');
};

/**
 * Browses to the given url or /, loads the given draft, and activates the
 * tool with the given title
 *
 * @param draft Draft to load or null to load a blank draft
 * @param title Title of the tool to activate
 * @param url Url to go to
 *
 * @returns The toolbar and the activated toolButton
 */
export const loadAndActivateTool = async (
  draft: string | null,
  title: string,
  url?: string
) => {
  const view = await loadAndWait(draft, url);
  const toolbar = await $('#toolbar');

  expect(toolbar).toExist();
  const toolButton = await asyncFind(
    await toolbar.$$('button'),
    async (button) => (await button.getAttribute('title')) === title
  );
  expect(toolButton).toExist();

  await toolButton.click();

  return { toolbar, toolButton, view };
};

/**
 * Get the tool button element from the toolbar
 *
 * @param title Title of the button to get
 *
 * @returns The tool button
 */
export const getToolButton = async (title: string) => {
  const toolbar = await $('#toolbar');
  if (toolbar) {
    return await asyncFind(
      await toolbar.$$('button'),
      async (button) => (await button.getAttribute('title')) === title
    );
  }
};

/**
 * Get the a coordinate from a list of coordinates in either solid line
 * coordinates (array of arrays) or a curve segments (array of arrays of
 * arrays)
 *
 * @param coordinates Coordinates/segments of path
 * @param index Index of coordinate to return. If negative, will be index
 *   from the end of the array (-1 being the last coordinate)
 *
 * @returns The coordinates [x, y]
 */
export const getCoordinate = (coordinates, index) => {
  if (index < 0) {
    index = coordinates.length + index;
  }

  return Array.isArray(coordinates[index][0])
    ? coordinates[index][0]
    : coordinates[index];
};

/**
 * Get drawing container x and y window offset
 */
export const getContainerShape = async () => {
  const container = await $('#canvasContainer');
  expect(container).toExist();
  const containerLocation = await container.getLocation();
  const size = await container.getSize();
  return {
    x: Math.ceil(containerLocation.x),
    y: Math.ceil(containerLocation.y),
    width: Math.ceil(size.width),
    height: Math.ceil(size.height)
  };
};

interface PanZoom {
  offset: {
    x: number;
    y: number;
  };
  zoom: number;
}

/**
 * Get current drawing zoom and offset
 */
export const getDrawingPanZoom = async () =>
  await browser.execute(
    (): PanZoom => {
      const offset = window['draftInstance'].drawing.offset;

      return {
        offset: { x: offset.x, y: offset.y },
        zoom: window['draftInstance'].drawing.zoom
      };
    }
  );

/**
 * Get currently selected items on draft
 */
export const getSelected = async () =>
  await browser.execute(() =>
    window['draftInstance'].drawing.selected.map((selected) => ({
      type: selected.className,
      name: selected.name,
      data: selected.data
    }))
  );

/**
 * Get socket events from the app
 *
 * @param event Events to get
 *
 * @returns Array of matching events
 */
export const getEvents = async (
  events: string | Array<string> | ((event) => boolean)
) => {
  return await browser.execute((events) => {
    if (!events) {
      return window['socketEvents'];
    } else if (typeof events === 'function') {
      return window['socketEvents'].filter(events);
    } else {
      if (!Array.isArray(events)) {
        events = [events];
      }

      return window['socketEvents'].sent.filter(
        (event) => events.indexOf(event.event) !== -1
      );
    }
  }, events);
};

/**
 * Wait for a particular event to be emitted
 *
 * @param event Event type to wait for
 */
export const waitForEvent = async (event) =>
  await browser.executeAsync(
    (e, done) => window['socketEvents'].waitForEvent(e).then(done) as any,
    event
  );
