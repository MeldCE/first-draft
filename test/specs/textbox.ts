import tool from '../../src/app/tools/textbox';
import {
  asyncFind,
  getContainerShape,
  getEvents,
  getSelected,
  getToolButton,
  loadAndActivateTool,
  loadAndWait
} from '../helpers/utils';

describe('first-draft textbox tool', () => {
  const textboxActions = [
    {
      type: 'pointer',
      id: 'mouse',
      parmeters: {
        pointerType: 'mouse'
      },
      actions: [
        { type: 'pointerMove', duration: 0, x: 50, y: 200 },
        { type: 'pointerDown', button: 0 },
        { type: 'pointerUp', button: 0 },
        { type: 'pause', duration: 200 }
      ]
    },
    {
      type: 'key',
      id: 'keyboard',
      actions: [
        { type: 'keyDown', value: 'h' },
        { type: 'keyUp', value: 'h' },
        { type: 'pause', duration: 200 },
        { type: 'keyDown', value: 'i' },
        { type: 'keyUp', value: 'i' },
        { type: 'pause', duration: 200 }
      ]
    },
    {
      type: 'pointer',
      id: 'mouse',
      parmeters: {
        pointerType: 'mouse'
      },
      actions: [
        { type: 'pointerMove', duration: 0, x: 10, y: 200 },
        { type: 'pointerDown', button: 0 },
        { type: 'pointerUp', button: 0 },
        { type: 'pause', duration: 500 }
      ]
    }
  ];

  it('becomes selected when clicked', async () => {
    await loadAndWait(null);
    const toolbar = await $('#toolbar');
    expect(toolbar).toExist();
    const toolButton = await asyncFind(
      await toolbar.$$('button'),
      async (button) =>
        (await button.getAttribute('title')) === tool.description
    );
    expect(toolButton).toExist();

    await toolButton.click();
    expect(toolButton).toHaveAttribute('aria-selected', 'true');
    const changeEvent = await browser.execute(() =>
      window['socketEvents'].sent.find(
        (event) => event.event === 'draft:changeTool'
      )
    );

    expect(changeEvent).toBeDefined();
    expect(changeEvent.data.tool).toEqual('textbox');
  });

  it('creates a textbox', async () => {
    await loadAndActivateTool(null, tool.description);

    const container = await $('#canvasContainer');
    expect(container).toExist();
    const canvas = await container.$('#canvas');
    expect(canvas).toExist();

    await canvas.click({ button: 0, x: 50, y: 50 });

    let div = await container.$('.editTextbox');
    expect(div).toExist();
    expect(div).toBeFocused();

    await div.setValue('hello');

    await canvas.click({ button: 0, x: 10, y: 50 });

    div = await container.$('.editTextbox');
    expect(div).not.toExist();

    const drawEvents = await getEvents('draft:draw');

    const lastEvent = drawEvents.pop();
    const elementName = lastEvent.data.element.name;

    expect(lastEvent.data.action).toBe('create');
    expect(lastEvent.data.element.type).toBe('Textbox');
    expect(lastEvent.data.active).toBe(false);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.action).toBe('create');
      expect(drawEvents[i].data.active).toBe(true);
      expect(drawEvents[i].data.element.name).toBe(elementName);
      expect(drawEvents[i].data.element.type).toBe('Textbox');
      expect(drawEvents[i].data.element.editTextbox).toBeUndefined();
    }
  });

  it('changes the properties of the selected textbox when options are changed', async () => {
    const { toolbar } = await loadAndActivateTool(
      'textbox',
      'Select/move tool'
    );

    const textboxOptions = await getToolButton('Textbox options');

    const container = await $('#canvasContainer');
    expect(container).toExist();
    const canvas = await container.$('#canvas');
    expect(canvas).toExist();

    const containerShape = await getContainerShape();

    // Can't use click as it the offset is the middle of the canvas
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          {
            type: 'pointerMove',
            duration: 0,
            x: containerShape.x + 60,
            y: containerShape.y + 60
          },
          { type: 'pointerDown', button: 0 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 500 }
        ]
      }
    ];

    await browser.performActions(actions);

    const selected = await getSelected();
    expect(selected.length).toBe(1);
    expect(selected[0].type).toBe('Group');

    // Open the options
    await textboxOptions.click();

    const optionForm = await toolbar.$('.buttonContainer.textbox .toolOptions');

    expect(optionForm).toExist();

    // Change values
    await (await optionForm.$('[name="fontSize"]')).setValue('20');
    await (await optionForm.$('[name="backgroundColor"]')).setValue('#00ff00');
    await (await optionForm.$('[name="color"]')).setValue('#ff0000');
    await (await optionForm.$('[name="padding"]')).setValue('10');
    await (await optionForm.$('[name="backgroundOpacity"]')).setValue('40');

    await textboxOptions.click();

    let drawEvents = await getEvents('draft:draw');

    let lastEvent = drawEvents.pop();

    expect(lastEvent.data.action).toBe('modifyMultiple');
    expect(lastEvent.data.elements.length).toBe(1);
    expect(lastEvent.data.elements[0].element.name).toBe('textbox1');
    expect(lastEvent.data.elements[0].element.padding).toBe(10);
    expect(lastEvent.data.elements[0].element.fontSize).toBe(20);
    expect(lastEvent.data.elements[0].element.background).toBe('#ffffff66');
    expect(lastEvent.data.elements[0].element.color).toBe('red');
    expect(lastEvent.data.elements[0].element.content).toBe('hello');

    // Create a new textbox
    const toolButton = await asyncFind(
      await toolbar.$$('button'),
      async (button) =>
        (await button.getAttribute('title')) === tool.description
    );
    expect(toolButton).toExist();

    await toolButton.click();

    await browser.performActions(textboxActions);

    drawEvents = await getEvents('draft:draw');

    lastEvent = drawEvents.pop();

    expect(lastEvent.data.action).toBe('create');
    expect(lastEvent.data.element.padding).toBe(5);
    expect(lastEvent.data.element.fontSize).toBe(12);
    expect(lastEvent.data.element.background).toBe('#ffffffcc');
    expect(lastEvent.data.element.color).toBe('red');
  });

  it('changes the properties of new text when options are changed with nothing selected', async () => {
    const { toolbar } = await loadAndActivateTool(null, tool.description);

    const textboxOptions = await getToolButton('Textbox options');

    // Open the options
    await textboxOptions.click();

    const optionForm = await toolbar.$('.buttonContainer.textbox .toolOptions');

    expect(optionForm).toExist();

    // Change values
    await (await optionForm.$('[name="fontSize"]')).setValue('20');
    await (await optionForm.$('[name="backgroundColor"]')).setValue('#00ff00');
    await (await optionForm.$('[name="color"]')).setValue('#ff0000');
    await (await optionForm.$('[name="padding"]')).setValue('10');
    await (await optionForm.$('[name="backgroundOpacity"]')).setValue('40');

    await textboxOptions.click();

    await browser.performActions(textboxActions);

    const drawEvents = await getEvents('draft:draw');

    const lastEvent = drawEvents.pop();

    expect(lastEvent.data.action).toBe('create');
    expect(lastEvent.data.element.padding).toBe(10);
    expect(lastEvent.data.element.fontSize).toBe(20);
    expect(lastEvent.data.element.background).toBe('#ffffff66');
    expect(lastEvent.data.element.color).toBe('red');
  });
});
