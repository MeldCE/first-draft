import {
  asyncFind,
  getContainerShape,
  loadAndActivateTool,
  loadAndWait,
  getDrawingPanZoom
} from '../helpers/utils';

const toolTitle = 'Pan/zoom tool';
let containerShape;

describe('first-draft pan tool', () => {
  it('becomes selected when clicked', async () => {
    await loadAndWait('simple');
    const toolbar = await $('#toolbar');
    containerShape = await getContainerShape();

    expect(toolbar).toExist();
    const toolButton = await asyncFind(
      await toolbar.$$('button'),
      async (button) => (await button.getAttribute('title')) === toolTitle
    );
    expect(toolButton).toExist();

    await toolButton.click();
    expect(toolButton).toHaveAttribute('aria-selected', 'true');
    const changeEvent = await browser.execute(() =>
      window['socketEvents'].sent.find(
        (event) => event.event === 'draft:changeTool'
      )
    );

    expect(changeEvent).toBeDefined();
    expect(changeEvent.data.tool).toEqual('panZoom');
  });

  it('pans the draft', async () => {
    const { view: initialView } = await loadAndActivateTool(
      'simple',
      toolTitle
    );

    expect(initialView.x).toBe(0);
    expect(initialView.y).toBe(0);
    expect(initialView.zoom).toBe(1);

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 1000 },
          {
            type: 'pointerMove',
            duration: 1000,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 1000 },
          { type: 'pointerUp', button: 0 },
          {
            type: 'pointerMove',
            duration: 1000,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 1000 }
        ]
      }
    ];

    await browser.performActions(actions);

    const newOffset = await getDrawingPanZoom();

    expect(newOffset.offset.x).toBe(initialView.x - 50);
    expect(newOffset.offset.y).toBe(initialView.y - 50);

    // Check the view has been added to the url
    const url = new URL(await browser.getUrl());

    expect(url.searchParams.get('l')).toBe((-50).toString());
    expect(url.searchParams.get('t')).toBe((-50).toString());
    expect(url.searchParams.get('r')).toBe(
      (containerShape.width - 50).toString()
    );
    expect(url.searchParams.get('b')).toBe(
      (containerShape.height - 50).toString()
    );
    expect(url.searchParams.get('z')).toBe((1).toString());
  });

  // TODO Currently failing in standalone-firefox
  it.skip('pans the draft with middle button and without tool', async () => {
    const initialView = await loadAndWait('simple');

    expect(initialView.x).toBe(0);
    expect(initialView.y).toBe(0);
    expect(initialView.zoom).toBe(1);

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 1 },
          { type: 'pause', duration: 1000 },
          {
            type: 'pointerMove',
            duration: 1000,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 1000 },
          { type: 'pointerUp', button: 1 },
          {
            type: 'pointerMove',
            duration: 1000,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 1000 }
        ]
      }
    ];

    await browser.performActions(actions);

    const newOffset = await getDrawingPanZoom();

    expect(newOffset.offset.x).toBe(initialView.x - 50);
    expect(newOffset.offset.y).toBe(initialView.y - 50);
  });
});
