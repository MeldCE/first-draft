import {
  asyncFind,
  getContainerShape,
  getSelected,
  loadAndActivateTool,
  loadAndWait
} from '../helpers/utils';

let drawingOffset;

describe('first-draft select/move tool', () => {
  it('becomes selected when clicked', async () => {
    await loadAndWait('simple');
    const toolbar = await $('#toolbar');
    drawingOffset = await getContainerShape();

    expect(toolbar).toExist();
    const toolButton = await asyncFind(
      await toolbar.$$('button'),
      async (button) =>
        (await button.getAttribute('title')) === 'Select/move tool'
    );
    expect(toolButton).toExist();

    await toolButton.click();
    expect(toolButton).toHaveAttribute('aria-selected', 'true');
    const changeEvent = await browser.execute(() =>
      window['socketEvents'].sent.find(
        (event) => event.event === 'draft:changeTool'
      )
    );

    expect(changeEvent).toBeDefined();
    expect(changeEvent.data.tool).toEqual('select');
  });

  it('selects an item with a click', async () => {
    await browser.execute(() => window['socketEvents'].clear());
    await browser.execute(() => window['draftInstance'].drawing.deselectAll());

    expect(await getSelected()).toEqual([]);

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          {
            type: 'pointerMove',
            duration: 0,
            x: drawingOffset.x + 40,
            y: drawingOffset.y + 40
          },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 1000 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 1000 }
        ]
      }
    ];

    await browser.performActions(actions);

    const selected = await getSelected();

    expect(selected.length).toBe(1);
    expect(selected[0].type).toBe('Raster');
  });

  it('selects boxed elements only when selected to the right', async () => {
    await browser.execute(() => window['socketEvents'].clear());
    await browser.execute(() => window['draftInstance'].drawing.deselectAll());

    expect(await getSelected()).toEqual([]);

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          {
            type: 'pointerMove',
            duration: 0,
            x: drawingOffset.x + 284,
            y: drawingOffset.y + 10
          },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 1000 },
          {
            type: 'pointerMove',
            duration: 1500,
            x: drawingOffset.x + 416,
            y: drawingOffset.y + 200
          },
          { type: 'pause', duration: 1500 },
          { type: 'pointerUp', button: 0 },
          {
            type: 'pointerMove',
            duration: 1000,
            x: drawingOffset.x + 50,
            y: drawingOffset.y + 50
          },
          { type: 'pause', duration: 1000 }
        ]
      }
    ];

    await browser.performActions(actions);

    const selected = await getSelected();

    expect(selected.length).toBe(2);
    for (let i = 0; i < selected.length; i++) {
      expect(selected[i].type).toBe('Path');
    }
  });

  it('selects any elements within when selected to the left', async () => {
    await browser.execute(() => window['socketEvents'].clear());
    await browser.execute(() => window['draftInstance'].drawing.deselectAll());

    expect(await getSelected()).toEqual([]);

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          {
            type: 'pointerMove',
            duration: 0,
            x: drawingOffset.x + 416,
            y: drawingOffset.y + 10
          },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 1000 },
          {
            type: 'pointerMove',
            duration: 1500,
            x: drawingOffset.x + 284,
            y: drawingOffset.y + 200
          },
          { type: 'pause', duration: 1500 },
          { type: 'pointerUp', button: 0 },
          {
            type: 'pointerMove',
            duration: 1000,
            x: drawingOffset.x + 50,
            y: drawingOffset.y + 50
          },
          { type: 'pause', duration: 1000 }
        ]
      }
    ];

    await browser.performActions(actions);

    const selected = await getSelected();

    expect(selected.length).toBe(4);
    for (let i = 0; i < selected.length; i++) {
      expect(selected[i].type).toBe('Path');
    }
  });

  it('moves selected image', async () => {
    await browser.execute(() => window['socketEvents'].clear());
    await browser.execute(() => window['draftInstance'].drawing.deselectAll());

    expect(await getSelected()).toEqual([]);

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 1000 },
          {
            type: 'pointerMove',
            duration: 1000,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 1000 },
          { type: 'pointerUp', button: 0 },
          {
            type: 'pointerMove',
            duration: 1000,
            x: drawingOffset.x + 50,
            y: drawingOffset.y + 50
          },
          { type: 'pause', duration: 1000 }
        ]
      }
    ];

    await browser.performActions(actions);

    const selected = await getSelected();

    expect(selected.length).toBe(1);
    expect(selected[0].type).toBe('Raster');

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();
    expect(lastEvent.data.elements.length).toBe(1);
    expect(lastEvent.data.elements[0].elementName).toBe('V739vkkoeg');
    expect(lastEvent.data.elements[0].layerName).toBe('images');
    expect(lastEvent.data.elements[0].position).toEqual([188, 188]);
    expect(lastEvent.data.active).toBe(false);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.active).toBe(true);
      expect(drawEvents[i].data.elements[0].elementName).toBe('V739vkkoeg');
      expect(drawEvents[i].data.elements[0].layerName).toBe('images');
    }
  });

  it('moves selected elements', async () => {
    await loadAndActivateTool('simple', 'Select/move tool');

    await browser.execute(() => window['draftInstance'].drawing.selectAll());

    expect((await getSelected()).length).toEqual(5);

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 1000 },
          {
            type: 'pointerMove',
            duration: 1000,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 1000 },
          { type: 'pointerUp', button: 0 },
          {
            type: 'pointerMove',
            duration: 1000,
            x: drawingOffset.x + 50,
            y: drawingOffset.y + 50
          },
          { type: 'pause', duration: 1000 }
        ]
      }
    ];

    await browser.performActions(actions);

    const selected = await getSelected();

    expect(selected.length).toBe(5);

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();

    expect(lastEvent.data.elements.length).toBe(5);
    expect(lastEvent.data.active).toBe(false);
    expect(lastEvent.data.elements[0].position).toEqual([188, 188]);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.elements.length).toBe(5);
      expect(drawEvents[i].data.active).toBe(true);
    }
  });

  it('deselects, then changes to the select tool on Esc', async () => {
    await loadAndActivateTool('simple', 'Pen tool');
    const toolbar = await $('#toolbar');
    expect(toolbar).toExist();

    const selectButton = await asyncFind(
      await toolbar.$$('button'),
      async (button) =>
        (await button.getAttribute('title')) === 'Select/move tool'
    );

    const penButton = await asyncFind(
      await toolbar.$$('button'),
      async (button) => (await button.getAttribute('title')) === 'Pen tool'
    );

    expect(selectButton).toHaveAttribute('aria-selected', 'false');
    expect(penButton).toHaveAttribute('aria-selected', 'true');

    // Select everything
    await browser.execute(() => window['draftInstance'].drawing.selectAll());
    expect((await getSelected()).length).not.toEqual(0);

    const actions = [
      {
        type: 'key',
        id: 'keyboard',
        actions: [
          { type: 'keyDown', value: 'Escape' },
          { type: 'keyUp', value: 'Escape' }
        ]
      }
    ];

    // First Esc should unselect only
    await browser.performActions(actions);

    expect((await getSelected()).length).toEqual(0);
    expect(selectButton).toHaveAttribute('aria-selected', 'false');
    expect(penButton).toHaveAttribute('aria-selected', 'true');

    // Second Esc should change tool
    await browser.performActions(actions);

    expect((await getSelected()).length).toEqual(0);
    expect(selectButton).toHaveAttribute('aria-selected', 'true');
    expect(penButton).toHaveAttribute('aria-selected', 'false');
  });
});
