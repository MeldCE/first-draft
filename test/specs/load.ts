import { blankDraft, loadAndWait } from '../helpers/utils';

describe('redirction', () => {
  before(async () => {
    await browser.url('/');
    await browser.execute(blankDraft);
  });

  it('redirect to a random draft', async () => {
    expect(browser).toHaveUrlContaining('/d/');
  });

  it('requests the draft', async () => {
    const draftId = (await browser.getUrl()).replace(/^.*\/d\/(.*)\//, '$1');
    const connectEvent = await browser.execute(() =>
      window['socketEvents'].sent.find(
        (event) => event.event === 'draft:connect'
      )
    );

    expect(connectEvent).toBeDefined();
    expect(connectEvent.data.name).toEqual(draftId);
    expect(connectEvent.data.getDraft).toEqual(true);
  });
});

describe('loading', () => {
  it('loads at 0,0 1:1 with new drawing', async () => {
    const view = await loadAndWait(null, '/d/test/');
    expect(view.zoom).toBe(1);
    expect(view.x).toBe(0);
    expect(view.y).toBe(0);
  });

  it('loads at 0,0 1:1 with small drawing', async () => {
    const view = await loadAndWait('simple', '/d/test/');
    expect(view.zoom).toBe(1);
    expect(view.x).toBe(0);
    expect(view.y).toBe(0);
  });

  it('pans to fit offset drawing', async () => {
    const view = await loadAndWait('shifted', '/d/test/');
    expect(view.zoom).toBe(1);
    expect(view.x).toBe(-128);
    expect(view.y).toBe(-128);
  });

  it('scales to fit big drawing', async () => {
    const view = await loadAndWait('big', '/d/test/');
    expect(view.zoom).toBeLessThan(1);
    expect(view.x).toBe(0);
    expect(view.y).toBe(0);
  });

  it('goes to t,0 without l,b,r in url', async () => {
    const view = await loadAndWait(null, '/d/test/?t=300');
    expect(view.zoom).toBe(1);
    expect(view.x).toBe(0);
    expect(view.y).toBe(300);
  });

  it('goes to 0,l without t,b,r in url', async () => {
    const view = await loadAndWait(null, '/d/test/?l=300');
    expect(view.zoom).toBe(1);
    expect(view.x).toBe(300);
    expect(view.y).toBe(0);
  });

  it('goes to t,l without b,r in url', async () => {
    const view = await loadAndWait(null, '/d/test/?t=300&l=300');
    expect(view.zoom).toBe(1);
    expect(view.x).toBe(300);
    expect(view.y).toBe(300);
  });

  it('centers view with 1:1 zoom in url', async () => {
    const view = await loadAndWait(null, '/d/test/?t=300&r=600&b=600&l=300');
    expect(view.zoom).toBe(1);
    expect(view.x).toBe(300 - (view.width - 300) / 2);
    expect(view.y).toBe(300 - (view.height - 300) / 2);
  });

  it('centers large view with in url', async () => {
    // Force height to be the scaling factor
    const view = await loadAndWait(null, '/d/test/?t=300&r=2300&b=4300&l=300');
    const scale = view.height / 4000;
    expect(view.zoom).toBe(Math.round(10 * scale) / 10);
    const viewWidth = 2000 * scale;
    expect(view.y).toBe(300);
    expect(view.x).toBeCloseTo(300 - (view.width - viewWidth) / 2 / scale, 1);
  });
});
