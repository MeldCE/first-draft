import {
  asyncFind,
  getCoordinate,
  getToolButton,
  loadAndActivateTool,
  loadAndWait
} from '../helpers/utils';

describe('first-draft pen tool', () => {
  const basicLineActions = [
    {
      type: 'pointer',
      id: 'mouse',
      parmeters: {
        pointerType: 'mouse'
      },
      actions: [
        { type: 'pointerMove', duration: 0, x: 20, y: 100 },
        { type: 'pointerDown', button: 0 },
        { type: 'pause', duration: 500 },
        {
          type: 'pointerMove',
          duration: 500,
          origin: 'pointer',
          x: 50,
          y: 50
        },
        { type: 'pause', duration: 500 },
        {
          type: 'pointerMove',
          duration: 500,
          origin: 'pointer',
          x: 50,
          y: 50
        },
        { type: 'pause', duration: 500 },
        { type: 'pointerUp', button: 0 },
        {
          type: 'pointerMove',
          duration: 500,
          origin: 'pointer',
          x: 50,
          y: 50
        },
        { type: 'pause', duration: 500 }
      ]
    }
  ];

  it('becomes selected when clicked', async () => {
    await loadAndWait(null);
    const toolbar = await $('#toolbar');
    expect(toolbar).toExist();
    const toolButton = await asyncFind(
      await toolbar.$$('button'),
      async (button) => (await button.getAttribute('title')) === 'Pen tool'
    );
    expect(toolButton).toExist();

    await toolButton.click();
    expect(toolButton).toHaveAttribute('aria-selected', 'true');
    const changeEvent = await browser.execute(() =>
      window['socketEvents'].sent.find(
        (event) => event.event === 'draft:changeTool'
      )
    );

    expect(changeEvent).toBeDefined();
    expect(changeEvent.data.tool).toEqual('pen');
  });

  it('draws a line with a mouse dragging', async () => {
    await browser.execute(() => window['socketEvents'].clear());

    await browser.performActions(basicLineActions);

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();
    const elementName = lastEvent.data.element.name;

    expect(lastEvent.data.action).toBe('create');
    expect(lastEvent.data.element.type).toBe('Path');
    expect(lastEvent.data.active).toBe(false);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.action).toBe('create');
      expect(drawEvents[i].data.active).toBe(true);
      expect(drawEvents[i].data.element.name).toBe(elementName);
      expect(drawEvents[i].data.element.type).toBe('Path');
    }

    expect(lastEvent.data.element.segments.length).toBeLessThan(
      drawEvents[drawEvents.length - 1].data.element.segments.length
    );

    // Check didn't take in last pointer move
    const first = getCoordinate(lastEvent.data.element.segments, 0);
    const last = getCoordinate(lastEvent.data.element.segments, -1);
    expect(last[0] - first[0]).toBe(100);
    expect(last[1] - first[1]).toBe(100);
  });

  it('cancels a line while mouse dragging with an Esc', async () => {
    await browser.execute(() => window['socketEvents'].clear());

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 200 }
        ]
      },
      {
        type: 'key',
        id: 'keyboard',
        actions: [
          { type: 'keyDown', value: 'Escape' }, //'\uE00C' },
          { type: 'pause', duration: 500 },
          { type: 'keyUp', value: 'Escape' }, // '\uE00C' }
          { type: 'pause', duration: 500 }
        ]
      },
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerUp', button: 0 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 }
        ]
      }
    ];

    await browser.performActions(actions);

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();
    const elementName = lastEvent.data.elementName;
    const layerName = lastEvent.data.layerName;

    expect(lastEvent.data.action).toBe('delete');
    expect(lastEvent.data.active).toBe(true);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.action).toBe('create');
      expect(drawEvents[i].data.active).toBe(true);
      expect(drawEvents[i].data.element.name).toBe(elementName);
      expect(drawEvents[i].data.layerName).toBe(layerName);
      expect(drawEvents[i].data.element.type).toBe('Path');
    }
  });

  it('draws a line with a mouse clicking', async () => {
    await browser.execute(() => window['socketEvents'].clear());

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 0 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 },
          { type: 'pointerDown', button: 0 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 },
          { type: 'pointerDown', button: 0 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 },
          { type: 'pointerDown', button: 2 },
          { type: 'pointerUp', button: 2 },
          { type: 'pause', duration: 500 }
        ]
      }
    ];

    await browser.performActions(actions);

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();
    const elementName = lastEvent.data.element.name;

    expect(lastEvent.data.action).toBe('create');
    expect(lastEvent.data.element.type).toBe('Path');
    expect(lastEvent.data.active).toBe(false);
    expect(lastEvent.data.element.segments.length).toBe(3);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.action).toBe('create');
      expect(drawEvents[i].data.active).toBe(true);
      expect(drawEvents[i].data.element.name).toBe(elementName);
      expect(drawEvents[i].data.element.type).toBe('Path');
    }

    expect(drawEvents[drawEvents.length - 1].data.element.segments.length).toBe(
      3
    );

    // Check didn't take in last pointer move
    const first = getCoordinate(lastEvent.data.element.segments, 0);
    const last = getCoordinate(lastEvent.data.element.segments, -1);
    expect(last[0] - first[0]).toBe(100);
    expect(last[1] - first[1]).toBe(100);
  });

  it('cancels a line while mouse clicking with an Esc', async () => {
    await browser.execute(() => window['socketEvents'].clear());

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 0 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 },
          { type: 'pointerDown', button: 0 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pointerDown', button: 0 },
          { type: 'pointerUp', button: 0 },
          { type: 'pause', duration: 200 }
        ]
      },
      {
        type: 'key',
        id: 'keyboard',
        actions: [
          { type: 'keyDown', value: 'Escape' },
          { type: 'keyUp', value: 'Escape' },
          { type: 'pause', duration: 200 }
        ]
      },
      {
        type: 'pointer',
        id: 'mouse',
        parmeters: {
          pointerType: 'mouse'
        },
        actions: [
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 }
        ]
      }
    ];

    await browser.performActions(actions);

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();
    const elementName = lastEvent.data.elementName;
    const layerName = lastEvent.data.layerName;

    expect(lastEvent.data.action).toBe('delete');
    expect(lastEvent.data.active).toBe(true);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.action).toBe('create');
      expect(drawEvents[i].data.active).toBe(true);
      expect(drawEvents[i].data.element.name).toBe(elementName);
      expect(drawEvents[i].data.layerName).toBe(layerName);
      expect(drawEvents[i].data.element.type).toBe('Path');
    }
  });

  it('draws a line with a finger', async () => {
    await browser.execute(() => window['socketEvents'].clear());

    // Draw line
    const actions = [
      {
        type: 'pointer',
        id: 'finger1',
        parmeters: {
          pointerType: 'touch'
        },
        actions: [
          { type: 'pointerMove', duration: 0, x: 20, y: 100 },
          { type: 'pointerDown', button: 0 },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 },
          { type: 'pointerUp', button: 0 },
          {
            type: 'pointerMove',
            duration: 500,
            origin: 'pointer',
            x: 50,
            y: 50
          },
          { type: 'pause', duration: 500 }
        ]
      }
    ];

    await browser.performActions(actions);

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();
    const elementName = lastEvent.data.element.name;

    expect(lastEvent.data.element.type).toBe('Path');
    expect(lastEvent.data.active).toBe(false);

    for (let i = 0; i < drawEvents.length; i++) {
      expect(drawEvents[i].data.active).toBe(true);
      expect(drawEvents[i].data.element.name).toBe(elementName);
      expect(drawEvents[i].data.element.type).toBe('Path');
    }

    expect(lastEvent.data.element.segments.length).toBeLessThan(
      drawEvents[drawEvents.length - 1].data.element.segments.length
    );

    // Check didn't take in last pointer move
    const first = getCoordinate(lastEvent.data.element.segments, 0);
    const last = getCoordinate(lastEvent.data.element.segments, -1);
    expect(last[0] - first[0]).toBe(100);
    expect(last[1] - first[1]).toBe(100);
  });

  it('changes the properties of the selected line when options are changed', async () => {
    const { toolbar } = await loadAndActivateTool(null, 'Pen tool');

    const penOptions = await getToolButton('Pen options');

    await browser.performActions(basicLineActions);

    // Open the options
    await penOptions.click();

    const optionForm = await toolbar.$('.buttonContainer.pen .toolOptions');

    expect(optionForm).toExist();

    // Change values
    await (await optionForm.$('[name="thickness"]')).setValue('20');
    await (await optionForm.$('[name="opacity"]')).setValue('20');
    try {
      // Try setValue for Puppeteer (locally running tests)
      await (await optionForm.$('[name="strokeCap"]')).setValue('none');
    } catch {
      // Use selectByAttribute for Selenium
      await (await optionForm.$('[name="strokeCap"]')).selectByAttribute(
        'value',
        'none'
      );
    }
    await penOptions.click();

    // Check for modify event(s)
    let drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    let lastEvent = drawEvents.pop();
    let firstEvent = drawEvents.shift();

    expect(firstEvent.data.action).toBe('create');

    let elementName = firstEvent.data.element.name;

    expect(lastEvent.data.action).toBe('modifyMultiple');
    expect(lastEvent.data.elements.length).toBe(1);
    expect(lastEvent.data.elements[0].element.name).toBe(elementName);
    expect(lastEvent.data.elements[0].element.strokeWidth).toBe(20);
    expect(lastEvent.data.elements[0].element.opacity).toBe(0.2);
    expect(lastEvent.data.elements[0].element.strokeCap).toBe('none');

    // Draw a second line
    await browser.execute(() => window['socketEvents'].clear());
    await browser.performActions(basicLineActions);

    drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    lastEvent = drawEvents.pop();
    firstEvent = drawEvents.shift();

    expect(firstEvent.data.action).toBe('create');

    elementName = firstEvent.data.element.name;

    // TODO Import default values from a config
    expect(lastEvent.data.action).toBe('create');
    expect(lastEvent.data.element.name).toBe(elementName);
    expect(lastEvent.data.element.strokeWidth).toBe(2);
    expect(lastEvent.data.element.opacity).toBeUndefined();
    expect(lastEvent.data.element.strokeCap).toBe('round');
  });

  it('changes the properties of new lines when options are changed with nothing selected', async () => {
    const { toolbar } = await loadAndActivateTool(null, 'Pen tool');

    const penOptions = await getToolButton('Pen options');

    // Open the options
    await penOptions.click();

    const optionForm = await toolbar.$('.buttonContainer.pen .toolOptions');

    expect(optionForm).toExist();

    // Change values
    await (await optionForm.$('[name="thickness"]')).setValue('20');
    await (await optionForm.$('[name="opacity"]')).setValue('20');
    try {
      // Try setValue for Puppeteer (locally running tests)
      await (await optionForm.$('[name="strokeCap"]')).setValue('none');
    } catch {
      // Use selectByAttribute for Selenium
      await (await optionForm.$('[name="strokeCap"]')).selectByAttribute(
        'value',
        'none'
      );
    }
    await penOptions.click();

    await browser.performActions(basicLineActions);

    const drawEvents = await browser.execute(() =>
      window['socketEvents'].sent.filter(
        (event) => event.event === 'draft:draw'
      )
    );

    const lastEvent = drawEvents.pop();
    const firstEvent = drawEvents.shift();

    expect(firstEvent.data.action).toBe('create');

    const elementName = firstEvent.data.element.name;

    // TODO Import default values from a config
    expect(lastEvent.data.action).toBe('create');
    expect(lastEvent.data.element.name).toBe(elementName);
    expect(lastEvent.data.element.strokeWidth).toBe(20);
    expect(lastEvent.data.element.opacity).toBe(0.2);
    expect(lastEvent.data.element.strokeCap).toBe('none');
  });
});
