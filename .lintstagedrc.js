module.exports = {
  '{src/README.md,CHANGELOG.md,config.example.js,package.json}': [
    () => 'utils/mdFileInclude src/README.md README.md',
    'git add README.md'
  ],
  '*.{scss,md}': [
    'prettier --write'
  ],
  '*.{ts,json}': [
    'prettier --write',
    'eslint -c .eslintrc.commit.js --fix'
  ]
};
