# first-draft 0.8.0

FOSS live drawing and annotation tool.

[![pipeline status](https://gitlab.com/bytesnz/first-draft/badges/master/pipeline.svg)](https://gitlab.com/bytesnz/first-draft/commits/master)
[![first-draft on NPM](https://bytes.nz/b/first-draft/npm)](https://npmjs.com/package/first-draft)
[![first-draft on Matrix](https://bytes.nz/b/first-draft/custom?color=lightgrey&name=[matrix]&value=%23first-draft%3Amatrx.org)](https://matrix.to/#/#first-draft:matrix.org)
[![developtment time](https://bytes.nz/b/first-draft/custom?color=yellow&name=development+time&value=~319+hours)](https://gitlab.com/MeldCE/first-draft/blob/master/.tickings)
[![support development](https://bytes.nz/b/first-draft/custom?color=brightgreen&name=support+development&value=$$)](https://patreon.com/first_draft)
[![contributor covenant](https://bytes.nz/b/first-draft/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/MeldCE/first-draft/blob/master/code-of-conduct.md)

# INTIAL DEVELOPMENT - NOT STABLE!

**This package is under heavy initial development. Once it is ready to be
used in production, it will be published as a non-zero major version.**

# Found a Bug or Got an Idea?

If you found a bug or have an idea for this project, please either
[create an issue for it](https://gitlab.com/MeldCE/first-draft/issues/new)
(Gitlab account currently required) or
[send us an email](mailto:incoming+meldce-first-draft-10995301-issue-@incoming.gitlab.com)!

## Aim

I believe that the best ideas are created through collaboration. If you work
in a visual sector, creating blueprints for buildings or layouts for web sites,
having somewhere to share your ideas, to annotate changes and highlight key
features allows fast feedback and increased user input. Everyone should have
somewhere to share their first draft.

What better place to share it than the Internet.

To make sharing and getting feedback easier, faster and more customisable,
I am creating first-draft, an open-source web-based live collaboration drawing
and annotation tool.

[Take a look below](#current-controls) for how to use the tools.

## Features

Some of the current and upcoming features include (_future features are in
italics_):

- synchronised drawing between clients connected to the same draft
- pen tool
- an infinite and zoomable canvas
- text annotation
- storage as JSON and images
- select tool and delete
- images
- sharing views of the draft via url
- touch screen support (_currently limited - pen tool and two-finger panning works_)
- [_user restricted editing_](https://gitlab.com/MeldCE/first-draft/issues/4)
- _configurable tools_
- [_customisable draft defaults, including available tools, permissions and tool/key mappings_](https://gitlab.com/MeldCE/first-draft/issues/27)
- [_undo/redo_](https://gitlab.com/MeldCE/first-draft/issues/8)
- [_draft versioning_](https://gitlab.com/MeldCE/first-draft/issues/10)
- [_draft duplication_](https://gitlab.com/MeldCE/first-draft/issues/9)
- [_offline drawing_](https://gitlab.com/MeldCE/first-draft/issues/23)
- [_MongoDB storage_](https://gitlab.com/MeldCE/first-draft/issues/47)
- [_syncing views between users_](https://gitlab.com/MeldCE/first-draft/-/issues/43)
- _Hosted Drafts_

## Online

A public instance is available at https://draw.first-draft.xyz.

A demo instance is also available at https://demo.first-draft.xyz. The demo
instance will be regularly cleared so should not be used if you want to keep
your drafts

## Using first-draft

### Current Controls

These are the default controls for interacting with the canvas.

#### Any Tool

- Middle-button down, drag and release will pan the canvas
- Left-button down, drag and release with the shift-key will pan the canvas
- Two-finger drag will pan the canvas
- Scroll-wheel will zoom in/out the canvas
- Delete-key will delete the currently selected items
- Press the Esc key to cancel the action you are currently doing, then again
  to change to the [Select tool](#select-tool).

#### Pen Tool

- Left-button down, drag and release will draw a freehand line
- One-finger drag will draw a freehand line
- Left-click will start drawing lines between clicks. Right-click will finish
  the line. You can also use left-button down, drag and release to draw a
  freehand section.
- Escape during the drawing of a line will cancel the drawing of the line

#### Pan Tool

- Left-button down, drag and release will pan the canvas

#### Select Tool

- Left-click on an item will select the item
- Left-click on an item with the ctrl-key held down will add the item to
  the selection
- Left-button down, drag and release on a selected item will move the items
- Left-click and drag to the right to select any items fully contained within
  the drawn box
- Left-click and drag to the left to select any items that are full contained
  within the box or partially touch the box
- Hold down and do one of the two actions above to add items to the selection
  and be able to select items that are on top of an image without selecting
  or moving the image

### Pen and Text Presets

The styling of both lines and text can be adjust either before drawing or
when the item has been selected by clicking on the options menu (down arrow)
beside the relevant tool.

Preset styles for each tool can be saved for use later. This can be done by
selecting the settings for the preset and then clicking the
_Save settings as preset_ button. An existing preset can be removed by
double-clicking on the preset.

## Running first-draft

To use the package can either be installed per project, or globally

```
yarn add first-draft

# Create a folder for the drafts to be stored in
mkdir drafts

# Create a config file if you want
vim config.js

npx first-draft
```

or globally

```
yarn global add first-draft

# Create a folder for the drafts to be stored in
mkdir drafts

# Create a config file if you want
vim config.js

first-draft
```

By default, the server listens on 127.0.0.1:7067. This can be changed by
setting the `HOST` and `PORT` environment variables.

first-draft can also be run from its [Git][] repository, available on
[Gitlab](https://gitlab.com/MeldCE/first-draft). See the
[Development](#development) section for details.

### Docker

first-draft can also be run in container on any operating system using
[Docker][] or similar, and the docker images available from
[first-draft's container registry](first-draft-docker).
Installation instructions for [Docker][] are available for
[Windows][docker-windows], [Linux][docker-linux] and [Mac][docker-mac].

The `first-draft` image can be used to run the latest version of first-draft.
first-draft will start on port 80 within the container and it will by
default use file storage look for drafts in the `/app/drafts` folder. It will
also look for a configuration file in `/app/config.js`.

You can run this image using docker run:

```shell
docker run -i -p 8080:80 -v $(pwd)/config.js:/app/config.js \
  -v $(pwd)/drafts:/app/drafts registry.gitlab.com/meldce/first-draft
```

The `first-draft-dev` image can be used to develop first-draft. See the
[Development Docker Container][#development-docker-container] for more details.

### Configuration

A configuration file, `config.js` can be configure some basic settings in
first-draft. The config file should be located where you start the first-draft
server from. Below is an example config file with a description of the
currently available parameters. More will come

```javascript
module.exports = {
  // Password required to edit any drafts on the instance
  // Default: no password
  editPassword: 'notsosecretatthemoment',
  // BaseURI for the drafts
  // Default: "/d"
  draftUri: '/d',
  tools: {
    // Only enable to pen and select tools. Available tools are:
    // pen, select, image, pan-zoom and textbox
    enabledTools: [ 'pen', 'select' ],
    // Modify the default pointer/key mappings for tools
    mappings: [
      // This will change the escape key to clear the current select then
      // activate the select tool
      {
        tool: 'select',
        type: 'keyup',
        filter: {
          key: 'Escape'
        },
        action: 'clearAndSelectTool'
      },
      // When you press the p key, it will change to the pen tool
      {
        tool: 'pen',
        type: 'keyup',
        filter: {
          key: 'p'
        },
        action: 'activate'
      }
    ]
  }
};


```

### Nginx Reverse Proxy

If putting first-draft behind an
[Nginx Reverse Proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/),
to be able to use Web Sockets you will need to ensure the `Upgrade` and
`Connection` headers are passed through to first-draft. If wanting to upload
images larger than 2MB, you will also need to increase the
[`client_max_body_size`](https://nginx.org/en/docs/http/ngx_http_core_module.html#client_max_body_size).
An example server configuration, with the maximum size set to 10MB is below.

```
server {
  listen 80;
  listen 443 ssl;
  server_name master.first-draft.xyz;

  client_max_body_size 10M;

  location / {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_pass http://localhost:7067;
    proxy_http_version 1.1;
    proxy_redirect off;
  }

  root /var/www/first-draft.xyz;
}
```

## Love Opensource

first-draft is built on top of some great packages, including:

- [paper.js](http://paperjs.org)
- [fontastic.me](http://fontastic.me)
- [parcel.js](https://parceljs.org)
- [prettier](https://prettier.io/)

## Contributing

If you would like to contribute, either through raising or commenting on
issues or by submitting merge requests, please read our
[Code of Conduct / Contributor Covenant](https://gitlab.com/MeldCE/first-draft/blob/master/code-of-conduct.md),
adopted from https://www.contributor-covenant.org/. Breaches of this code
will not be tolerated and will acted upon.

We greatly appreciate contributions you would like to make. Feel free to
[create a new issue](https://gitlab.com/MeldCE/first-draft/issues/new)
to suggest an improvement, request a new feature or report an
issue.

We have also set up a [Patreon page](https://patreon.com/first_draft) should
you wish to support the continued development and support of first-draft.

### Development Process

Gitlab is being used for managing the development of this projact.

- The [Feature Board](https://gitlab.com/MeldCE/first-draft/boards/982414) shows the list of features being developed
- The [Version Board](https://gitlab.com/MeldCE/first-draft/boards/982432) shows the user stories planned for each [release](https://gitlab.com/MeldCE/first-draft/milestones)
- The [Development Board](https://gitlab.com/MeldCE/first-draft/boards/971250) shows the process of user stories
- The [Bug List](https://gitlab.com/MeldCE/first-draft/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=type%3A%20bug) shows any bugs that are currently outstanding

The process that will be followed for fixing a bug/creating a new feature will
be:

1. A [Gitlab Issue](https://gitlab.com/MeldCE/first-draft/issues)
   is created for the bug/work package, detailing the requirements of what
   needs to be done.
1. The issue is assigned a priority and discussed until is is fully specified
   and ready to be developed.
1. A [Gitlab Merge Request](https://gitlab.com/MeldCE/first-draft/merge_requests)
   is with the branch named after the issue number and two or three word
   description, and marked with "WIP:" in the merge request title to symbolise
   it is not yet ready to be merged
1. Work is carried out on the branch, commiting and pushing as required
1. Once work is complete, the "WIP:" is removed from the merge request title
   and the branch will be reviewed.
1. Once reviewed for both functionality and code quality, the merge request
   will be approved
1. The merge request will then be merged into dev, ready for the next
   version release.

### Development

first-draft is written in [typescript][] and uses [NodeJS][] to run on the
server.

To run first-draft from the [Git][] repository. You will require both a
[Git][git] or a [Git client][git-clients] and [NodeJs][nodejs] to be
installed:

1. Clone the repository -
   `git clone https://gitlab.com/MeldCE/first-draft.git`
1. Install the dependencies - `npm install` or `yarn`
1. Start the development server - `npm run dev` or `yarn dev`

#### Development Docker Contanier

A development environment can also be run using the
[dev first-draft docker image][first-draft-dev-docker], available from the
[first-draft registry][first-draft-registry].
The server will start on port 80 and will run with both server and client
hot-reloading.

To create and run a development instance of first-draft using Docker
(install instructions:

1. Clone the repository -
   `git clone https://gitlab.com/MeldCE/first-draft.git`
1. Run the docker container, mounting the _src_ folder, and optionally a config
   file and drafts folder into the docker container

```shell
touch config.js
docker run -it --rm --name first-draft -p 8080:80 -v $(pwd)/src:/app/src:ro \
  -v $(pwd)/config.js:/app/config.js -v $(pwd)/drafts:/app/drafts:ro \
  registry.gitlab.com/meldce/first-draft/dev
```

#### Testing

There are two sets of tests used in testing first-draft: non-ui tests using
[ava][]; and ui tests using [webdriver.io][].

Non-ui tests can be run locally or by using the dev docker container.

To run non-ui tests locally:

```shell
yarn test
```

To run non-ui tests in the dev docker container

```shell
docker run -it --rm -p 8080:80 -v $(pwd)/src:/app/srci:ro \
  registry.gitlab.com/meldce/first-draft/dev yarn test
```

UI tests can also be run locally on a local browser, locally using
the Selenium standalone docker containers
([firefox][standalone-firefox] or [chrome][standalone-chrome]), or in the dev
docker container using the Selenium standalon docker containers.

```shell
npm run test:ui
```

If the above command fails, you will need to run the tests using a docker
test browser

```shell
docker run --rm -it --name selenium selenium/standalone-firefox
```

```shell
HOST=0.0.0.0 TEST_HOST=http://172.17.0.1:7067 npm run test:ui -- --hostname 172.17.0.3 --path=/wd/hub
```

`172.17.0.1` is the IP address of the docker host machine and `172.17.0.3` is
the IP address of the selenium container.

To run the tests in the Chrome browser, the `browserName` property in the
`capabilities` array needs to be changed to `chrome` in the _wdio.conf.ts_
file.

To run a specific spec file, specify it using the `--spec` argument

```shell
HOST=0.0.0.0 TEST_HOST=http://172.17.0.1:7067 npm run test:ui -- --hostname 172.17.0.3 --path=/wd/hub --spec test/spec/load.ts
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.8.0] 2022-05-18

### Added

- Importing of PDFs. Each page of the PDF is rendered and then imported
  as a collection of images

## [0.7.0] 2022-02-11

### Added

- Presets label to presets

### Fixed

- Ignore z in url if is equal to 0
- Add tool preset information to the README
- Improved styling around options menu
- Handling of drawing when move mouse off canvas, unclick, then move back on
- Fixed issues with presets
- Errors caused by trying to change the styling of textboxes and lines
  being created

### Changed

- Migrated to [Vite](https://vitejs.dev/) as just so much easier
- Changed to using alpine as base image for docker images

## [0.6.2]

### Fixed

- Fixed initial zoom on draft with spaces, by removing initial URL rewrite

### Changed

- URLs are now not decoded in the address bar, makig them easier to share

## [0.6.1] 2021-01-22

### Fixed

- The scaling of font when the font size is changed during editing a textbox

## [0.6.0] 2021-01-20

### Added

- Added stroke ending to pen options
- Added text options and presets
- Added pen presets

## Changed

- Pressing escape if there are objects selected will deselect those selected
  objects. Pressing escape with no objects selected will change the tool to the
  Select tool

## [0.5.0] 2020-10-01

### Added

- Added UI testing framework using [WebdriverIO](https://webdriver.io)
- Added ability to share a view of a draft through a URL
- Added the current view to the url. If view was specified in the original
  url, you will be able to navigate back to it using the browsers back button.
- Added `viewUrl` to the config to enable/disable adding of the current view
  to the url
- Added `alwaysReplaceUrl` to disable/enable keeping the initial view in the
  browser history

### Fixed

- Optimizing of Javascript

## [0.4.3] - 2020-09-26

### Added

- Added deflate/gzip compression to server

## [0.4.2] - 2020-05-12

### Changed

- Changed to [alpine](https://www.alpinelinux.org/)-based
  [node](https://nodejs.org) [docker](https://docs.docker.com/engine/) image
  to decrease size and increase security of docker image
- Updated a few dependencies

## [0.4.1] - 2020-05-10

### Fixed

- Setting of client IP for debug logging

## [0.4.0] - 2020-04-12

### Added

- Thumbnailing of large images to increase performance of draft with large
  images and decrease the amount of data that needs to be downloaded (as
  it can be configured to download thumbnails and full-size images only when
  required [when the images are zoomed in on]). A 500ms debounce delay has been
  added to optimize performance of changing between images.
- Added the ability to zoom out further (up to 0.001x by default), and make it
  configurable using the `minZoomLevel` configuration parameter
- Implemented the ability to customise the tools enabled and the mappings
  per server (in the config file) and per draft (manually)
- Added a dual function selection box to the select tool. Click and drag the
  selection box to the right to select only those items completely enclosed
  by the box. Click and drag the selection box to the left to select all items
  that touch or are inside the box. You can add more items to an existing
  selection by holding down (by default) the Ctrl key. Also works with touch
  devices.
- When creating a new draft, the name of the draft is now set to the slug
  (the unique url id) of the draft by default unless the slug has been
  randomly generated.
- Add ability to export the current view as a PNG or SVG image and the
  entire drawing as an SVG image
- Sending of cursor, tool change and view change update messages, which will
  be used to share view and cursor with other users. This can be turned off
  for either the server, drawing or user by using the `share` config object.
- Added ability to debug requests and events on the server

### Fixed

- Fixed the remembering of passwords and user details
- non-urls being pasted are now ignored, unless in the text tool

### Other

- Updated dependencies, except PaperJs, which is currently broken
- Added temporary fix for Gitlab docker build issue (#149)

## [0.3.6] - 2019-09-10

### Fixed

- Enabling of saving of password when a password is entered

## [0.3.5] - 2019-09-09

### Added

- Support email address to README

## [0.3.4] - 2019-07-22

### Fixed

- Fixed image file select on Chrome for Android

## [0.3.3] - 2019-07-18

### Fixed

- Fixed pasting formatted text

## [0.3.2] - 2019-07-14

### Fixed

- Fixed README

## [0.3.1] - 2019-07-13

### Fixed

- pasting text when textbox tool active, but no textbox active

## [0.3.0] - 2019-07-12

### Added

- docker containers for containerised running and development
- automated development branch previewing for all
  [merge requests](https://gitlab.com/MeldCE/first-draft/merge_requests)
- preview of the next segment for the click-aciton pen tool
- being able to paste text into a draft when the textbox tool is selected
- pen configuration that can change line properties for future and currently
  selected lines
- insert images through either an image url or selecting them from your
  computer

### Fixed

- Using special characters in draft ids
- Using special characters in images

## [0.2.1] - 2019-05-24

### Added

- Pan/Zoom tool with global using middle click or shift click
- Cancelling of a path using the pen with a right click or escape
- Click select/move/delete tool
- Click pen tool
- Image insertion using drag and drop or pasting image(s) onto the draft
- Global edit password that can be set in a config file (see README)

### Fixed

- Sizing in mobile browsers
- Cancelled line not being removed from other connected clients

## [0.1.3] - 2019-03-13

### Added

- Added demo link to README

### Fixed

- Add bang to first-draft command so it runs

## [0.1.2] - 2019-03-10

Initial release.

Features:

- pen tool
- saving to JSON/folder

## [0.0.1] - 2019-03-02

Initial placeholder release

[unreleased]: https://gitlab.com/MeldCE/first-draft/compare/v0.8.0...dev
[0.8.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.7.0...v0.8.0
[0.7.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.6.2...v0.7.0
[0.6.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.3...v0.5.0
[0.4.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.6...v0.4.0
[0.3.6]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.5...v0.3.6
[0.3.5]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.4...v0.3.5
[0.3.4]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.3...v0.3.4
[0.3.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/MeldCE/first-draft/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/MeldCE/first-draft/compare/v0.1.3...v0.2.1
[0.1.3]: https://gitlab.com/MeldCE/first-draft/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/MeldCE/first-draft/compare/v0.0.1...v0.1.2
[0.0.1]: https://gitlab.com/MeldCE/first-draft/tree/v0.0.1


[first-draft-docker]: https://gitlab.com/MeldCE/first-draft/container_registry
[git]: https://git-scm.com/
[git-clients]: https://git-scm.com/downloads/guis/
[docker]: https://docs.docker.com/engine/
[docker-windows]: https://docs.docker.com/docker-for-windows/install/
[docker-linux]: https://docs.docker.com/engine/install/
[docker-mac]: https://docs.docker.com/docker-for-mac/install/
[typescript]: https://www.typescriptlang.org/
[nodejs]: https://nodejs.org
[first-draft-dev-docker]: https://gitlab.com/MeldCE/first-draft/container_registry/eyJuYW1lIjoibWVsZGNlL2ZpcnN0LWRyYWZ0L2RldiIsInRhZ3NfcGF0aCI6Ii9NZWxkQ0UvZmlyc3QtZHJhZnQvcmVnaXN0cnkvcmVwb3NpdG9yeS81ODgyOTUvdGFncz9mb3JtYXQ9anNvbiIsImlkIjo1ODgyOTUsImNsZWFudXBfcG9saWN5X3N0YXJ0ZWRfYXQiOm51bGx9
[standalone-firefox]: https://hub.docker.com/r/selenium/standalone-firefox
[standalone-chrome]: https://hub.docker.com/r/selenium/standalone-chrome
