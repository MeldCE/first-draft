module.exports = {
  // Password required to edit any drafts on the instance
  // Default: no password
  editPassword: 'notsosecretatthemoment',
  // BaseURI for the drafts
  // Default: "/d"
  draftUri: '/d',
  tools: {
    // Only enable to pen and select tools. Available tools are:
    // pen, select, image, pan-zoom and textbox
    enabledTools: [ 'pen', 'select' ],
    // Modify the default pointer/key mappings for tools
    mappings: [
      // This will change the escape key to clear the current select then
      // activate the select tool
      {
        tool: 'select',
        type: 'keyup',
        filter: {
          key: 'Escape'
        },
        action: 'clearAndSelectTool'
      },
      // When you press the p key, it will change to the pen tool
      {
        tool: 'pen',
        type: 'keyup',
        filter: {
          key: 'p'
        },
        action: 'activate'
      }
    ]
  }
};

